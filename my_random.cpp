#define MYRANDOM_CPP
#include "my_random.hpp"
double GEO_DIST_LENGTH, EXP_DIST_EVENTS;

MyRandom pop_rand;
MyRandom evo_rand;
MyRandom env_rand;
MyRandom switch_rand;

//initializing all distributions and generators
MyRandom::MyRandom()
  : uniform_dist(0,1), uniform_gen(0),
    uniform_dist2(-1,1),uniform_gen2(0),
    uniform_dist3(-1.5,1.5),uniform_gen3(0),
    uniform_dist4(-1.75,1.25),uniform_gen4(0),
    uniform_int_dist10(1,10),uniform_int_gen10(0),
    normal_dist(0,1), normal_dist_gen(0),                   //normal_dist(0,1) is Gaussian with mean 0 and standard dev. 1
    normal_dist2(-0.3,.7), normal_dist_gen2(0)
    // ,geometric_dist1(0.1), geometric_dist_gen1(0)
{}

MyRandom::~MyRandom()
{
  delete uniform_gen;
  delete uniform_gen2;  
  delete uniform_gen3;  
  delete uniform_gen4;  
  delete uniform_int_gen10;
  delete normal_dist_gen;  
  delete normal_dist_gen2;
  delete geometric_dist_gen1;
  delete exponential_dist_gen1;
}

void MyRandom::init(unsigned int seed)
{
  EXP_DIST_EVENTS=0.5;
  geometric_dist1 = boost::geometric_distribution<int>(GEO_DIST_LENGTH);
  exponential_dist1 = boost::exponential_distribution<double>(EXP_DIST_EVENTS);
  //seeding the general purpose random number generator
  random_gen.seed(seed);
  //random number generators defined on their respective distributions
  uniform_gen =      new boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<double> >(random_gen,uniform_dist);
  uniform_gen2 =     new boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<double> >(random_gen,uniform_dist2);
  uniform_gen3 =     new boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<double> >(random_gen,uniform_dist3);
  uniform_gen4 =     new boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<double> >(random_gen,uniform_dist4);
  uniform_int_gen10= new boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_int <int   > >(random_gen,uniform_int_dist10);
  normal_dist_gen =  new boost::variate_generator<boost::lagged_fibonacci19937&, boost::normal_distribution<double> >(random_gen,normal_dist);
  normal_dist_gen2 =  new boost::variate_generator<boost::lagged_fibonacci19937&, boost::normal_distribution<double> >(random_gen,normal_dist2);
  geometric_dist_gen1=new boost::variate_generator<boost::lagged_fibonacci19937&, boost::geometric_distribution<int> >(random_gen,geometric_dist1);
  exponential_dist_gen1=new boost::variate_generator<boost::lagged_fibonacci19937&, boost::exponential_distribution<double> >(random_gen,exponential_dist1);
}
