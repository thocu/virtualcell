#include "petridish.hpp"
#include <boost/serialization/export.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>

int LIFETIME; // 1000 
double MEASURESTEP; // 1 //every n steps we make a concentration
		 // measurement if LOWPROFILE = false

double MUTATION_RATE;
double CHANGE_RATE;

int PER_GENE_MUTATION; // 2 // 1 = no major mut,2 = major mut,3 =
		       // ordered genome
bool SPARSE; // true
bool DEATH; // false // IF TRUE, reproduce2() IS USED; make production
	    // rate an evolutionary target
bool ENV_SKEWED; // 0 // skew the environment prob func towards lower
		 // concentrations using a normal distribution with
		 // mean < 0.

int REPRODUCE; // 1 //1 or 2 works with DEATH true or false but when 1
	       // , DEATH doesn't have any effect
bool _3ENV; // false
bool LOWPROFILE; // false

int TREE_CUTOFF; // 50 // when making a tree, stop adding nodes n
		 // generations last generation
int SAVETIME; // 500 // after how many times will a dish_save +
	      // drawing of tree occur
int ANALYZE_TIME; // after how many generations will fitness be analyzed
//int STOPSAVE; // 10000 // 5000 // stop saving dishes after this time
//to prevent segfault

// not used yet
int GENERATIONS,POP_SEED,EVO_SEED,ENV_SEED;

double CHANGE_THRESHOLD, H; 
double ERROR_RATE, STEP_REDUCTION; // fraction of cells with
				   // integration error, above which
				   // integration step is with a
				   // factor STEP_REDUCTION.

double ENV_CHANGE_FIT_MIN;
bool ENV_SWITCHING = false;
bool CHANGE_ENV = false;
bool reached_fitness = false;
double RANK_CUT_OFF;
bool NO_TARGET_CONC;
bool RESET;

//IF YOU WANT MORE GRAPHS change the number after grace_par_file to the appropriate amount of pipes HERE!!
PetriDish::PetriDish(string grace_par_file,int enable):grace(enable,grace_par_file,8),
		       cells(),ancestors(),roots(),pop_size(0),Time(0),
		       generations(0),mutationrate(0),change_rate(0),updatesteps(0),
		       fitness_max(0),fitness_average(0),complexity_max(0),
		       complexity_average(0.),start_nmbr_genes(0),
		       pop_seed_used(1),evo_seed_used(1),env_seed_used(1),
		       fout(),fout2(),drawings(0){
  /*std::vector<double> generation_set;
  std::vector<double> pop_size_set;
  std::vector<double> Time_set;
  std::vector<double> mutationrate_set;
  std::vector<double> change_rate_set;
  std::vector<double> evo_seed_used_set;
  std::vector<double> env_seed_used_set; */
      DishParamUpdate();
  }

// alternative constructor to read in a chosen grace parameter file (
// used in restoring dishes )
PetriDish::PetriDish(string grace_par_file,int enable,int graphs)
  :grace(enable,grace_par_file,graphs),cells(),ancestors(),roots(),pop_size(0),Time(0),
   generations(0),mutationrate(0),change_rate(0),updatesteps(0),fitness_max(0),
   fitness_average(0),complexity_max(0),complexity_average(0.),start_nmbr_genes(0),
   pop_seed_used(1),evo_seed_used(1),env_seed_used(1),fout(),fout2(),drawings(0){
  /*std::vector<double> generation_set;
  std::vector<double> pop_size_set;
  std::vector<double> Time_set;
  std::vector<double> mutationrate_set;
  std::vector<double> change_rate_set;
  std::vector<double> evo_seed_used_set;
  std::vector<double> env_seed_used_set; */
      DishParamUpdate();
}

//primary constructor
PetriDish::PetriDish(int nmbr_cells,int genes,string savedir, string grace_file,
		     int enable = 0,int pop_seed =2,int evo_seed =2,
		     int env_seed = 2, int graphs = 4, double mut_rate = 0.05,
		     double ch_rate = 0.4 )  //0.2
  :grace(enable,grace_file.c_str(),graphs),
   cells(),ancestors(),roots(),Time(0),generations(0),mutationrate(mut_rate),
   change_rate(ch_rate),fitness_max(0.),fitness_average(0.),complexity_max(0),
   complexity_average(0),start_nmbr_genes(genes),pop_seed_used(pop_seed),
   evo_seed_used(evo_seed),env_seed_used(env_seed),savedirectory(savedir),
   grace_par_file(grace_file),fout(),fout2(),drawings(0){
 /* std::vector<double> generation_set;
  std::vector<double> pop_size_set;
  std::vector<double> Time_set;
  std::vector<double> mutationrate_set;
  std::vector<double> change_rate_set;
  std::vector<double> evo_seed_used_set;
  std::vector<double> env_seed_used_set; */
      DishParamUpdate();
    fout.setf(ios::left);
    fout2.setf(ios::left);
    pop_rand.init(pop_seed);
    evo_rand.init(evo_seed);
    env_rand.init(env_seed);

    for(int i = 0; i< nmbr_cells; i++){
      cells.push_back(new Cell(genes,savedir));
    }
    pop_size = nmbr_cells;
    updatesteps = int(LIFETIME/H);
    saveParams();
  }


//test constructor : create cells with a range of genome sizes (strict) 
//to find correlation between size and fitness
PetriDish::PetriDish(int nr_cells_per_size_class, int min_size_class, 
		     int max_size_class, string savedir, int evo_seed = 2)
  :grace(0,"default.par",2),
  cells(),ancestors(),roots(),Time(0),generations(0),fitness_max(0.),
  fitness_average(0.),complexity_max(0),complexity_average(0),start_nmbr_genes(1),
   evo_seed_used(evo_seed),savedirectory(savedir),fout(),fout2(),drawings(0){

    fout.setf(ios::left);
    fout2.setf(ios::left);
    evo_rand.init(evo_seed);
    env_rand.init(evo_seed);  //we make env_seed = evo_seed
    string strict = "strict_size";
    for(int j = min_size_class; j <= max_size_class; j++){
      for(int i = 0; i < nr_cells_per_size_class; i++){
	cells.push_back(new Cell(j,strict,savedir));
      }
    }
    pop_size = nr_cells_per_size_class * ( max_size_class - min_size_class );
    updatesteps = int(LIFETIME/H);
    saveParams();
}

//update concentrations of gene products at current timestep for all
//cells in list
void PetriDish::updateStep(list<Cell *> & cell_list){
  /*TEMPORARY outcommenting for testing
  bool lower_H;
  do {
    lower_H = false;
    int errors = 0;
    for(cells_iter = cell_list.begin(); cells_iter != cell_list.end(); cells_iter++){
      (*cells_iter)->NewVals();    
      if((*cells_iter)->get_error()){
	errors++;
      }      
    }
    if(double(errors)/double(pop_size) > ERROR_RATE){
      H=H/STEP_REDUCTION;
      std::cout << "Lowered step size to " << H << " . Recalculating this step." << std::endl;
      lower_H=true;
    }
  } while (lower_H);
  */
  for(cells_iter = cell_list.begin(); cells_iter != cell_list.end(); cells_iter++){
    (*cells_iter)->Update(1);    
    //   std::cout << std::endl;      
  }
  _time += H;
}

//print concentrations of gene products at current timestep for all
//cells in list
void PetriDish::PrintStep(list<Cell *> & list){
  for(cells_iter = list.begin(); cells_iter != list.end() ; cells_iter++){
    (*cells_iter)->printConc(savedirectory,_time);    
  }  
  _time+=H;
}

//update and print concentrations of gene products at current timestep
//for all cells in list
void PetriDish::AnalyseStep(list<Cell *> & list){   
  bool print;
  /*TEMPORARY outcommenting for testing
  bool lower_H;
  do {
    lower_H = false;
    int errors = 0;
    for(cells_iter = list.begin(); cells_iter != list.end() ; cells_iter++){
      (*cells_iter)->NewVals();
      if((*cells_iter)->get_error()){
	errors++;
      }
    }
    if(double(errors)/double(pop_size) > ERROR_RATE){
      H=H/STEP_REDUCTION;
      std::cout << "Lowered step size to " << H << " . Recalculating this step." << std::endl;
      lower_H=true;
    }
  } while (lower_H);
  */
  for(cells_iter = list.begin(); cells_iter != list.end(); cells_iter++){
    print = (*cells_iter)->Update(1) ;
    if(fmod(_time,MEASURESTEP) == 0. && print){
      (*cells_iter)->printConc(savedirectory,_time);    
    }      
  }
  _time+=H;
}

void PetriDish::drawGraphs(list<Cell *> & cell_list){
  for(cells_iter = cell_list.begin(); cells_iter != cell_list.end() ; cells_iter++){
    (*cells_iter)->Graph(savedirectory);
  }
}

void PetriDish::saveParams(){
  string filename = savedirectory + "params.txt";
  fout.open(filename.c_str(), ios_base::out);
  fout << "LIFETIME = "<< LIFETIME << endl
       << "MEASURESTEP = " << MEASURESTEP << endl
       << "PER_GENE_MUTATION = " << PER_GENE_MUTATION << endl
       << "SPARSE " << SPARSE << endl
       << "DEATH = "<< DEATH << endl
       << "ENV_SKEWED = " << ENV_SKEWED << endl
       << "REPRODUCE = "<< REPRODUCE << endl
       << "_3ENV = "<< _3ENV << endl
       << "LOWPROFILE = " << LOWPROFILE << endl 
       << "TREE_CUTOFF = " << TREE_CUTOFF << endl
       << "SAVETIME = " << SAVETIME << endl

       << "pop_size = " << pop_size << endl
       << "pop_seed_used = "<< pop_seed_used << endl
       << "evo_seed_used = "<< evo_seed_used << endl
       << "env_seed_used = "<< env_seed_used << endl
       << "mutationrate = "<< mutationrate << endl
       << "change_rate = " << change_rate << endl

       << "Perm = " << Perm << endl
       << "startAin = " << startAin << endl
       << "startXin = " << startXin << endl
       << "TARGETA = " << TARGETA << endl
       << "TARGETX = " << TARGETX << endl
       << "N = " << N << endl
       << "INS = "<<INS << endl
       << "DUP = "<<DUP << endl
       << "DEL = "<<DEL << endl
       << "POINT = "<<POINT << endl
       << "ASSAYS = " << ASSAYS << endl;
  fout.close();
}

void PetriDish::saveFitness_Max_Average(){  
//saves fitnesses and sizes for all individuals & max's & averages
//etc.
  double max = 0.;
  double average = 0.;

  double split_average01 = 0;
  double split_average1 = 0;
  double split_average10 = 0;

  double split_anabolite_average01 = 0.;
  double split_anabolite_average1 = 0.;
  double split_anabolite_average10 = 0.;
  double total_anabolite_average = 0.;

  int cmax = 0;
  double caverage = 0.;
  double prod_average = 0.;
  double prod_max = 0.;
  Cell * fittest = cells.front();
  double distance_fittest;
  int distance_to_luca;
  ostringstream formatter;

  vector<pair<int,double> > sizeclass_fitness =  vector<pair<int,double> >(200,make_pair(0,0.0));   
  //holds # of cells in, and fitness of sizeclasses
  vector<pair<int,double> >::iterator sizeclass_iter;

  string filename = savedirectory + "fitnesses.dat";
  fout.open(filename.c_str(), ios_base::out | ios_base::app);
  fout << setw(13) << generations;

  filename = savedirectory + "sizes.dat";
  fout2.open(filename.c_str(), ios_base::out | ios_base::app);
  fout2 <<setw(13) << generations;

  grace[2] << generations;
  grace[3] << generations;

  for(cells_iter = cells.begin(); cells_iter != cells.end(); cells_iter++){
    double fitness;
    double basechance = 0.01;
    int size = (*cells_iter) -> get_nmbr_genes();
    // should this be: double production = (*cells_iter) ->
    // ReproductionAssay(); ?
    double prod_rate = (*cells_iter) -> get_production();
    if (prod_rate >0){ //a cell has to have gene products to live, they can't just rely on what their parents give them.
      fitness = (*cells_iter)->get_fitness() + basechance;
    }
    else fitness = 0;

    if(size <=0){ 
      cout << "this cell has size: " << size << endl;
      cout << **cells_iter << endl;
    }
    if((fitness-basechance)> max){
      max = (fitness-basechance);
      fittest = *cells_iter;
    }
    average += fitness;
    if(size > cmax) cmax = size;
    caverage += size; 
    if(prod_rate > prod_max) prod_max = prod_rate;
    prod_average += prod_rate;
    if(size < 200){
      (sizeclass_fitness[size].first)++;
      (sizeclass_fitness[size].second) += fitness;
    }
    else cout << "network size exceeds 200 !!! , not saving fitness " << endl;
    
    fout << setw(13) << fitness;
    fout2 << setw(13) << size;
    if( cells_iter == cells.begin() ){ //!LOWPROFILE ||
      grace[2] << size;
    }
    
    if(!SPARSE){
      double splitfitness01 = FitnessFunction( (*cells_iter) -> splitFitness(0) );
      double splitfitness1 = FitnessFunction( (*cells_iter) -> splitFitness(1) );
      double splitfitness10 =FitnessFunction( (*cells_iter) -> splitFitness(2) );
      split_average01 += splitfitness01;
      split_average1 += splitfitness1;
      split_average10 += splitfitness10;

      double split_a01 =  (*cells_iter) -> splitAnabolite(0);
      double split_a1 = (*cells_iter) -> splitAnabolite(1);
      double split_a10 = (*cells_iter) -> splitAnabolite(2);
      split_anabolite_average01 += split_a01;
      split_anabolite_average1 += split_a1;
      split_anabolite_average10 += split_a10;      
    }
  }
  cout << "made it out of the cell iterator loop" <<std::endl;

  formatter << "sample_cell_graph" << generations << ".dot";
  if(!LOWPROFILE)
    fittest  -> namedGraph(savedirectory,formatter.str());

  distance_to_luca = distanceLUCA();

  distance_fittest = distanceFittest(fittest);
  generation_best = make_pair(fittest,generations);

  fout << endl;
  fout.close();
  fout2 << endl;
  fout2.close();

  average /= cells.size();
  fitness_average = average;
  fitness_max = max;

  split_average01 /= cells.size();
  split_average1 /= cells.size();
  split_average10 /= cells.size();

  split_anabolite_average01 /= cells.size();
  split_anabolite_average1 /= cells.size();
  split_anabolite_average10 /= cells.size();
  total_anabolite_average = split_anabolite_average01 + split_anabolite_average1 +
    split_anabolite_average10;


  caverage /= cells.size();
  complexity_average = caverage;
  complexity_max = cmax;
  
  prod_average /= cells.size();
  product_gene_ratio_avrg = prod_average;

  product_gene_ratio_max = prod_max;

  filename = savedirectory + "max_average";
  fout.open(filename.c_str(), ios_base::out | ios_base::app);
  fout << setw(13) << generations << setw(13) << max << setw(13) << average 
       << setw(13) << cmax << setw(13) << caverage <<endl;
  fout.close();

  cout << generations << " " << fitness_max << " " 
       << fitness_average << endl;
  cout << generations << " " << complexity_max << " " 
       << complexity_average << endl;
  cout << generations << " " << product_gene_ratio_max << " "
       << product_gene_ratio_avrg  << endl;
  cout << "made it to lowprofile" <<std::endl;
  if(!LOWPROFILE){
    for(sizeclass_iter = sizeclass_fitness.begin(); sizeclass_iter != sizeclass_fitness.end(); sizeclass_iter ++){
      if((*sizeclass_iter).first != 0){
	(*sizeclass_iter).second /= (*sizeclass_iter).first;   //calculating average fitness for this sizeclass;
      }
      int sizeclass  = std::distance(sizeclass_fitness.begin(),sizeclass_iter);
      formatter.str("");
      formatter << savedirectory << "size_class" << sizeclass <<".dat";
      filename = formatter.str();
      fout.open(filename.c_str(), ios_base::out | ios_base::app);
      fout << setw(13) << generations << setw(13) << (*sizeclass_iter).second  
	   << setw(13)  << (*sizeclass_iter).first << endl;
      fout.close();
    }
  }
  cout << "made it past lowprofile" <<std::endl;

  if(!SPARSE){
    grace[0] << (generations) << fitness_max << fitness_average 
	     << split_average01 << split_average1 << split_average10 
	     << gracesc::endl;
    grace[5] << generations << log10(total_anabolite_average) << log10(split_anabolite_average01) 
	     << log10(split_anabolite_average1) << log10(split_anabolite_average10) 
	     << gracesc::endl;
  }
  else grace[0] << (generations) << fitness_max << fitness_average << gracesc::endl;
  grace[1] << (generations) << complexity_max  << complexity_average << gracesc::endl;
  grace[2] << complexity_average << gracesc::endl;
  grace[3] << distance_to_luca << distance_fittest << gracesc::endl;
  cout << "made it to gracepipes" <<std::endl;
  grace[6] << generations << Perm << Degr << N << gracesc::endl;
  grace[7] << generations << TARGETA << TARGETX << gracesc::endl;
  cout << "made it past gracepipes" <<std::endl;
}

double PetriDish::FitnessFunction(double assay){
  return pow(2.,assay) - 1.;
}

void PetriDish::storeFitness(list<Cell *> & list,int i){
 for(cells_iter = list.begin(); cells_iter != list.end(); cells_iter++){
   (*cells_iter)->store_fitness(i); 
 }
}

void PetriDish::storeScaledFitness(list<Cell *> & list){
 for(cells_iter = list.begin(); cells_iter != list.end(); cells_iter++){
   double raw_fitness = (*cells_iter)->FitnessAssay();
   (*cells_iter)->store_scaled_fitness(FitnessFunction(raw_fitness)); 
 }
}

void PetriDish::storeProduction(list<Cell *> & list,int env){
for(cells_iter = list.begin(); cells_iter != list.end(); cells_iter++){
   (*cells_iter)->store_production(env); 
 }
}

void PetriDish::storeReproduction(list<Cell *> & list){
  for(cells_iter = list.begin(); cells_iter != list.end(); cells_iter++){
    (*cells_iter)->store_scaled_production( (*cells_iter)->ReproductionAssay());
  }
}

void PetriDish::store_expression_A_X(list<Cell *> & list,int i){
 for(cells_iter = list.begin(); cells_iter != list.end(); cells_iter++){
   (*cells_iter)->store_expression_A_X(i); 
 }
}

void PetriDish::printCells(list<Cell *> & list){
  cout << list.size() << endl;
  for(cells_iter = list.begin(); cells_iter != list.end(); cells_iter++){
    cout << (*cells_iter)->get_number() << " " << (*cells_iter)->get_parent_number() << endl;
  }
}

string PetriDish::toString(){
  ostringstream formatter;
  for(cells_iter = cells.begin(); cells_iter != cells.end(); cells_iter++){
    formatter << *(*cells_iter) << endl << endl;
  }
  return formatter.str();
}

Cell* PetriDish::mutateCell1(Cell * c,double mut_rate){
  // including INS to maintain mutation rates when switching between
  // different mutation processes
  double total_mut = INS + DUP + DEL + POINT;  // INS 
  c -> mutateGenes(((mut_rate/total_mut)*DUP),   
		   ((mut_rate/total_mut)*DEL),   
		   ((mut_rate/total_mut)*POINT));
 return c;
}

Cell* PetriDish::mutateCell2(Cell * c,double mut_rate){
  // including INS to maintain mutation rates when switching between
  // different mutation processes
  double total_mut = INS + DUP + DEL + POINT;
  c -> mutateGenes2(((mut_rate/total_mut)*(DUP*4)),
		    ((mut_rate/total_mut)*(DEL*4)),
		    ((mut_rate/total_mut)*POINT)); 
 return c;
}

Cell*  PetriDish::mutateCell3(Cell * c,double mut_rate){
  double total_mut = INS + DUP + DEL + POINT;
  c -> mutateGenes3( (mut_rate/total_mut)*INS*4,
		     (mut_rate/total_mut)*DUP*4,
		     (mut_rate/total_mut)*DEL*4,
		     (mut_rate/total_mut)*POINT);
 return c;
}

// a mutation scheme whith WGD and small scale mutations, following a
// geometric distribution
Cell* PetriDish::mutateCell4(Cell * c,double mut_rate){
  double total_mut = INS + DUP + DEL + POINT;  // INS 
  c -> mutateGenes4((1/total_mut)*INS,
		    (1/total_mut)*DUP,
		    (1/total_mut)*DEL,
		    (mut_rate/total_mut)*POINT,
		    WGD,
		    mut_rate);
 return c;
}

// a mutation scheme with WGD and small scale mutations, following a
// geometric distribution
Cell* PetriDish::mutateCell5(Cell * c,double mut_rate){
  double total_mut = INS + DUP + DEL + POINT;  // INS 
  c -> mutateGenes5((mut_rate/total_mut)*DUP,
		    (mut_rate/total_mut)*DEL,
		    (mut_rate/total_mut)*POINT,
		    WGD);
 return c;
}

Cell*  PetriDish::mutateCell6(Cell * c,double mut_rate){
  double total_mut = INS + DUP + DEL + POINT;
  c -> mutateGenes6( (mut_rate/total_mut)*INS*4,
		     (mut_rate/total_mut)*DUP*4,
		     (mut_rate/total_mut)*DEL*4,
		     (mut_rate/total_mut)*POINT,
		     WGD);
 return c;
}

Cell*  PetriDish::mutateCell7(Cell * c,double mut_rate){
  c -> mutateGenes7(mut_rate,INS,DUP,DEL,POINT,GCR,SG,POLYP,NOTHING,MAX_GCR_FRAC);
 return c;
}

void PetriDish::mutateCells(){

  for(cells_iter = cells.begin(); cells_iter != cells.end(); cells_iter++){
    // scaling mutation rate to account for different mutation method
    if(PER_GENE_MUTATION == 1){ 
      mutateCell1(*cells_iter,mutationrate);
    }
    // when doing major duplications/deletions (which is a per genome
    // chance) between 1 and half the # of genes are affected (on
    // average 1/4 of genes) , so to scale, we need to multiply by 4
    else if(PER_GENE_MUTATION == 2){ 
      mutateCell2(*cells_iter,mutationrate);
    }
    else if(PER_GENE_MUTATION == 3){ 
      mutateCell3(*cells_iter,mutationrate);
    }
    else if(PER_GENE_MUTATION == 4){
      mutateCell4(*cells_iter,mutationrate);
    }
    else if(PER_GENE_MUTATION == 5){
      mutateCell5(*cells_iter,mutationrate);
    }
    else if(PER_GENE_MUTATION == 6){
      mutateCell6(*cells_iter,mutationrate);
    }
    else if(PER_GENE_MUTATION == 7){
      mutateCell7(*cells_iter,mutationrate);
    }

    else (*cells_iter)->mutateAgene(mutationrate); // removed 14 sept 2011 : *start_nmbr_genes);  
    // function mutates on a 1 mut per genome basis
  }
}

void PetriDish::resetConc(list<Cell *> & cell_list){
 for(cells_iter = cell_list.begin(); cells_iter != cell_list.end(); cells_iter++){
   (*cells_iter)->reset_concentration(); 
 }
}

// clean up lineages that have died out in this generation, compare to
// cleanList
void PetriDish::updateAncestors(list<Cell*>::iterator begin){
  for(cells_iter = begin; cells_iter != cells.end(); cells_iter++){
    if( (*cells_iter)->noChildren() ){
      // if it is the generation best, we will never delete this cell
      // ( a pointer to it remains in 'generation_best'
      if( *cells_iter != generation_best.first ){
	(*cells_iter) ->informParent();
	delete (*cells_iter);
      }
    }  
  }
}

void PetriDish::addCell(Cell * c){
  cells.push_front(c);
}

// used in spatial case to updateAncestors and erase cells that have
// died off, bringing the population size back to initial size
void PetriDish::cleanList(){
  for(cells_iter = cells.begin(); cells_iter != cells.end();){
    if((*cells_iter) -> isDead()){ 
      if( (*cells_iter)->noChildren() ){
	if( *cells_iter != generation_best.first ){      // because we want to compare best individuals 
                                                         // of two subsequent generations, we cannot 
                                                         // throw them away immediately
	  (*cells_iter) ->informParent();
	  delete (*cells_iter);
	}
      }
      cells_iter = cells.erase(cells_iter);
    }
    else cells_iter++;
  }
}

void PetriDish::reproduce(){               //fitness proportional reproduction

// old reproduction method
  list<Cell*>::iterator temp_begin = cells.begin();
  bool reproduce = true  ;                 
  double chance = 0.001;
  if(cells.size() <= 0) reproduce = false;  
  while(reproduce){
    for(cells_iter = temp_begin; cells_iter != cells.end(); cells_iter++){
      if(cells.size()>= 2*pop_size){
	reproduce = false;
	break;
      }
      if(evo_rand.uniform()<0.5) chance*=-1.;
      if(evo_rand.uniform() < (0.5 / fitness_average)* (*cells_iter)->get_fitness()+chance){
	Cell * child = new Cell(**cells_iter,savedirectory); 
	//copy constructor sets parent and child relation and push
	//them up front
	addCell(child);
      }

    }
  }
  // operate on tail of list, where previous generation is
  updateAncestors(temp_begin);
  cells.erase(temp_begin,cells.end());
  pop_size = cells.size();
  
}

void PetriDish::newreproduce(){
// new reproduction method
  list<Cell*>::iterator temp_begin = cells.begin();
   double basechance = 0.01; //basechance is also present in the fitness calculation, which means it needs to be put to 0 there if newreproduce isn't used. Not yet implemented so check first!!
  int celllistsize = cells.size();
  double fitnessarray[celllistsize];
  Cell* oldpopulationarray[celllistsize];
  int teller = 0;
  double totalfilled = 0;
  for(cells_iter = temp_begin; cells_iter != cells.end(); cells_iter++){
    if (fitness_average == 0) //in the case that fitness_average is 0, we just want all cells to have 1/celllistsize
    {
      totalfilled += 1.0+basechance/celllistsize;
    }
    else
    {
    double fitness;
    double prod_rate = (*cells_iter) -> get_production();
    if (prod_rate >0){ //a cell has to have gene products to live, they can't just rely on what their parents give them.
      fitness = (*cells_iter)->get_fitness() + basechance;
    }
    else fitness = 0;
    double fitval = fitness/fitness_average ;
      totalfilled +=  fitval/double(celllistsize); // last term is added base chance, which
//adds up to 0.01 for the total and makes the total value come to celllistsize + .1, which is why the total term has to be divided by that. 
    }
    fitnessarray[teller] = totalfilled;
    oldpopulationarray[teller] = *cells_iter;
    teller +=1;
  }
  cout << "totalfilled" << totalfilled << std::endl;
  double pullednumber = 0;
  int arraycheck = 0; 
  for (int t1 = 0; t1 < celllistsize; t1++)
  {
    arraycheck = 0;
    pullednumber = evo_rand.uniform();
    arraycheck = int(celllistsize*pullednumber);
    if (arraycheck == celllistsize) {
      arraycheck -=1; //to make sure that if the randomnumbergenerator reaches 1, you don't go looking past the final array value.
      cout << "pullednumber can reach 1" << std::endl;
    }
    if( pullednumber >= fitnessarray[arraycheck])
    {
      while(((arraycheck +1) < celllistsize) && ( pullednumber > fitnessarray[arraycheck]))
      {
        arraycheck +=1;

      }
    }
    else
    {
       while (pullednumber < fitnessarray[arraycheck])
       {
         arraycheck -=1;
	 if( arraycheck == -1) break; //because there is no [-1] value and you know you're at the lowest there.

       }
       arraycheck +=1;
    }
    Cell * child = new Cell(*oldpopulationarray[arraycheck],savedirectory); 
    //copy constructor sets parent and child relation and push
    //them up front
    addCell(child);  
    int previousarraycheck;
    if (arraycheck == 0) previousarraycheck =1;
    else previousarraycheck = arraycheck-1;
  }
  cout << "updating ancestors" << std::endl;
  updateAncestors(temp_begin);
  cout << "erasing ancestor cells" << std::endl;
  cells.erase(temp_begin,cells.end());
  cout << "reproduction done" << std::endl;
  pop_size = cells.size();

}
void PetriDish::reproduce2(){               //fitness proportional reproduction

  // temporary list of potentials, constructed on basis of regular fitness
  list<const Cell *> potentials; 

  list<Cell*>::iterator temp_begin = cells.begin();
  bool reproduce = true  ;                 
  if(cells.size() <= 0) reproduce = false;  
  while(reproduce){
    for(cells_iter = temp_begin; cells_iter != cells.end(); cells_iter++){
      if(potentials.size()>= pop_size){
	reproduce = false;
	break;
      }
      if(evo_rand.uniform() < (0.5 / fitness_average)* (*cells_iter)->get_fitness()){
	Cell *  potential = *cells_iter; 
	potentials.push_front(potential);
      }
    }
  }
  // sort in ascending order on their accumulated production ( by
  // means of overloaded <Cell,Cell>operator<
  potentials.sort(Cell_Production_Compare);

  list<const Cell *>::iterator pot_iter = potentials.begin();
  //throw away lower RANK_CUT_OFF fraction of population
  for(int i = 1; pot_iter != potentials.end();i++, pot_iter++){
    std::cout <<     (*pot_iter) -> get_production() << std::endl;
    if(double(i)/double(pop_size) >= (1. - RANK_CUT_OFF) ) break;  
  }
  // potentials.erase(potentials.begin(),pot_iter);
  potentials.erase(potentials.begin(),pot_iter);
  // start actual reproduction from cells in the reduced potentials
  // list;
  reproduce = true;
  while(reproduce){
    for(pot_iter = potentials.begin();pot_iter != potentials.end()  ;pot_iter++ ){
      if(cells.size()>= 2*pop_size){
	reproduce = false;
	break;
      }
      // draw from the potentials with equal chance
      if(evo_rand.uniform() < 0.5){ 
	//since we loop over this list at least 3.33 times to refill
	//the population, we can pick with fairly large chance and
	//still add stochasticity 
	Cell * temp = const_cast<Cell *>(*pot_iter);
 	Cell * child = new Cell(*temp,savedirectory); 
	//copy constructor sets parent and child relation and push them up front
	addCell(child);
      }
    }
  }
  updateAncestors(temp_begin);
  cells.erase(temp_begin,cells.end());
  pop_size = cells.size();
}


// take the first root as a template for the total population reset
// variables & empty pop_lists to start from initial conditions
void PetriDish::clone_ancestor(int clone_gen){
  generations = 0;
  cells.clear();
  Cell::total_number_cells = 0;
  Cell * blueprint = roots.front();  
  int at_generation=0;
  while(at_generation < clone_gen){
    blueprint = blueprint -> getChildren().front();
    at_generation ++;
  }
  Gene::total_nmbr_genes = blueprint -> gene_nmbr_max();

  while(cells.size()< pop_size){
    Cell * clone = new Cell(*blueprint,1,savedirectory); 
    addCell(clone);
  }
  for(ancestor_iter = ancestors.begin() ; ancestor_iter != ancestors.end() ; ancestor_iter++){
    delete *ancestor_iter;
  }
  ancestors.clear();
  roots.clear();
  saveParams();
}


void PetriDish::set_Aout(double change_freq){
  Achanged = false;
  if(env_rand.uniform() < change_freq){
    Achanged = true;
    if(ENV_SKEWED)
      //normal2 takes normal distributed random nrs with mean -0.3 and
      //st dev 1.3 ( modus: A_out = 0.5 )
      A_out = pow(10, env_rand.uniform4());      
    else
      A_out = pow(10, env_rand.uniform3());     
  }
}

void PetriDish::set_large_Aout_switch(){ // symbolises a large change in resource concentration.
    if(ENV_SKEWED)
      //normal2 takes normal distributed random nrs with mean -0.3 and
      //st dev 1.3 ( modus: A_out = 0.5 )
      A_out = pow(10, env_rand.uniform4());      //TODO?: change distribution  
    else
      A_out = pow(10, env_rand.uniform3());      //TODO?: change distribution  

}

void PetriDish::set_small_Aout_depletion(double depletion_rate){ //symbolises the depletion of resource caused by the population. Happens continously but in relatively small steps.
   A_out = ((env_rand.uniform() *A_out *depletion_rate ) + ((1.0- depletion_rate)*A_out)); //TODO?: change distribution
   Achanged = true;    
}

void PetriDish::set_Aout_3env(double change_freq){
  Achanged = false;
  if(env_rand.uniform() < change_freq){
    double A_new = A_out ;
    if(env_rand.uniform()< 0.33) {
      A_new = 0.1;
    }
    else if(env_rand.uniform()< 0.667) {
      A_new = 1.0;
    }
    else{
      A_new = 10.0;
    }
    if(A_new != A_out) Achanged = true;
    A_out = A_new;
  }  
}



void PetriDish::step_sparse(){
  generations++;
  Time = 0;
  _time= 0.;
  double old_H=H;
  //for random environmental changes over time. Otherwise they're off
    for (int gen_timer = 0; gen_timer<ASSAYS;gen_timer++)
    {
      if (depleting_resource){
        if(env_rand.uniform() < change_rate)
        {
          set_large_Aout_switch();
        }
        else
        {
          set_small_Aout_depletion(depletion_rate);
        }
      }
      else if(_3ENV)
        {
          set_Aout_3env(change_rate);
        }
        else
        {
      	  set_Aout(change_rate);
        }
      cout << "A_out = " << A_out << endl;
      seen_environments.push_back(make_pair(generations,A_out));
      if(RESET) resetConc(cells);
      for(; _time < (gen_timer+1)*double(LIFETIME);){
        updateStep(cells);
      }
      if ((gen_timer+1)<ASSAYS)
      {
        H=old_H;
        storeFitness(cells,(gen_timer));
        storeProduction(cells,(gen_timer));
      }
    }
    storeScaledFitness(cells);
    storeReproduction(cells);
    saveFitness_Max_Average();
    double A_out_temp;
    if(generations % ANALYZE_TIME == 0 || generations == 1) {
      A_out_temp = A_out;
      AnalyseFitness(cells,1);
      A_out = A_out_temp;
    }
    GracePlot();
    if(generations % SAVETIME == 0){
      A_out_temp = A_out;
      ostringstream formatter;
      formatter << "ver3_generation" << generations;
      findRoots();
      drawAncestorTree(SAVETIME * drawings,generations,TREE_CUTOFF);
      buildAncestors(generations);
      if (ENV_SWITCHING && reached_fitness){
        formatter << "S";
        CHANGE_ENV = true;
        reached_fitness = false;
      }
      SaveDish(formatter.str());
      A_out = A_out_temp;
    }
  }

void PetriDish::run_sparse(int cycles){
  if(_3ENV) set_Aout_3env(1.0);                            
  else set_Aout(1.0);
  for(int i = 0; i < cycles; i++){
    step_sparse();
    if(REPRODUCE == 2 || DEATH) reproduce2();
    else reproduce();
    mutateCells();
  }
}

void PetriDish::step(){       // the main Run method
    double old_H=H;
    generations++;
    Time = 0;
    _time=0;
    //we have to check for RESET here, because at birth, cells take
    //the concentration of their parent
    if(RESET) resetConc(cells);
  
    cout << "A_out = 0.1 " << endl;
    A_out = 0.1;  
    for(;_time < double(LIFETIME);){
      updateStep(cells);
    }
    H=old_H;
    storeFitness(cells,0);                        //Store fitness (no dynamic fitness assay)
    storeProduction(cells,0);
    if(RESET) resetConc(cells);

    cout << "A_out = 1. " << endl;
    A_out = 1. ;                            // A to 1.
    for(;_time < 2*double(LIFETIME);){
      updateStep(cells);
    }    
    H=old_H;
    storeFitness(cells,1);
    storeProduction(cells,1);
    if(RESET) resetConc(cells);

    cout << "A_out = 10. " << endl;
    A_out = 10.;                            // A to 10.
    for(;_time < 3*double(LIFETIME);){
      updateStep(cells);
    }    
    H=old_H;
    storeFitness(cells,2);
    storeScaledFitness(cells);
    storeProduction(cells,2);
    storeReproduction(cells);
    saveFitness_Max_Average();
    GracePlot();
    
    if(generations % SAVETIME == 0){
      ostringstream formatter;
      formatter << "ver3_generation" << generations;
      findRoots();
      drawAncestorTree(SAVETIME * drawings,generations,TREE_CUTOFF);
      buildAncestors(generations);
      if (ENV_SWITCHING && reached_fitness){
	formatter << "S";
	CHANGE_ENV = true;
	reached_fitness = false;
      }
      SaveDish(formatter.str());      
    }
}

void PetriDish::DishParamUpdate()
  {
   cout <<"in this parameterupdate, generation was" << generations <<std::endl;
   param_option_vector.push_back(new Parameter_Option_Set(generations, pop_size, Time, mutationrate, change_rate, evo_seed_used, env_seed_used, TARGETA, TARGETX, Perm, N, Degr));
   param_option_vector.back() -> set_generation(generations); 
   param_option_vector.back() -> set_populationsize(pop_size);
   param_option_vector.back() -> set_Time(Time);
   param_option_vector.back() -> set_mutationrate(mutationrate);
   param_option_vector.back() -> set_change_rate(change_rate);
   param_option_vector.back() -> set_evo_seed(evo_seed_used);
   param_option_vector.back() -> set_env_seed(env_seed_used);
   param_option_vector.back() -> set_Target_A(TARGETA);
   param_option_vector.back() -> set_Target_X(TARGETX);
   param_option_vector.back() -> set_Perm(Perm);
   param_option_vector.back() -> set_N(N);
   param_option_vector.back() -> set_Degr(Degr);
   

/*
   std::vector<double> temporary_vector;
   temporary_vector.push_back(generations);
   temporary_vector.push_back(pop_size);
   temporary_vector.push_back(Time);
   temporary_vector.push_back(mutationrate);
   temporary_vector.push_back(change_rate);
   temporary_vector.push_back(evo_seed_used);
   temporary_vector.push_back(env_seed_used);
   temporary_vector.push_back(TARGETA);
   temporary_vector.push_back(TARGETX);
   temporary_vector.push_back(Perm);
   temporary_vector.push_back(N);
   temporary_vector.push_back(Degr);
   param_option_vector.push_back(temporary_vector); */
   /*generation_set.push_back(generations);
   pop_size_set.push_back(pop_size);
   Time_set.push_back(Time);
   mutationrate_set.push_back(mutationrate);
   change_rate_set.push_back(change_rate);
   evo_seed_used_set.push_back(evo_seed_used);
   env_seed_used_set.push_back(env_seed_used);*/

  }

void PetriDish::run(int cycles){
  for(int j = 0; j< cycles; j++){
    step();
    if(REPRODUCE == 2|| DEATH) reproduce2();
    else    reproduce();                    //calculate fitness of whole run and reproduce
    mutateCells();                          //do a mutationstep per cell  
  }
}


void PetriDish::AnalysePop(){               
// method to analyse the last generation + it's ancestor tree in terms
// of concentration time plots and network graphs
  cout << "Analysing " << endl;            
  Time = 0;
  _time=0.;
  findRoots();
  buildAncestors(generations-TREE_CUTOFF);
  if(!LOWPROFILE)
    drawGraphs(ancestors);                           
  //start by drawing all the cells graphs
  
  if(!LOWPROFILE){
    double old_H = H;
    resetConc(ancestors);
    PrintStep(ancestors);                            //print the start concentrations
    cout << "A_out = 0.1 " << endl;
    A_out = 0.1;                            //set environment A conc to 0.1
    for(;_time < double(LIFETIME);){
      AnalyseStep(ancestors);                        //does updatesteps & printing for the whole ancestors-tree
    }
    H = old_H;
    resetConc(ancestors);
    //print the start concentrations
    PrintStep(ancestors);  
    cout << "A_out = 1. " << endl;
    A_out = 1. ;                            // A to 1.
    for(;_time < 2*double(LIFETIME);){
      AnalyseStep(ancestors);
    }    
    H = old_H;
    resetConc(ancestors);
    //print the start concentrations
    PrintStep(ancestors);
    cout << "A_out = 10. " << endl;
    A_out = 10.;                            // A to 10.
    for(;_time < 3*double(LIFETIME);){
      AnalyseStep(ancestors);
    }
    H = old_H;    
  }
}

void PetriDish::AnalyseFitness(list<Cell *> & list,int opt ){
  Time = 0;
  _time = 0;
  double old_H = H;
  cout << "A_out = 0.1 " << endl;
  A_out = 0.1;                            //set environment A conc to 0.1
  resetConc(list);
  for(;_time < double(LIFETIME);){
    updateStep(list);
  }
  H = old_H;
  storeFitness(list,0);                        //Store fitness (no dynamic fitness assay)
  storeProduction(list,0);
  store_expression_A_X(list,0);

  cout << "A_out = 1. " << endl;
  A_out = 1. ;                            // A to 1.
  resetConc(list);
  for(;_time < 2*double(LIFETIME);){
    updateStep(list);
  }    
  H = old_H;
  storeFitness(list,1);
  storeProduction(list,1);
  store_expression_A_X(list,1)  ;

  cout << "A_out = 10. " << endl;
  A_out = 10.;                            // A to 10.
  resetConc(list);
  for(; _time < 3*double(LIFETIME);){
    updateStep(list);
  }    
  H = old_H;
  storeFitness(list,2);
  storeProduction(list,2);
  store_expression_A_X(list,2);

  double average = 0;
  double fitness_max = 0;
  double split_average01 = 0;
  double split_average1 = 0;
  double split_average10 = 0;

  double split_anabolite_average01 = 0.;
  double split_anabolite_average1 = 0.;
  double split_anabolite_average10 = 0.;
  double total_anabolite_average = 0.;

  double max = 0;
  double prod_average = 0;
  double prod_max = 0;
  string filename;

  if(opt == 2){
    ostringstream formatter ;
    formatter << savedirectory << "tree_expr_data_generation" << generations << ".dat";
    filename = formatter.str();
    fout.open(filename.c_str(), ios_base::out); 
    fout << " " << setw(13) << "fitness@0.1" << setw(13) << "fitness@1.0" << setw(13) << "fitness@10." 
	 << setw(13) << "A_in@0.1" << setw(13) << "X_in@0.1"  
	 << setw(13) << "A_in@1.0" << setw(13) << "X_in@1.0" 
	 << setw(13) << "A_in@10." << setw(13) << "X_in@10." << std::endl;
  }
  if(opt == 3){
    ostringstream formatter ;
    formatter << savedirectory << "leaves_expr_data_generation" << generations << ".dat";
    filename = formatter.str();
    fout.open(filename.c_str(), ios_base::out); 
    fout << setw(20) << "gene_id" << setw(13) << "fitness@0.1" << setw(13) << "fitness@1.0" << setw(13) << "fitness@10." 
	 << setw(13) << "A_in@0.1" << setw(13) << "X_in@0.1"  
	 << setw(13) << "A_in@1.0" << setw(13) << "X_in@1.0" 
	 << setw(13) << "A_in@10." << setw(13) << "X_in@10." << std::endl;
  }
  for(cells_iter= list.begin(); cells_iter != list.end(); cells_iter++){
    double fitness = FitnessFunction( (*cells_iter) -> FitnessAssay() );
    if (fitness > fitness_max) fitness_max = fitness;
    double splitfitness01 = FitnessFunction( (*cells_iter) -> splitFitness(0) );
    double splitfitness1 =  FitnessFunction( (*cells_iter) -> splitFitness(1) );
    double splitfitness10 = FitnessFunction( (*cells_iter) -> splitFitness(2) );

    double split_a01 =  (*cells_iter) -> splitAnabolite(0);
    double split_a1 = (*cells_iter) -> splitAnabolite(1);
    double split_a10 = (*cells_iter) -> splitAnabolite(2);

    if(opt == 2 || opt == 3){
      double A_in01 = ((*cells_iter) -> split_expression(0) ).first;
      double X_in01 = ((*cells_iter) -> split_expression(0) ).second;
      double A_in1 = ((*cells_iter) -> split_expression(1) ).first;
      double X_in1 =  ((*cells_iter) -> split_expression(1) ).second;
      double A_in10 = ((*cells_iter) -> split_expression(2) ).first;
      double X_in10 =  ((*cells_iter) -> split_expression(2) ).second;
      fout << setw(20) << (*cells_iter) -> getnmbr() << setw(15) << splitfitness01 << setw(13) << splitfitness1
	   << setw(13) << splitfitness10 << setw(13) << A_in01 << setw(13) << X_in01 << setw(13) << A_in1 
	   << setw(13) << X_in1 << setw(13) << A_in10 << setw(13) << X_in10 << std::endl;
    }
    else{
      average += fitness;
      split_average01 += splitfitness01;
      split_average1 += splitfitness1;
      split_average10 += splitfitness10;
      if(fitness > max) max = fitness;
      
      split_anabolite_average01 += split_a01;
      split_anabolite_average1 += split_a1;
      split_anabolite_average10 += split_a10;


      double production = (*cells_iter) -> ReproductionAssay();
      prod_average += production;
      if(production > prod_max) prod_max = production;
    }
  }
  if(opt == 2 || opt == 3)
    fout.close();

  if(opt == 1){
    average /= list.size();
    split_average01 /= list.size();
    split_average1 /= list.size();
    split_average10 /= list.size();
    prod_average /= list.size();

    split_anabolite_average01 /= list.size();
    split_anabolite_average1 /= list.size();
    split_anabolite_average10 /= list.size();
    total_anabolite_average = split_anabolite_average01 + split_anabolite_average1 +
      split_anabolite_average10;

    filename = savedirectory + "final_fitnesses.dat";
    fout.open(filename.c_str(), ios_base::out | ios_base::app);
    fout << setw(13) << generations << setw(13) << max << setw(13) << average  << setw(13) 
	 << split_average01 << setw(13) << split_average1 << setw(13) << split_average10 << endl  ;
    fout.close();

    filename = savedirectory + "production.dat" ;
    fout2.open(filename.c_str(), ios_base::out | ios_base::app);
    fout2 << setw(13) << generations << setw(13) << prod_max 
	  << setw(13) << prod_average << endl;
    fout2.close();

    grace[4] << generations << max << average 
	     << split_average01 << split_average1 << split_average10 << gracesc::endl;
    grace[5] << generations << log10(total_anabolite_average) << log10(split_anabolite_average01) 
	     << log10(split_anabolite_average1) << log10(split_anabolite_average10) << gracesc::endl;

  }
  if (fitness_max > ENV_CHANGE_FIT_MIN) reached_fitness = true;
}

void PetriDish::drawAncestorTree(int start_gen, int end_gen, int cutoff){
  string savefile = savedirectory + "ancestor_tree";
  fout.open(savefile.c_str(),ios_base::out | ios_base::app);
  if( drawings == 0 ){
    fout << "graph CellTree {" << endl
	 << "node [style=filled]" << endl;
  }
  switch(drawings % 5) {
  case 0:{
    fout << "node [color=green]" << endl; //shape=point,label=\"\",
    fout << "edge [color=green]" << endl;
    break;
  }
  case 1:{
    fout << "node [color=blue]" << endl;
    fout << "edge [color=blue]" << endl;
    break;
  }
  case 2:{
    fout << "node [color=red]" << endl;
    fout << "edge [color=red]" << endl;
    break;
  }
  case 3:{
    fout << "node [color=purple]" << endl;
    fout << "edge [color=purple]" << endl;
    break;
  }
  case 4:{
    fout << "node [color=orange]" << endl;
    fout << "edge [color=orange]" << endl;
    break;
  }
  }
// we draw the tree up till depth: generation - TREE_CUTOFF 
// to prevent "tree explosion" at ends of branches
// we start writing nodes + edges to the tree file at the end point of a previous
// drawing cycle, to prevent overlapping of the earlier generation cells
  list<Cell *> Nodes(0);
  //  start_gen = start_gen - cutoff ; //savetime * drawings
  //  end_gen = end_gen - (cutoff+1); //generations

  buildTreeList(Nodes,start_gen - cutoff ,end_gen - (cutoff +1));
  list<Cell *>::iterator node_iter; 
  for(node_iter = Nodes.begin(); node_iter != Nodes.end(); node_iter ++){
    list<Cell *> children = (*node_iter) -> getOffspring();
    list<Cell *>::iterator child_iter;
    for(child_iter = children.begin(); child_iter != children.end(); child_iter++){
      fout << (*node_iter) -> getnmbr() << "--" << (*child_iter) -> getnmbr() << endl;
    }
  }
  fout.close();

  Nodes.clear();
  buildTreeList(Nodes,start_gen - cutoff + 1,end_gen - cutoff);
  AnalyseFitness(Nodes,2);
  
  Nodes.clear();
  start_gen = generations - cutoff;
  end_gen = generations - cutoff;
  buildTreeList(Nodes,end_gen - cutoff,end_gen - cutoff);
  AnalyseFitness(Nodes,3);

  drawings++;
}

const void PetriDish::findRoots(){
  if(roots.size() == 1) { 
    std::cout << "only 1 root left, sticking with it" << std::endl;
    return;
  }
  roots.clear();
  bool insert;
  int root_count = 0;
  for(cells_iter = cells.begin(); cells_iter != cells.end(); cells_iter++){
    insert = true;
    Cell * root = (*cells_iter)-> findRoot();
    for(roots_iter = roots.begin(); roots_iter != roots.end() ; roots_iter++){
      if(*roots_iter == root ){
	insert = false;
	break;
      }
    }
    if(insert){
      roots.push_front(root);
      root_count++;
    }
  }
  std::cout << "number of roots: " << root_count << std::endl;
}

void PetriDish::buildTreeList(list<Cell *> & tree_list, int start_gen, int end_gen){
  int depth = 1;
  for(roots_iter = roots.begin(); roots_iter != roots.end(); roots_iter++){
    addNodes(tree_list, *roots_iter, start_gen, end_gen, depth);
  }
}

void PetriDish::addNodes(list<Cell *> & tree_list, Cell* c,
	      int start_gen, int end_gen, int depth){
  if(depth > end_gen) return;
  if(depth >= start_gen) 
    tree_list.push_front(c);

  list<Cell *> children = c -> getOffspring();
  list<Cell *>::iterator child_iter;
  for(child_iter = children.begin(); child_iter != children.end(); child_iter++){
    addNodes(tree_list, *child_iter, start_gen, end_gen, depth+1); 
  }
}

void PetriDish::buildAncestors(int end_gen){
  ancestors.clear();
  buildTreeList(ancestors,1,end_gen);
}

// find the last common ancestor of current population and return the distance from present population 
int PetriDish::distanceLUCA(){
  int distance = generations;
  Cell * newLUCA;
  int birthdate;

  if(LUCA.first == NULL){
    roots.clear();
    ancestors.clear();
    findRoots();
    if( roots.size() > 1) return distance;
    else{
      newLUCA = roots.front();
      birthdate = 1;
      std::cout << "adding new luca from roots list, sized: " << roots.size() << std::endl;
    }
  }
  else{
    newLUCA = LUCA.first;
    birthdate = LUCA.second;
  }

  while ( birthdate < generations ){
    list<Cell *> children = newLUCA -> getOffspring();
    if( children.size() == 1){
      newLUCA = children.front();
      birthdate++;
    }
    else break;
  }
  LUCA = make_pair(newLUCA,birthdate);
  distance = generations - birthdate;
  return distance;
}

int PetriDish::distanceFittest(Cell * current_fittest){
  int distance = 0;
  if(generation_best.first != NULL){
    Cell * old_ancestor = generation_best.first;
    int birthdate = generation_best.second;
    Cell * current_ancestor = current_fittest;
    for(int gen = generations ; gen > birthdate; gen--){
      current_ancestor = current_ancestor -> getAncestor();
    }
    do{
      if(old_ancestor == NULL){
	distance = generations;
	break;
      }
      old_ancestor = old_ancestor -> getAncestor();
      current_ancestor = current_ancestor -> getAncestor();
      distance ++;
    }while (old_ancestor != current_ancestor);
    if( (generation_best.first) ->noChildren() ){
      (generation_best.first) -> informParent();
      delete generation_best.first;
    }
  }
  return distance;
}

void PetriDish::GracePlot(){
  string command;
  if(grace.open()){
	cout << "first data sent to graceplot" << std::endl;
    grace[0].draw();
    grace[0] << "autoscale";
    grace[1].draw();
    grace[1] << "autoscale";
    grace[2].draw();
    grace[2] << "autoscale";
    grace[3].draw();
    grace[3] << "autoscale";
    if(SPARSE){
      grace[4].draw();
      grace[4] << "autoscale";
    }
    
    grace[5].draw();
    grace[5] << "autoscale";
    grace[6].draw();
    grace[6] << "autoscale";
    grace[7].draw();
    grace[7] << "autoscale";

    if(generations % ANALYZE_TIME == 0){
      grace.redraw();
      command = "saveall \""+savedirectory+ "graceplot.agr\"" ;
      grace << command.c_str();
    }
  }
}

void PetriDish::setGrace(){
  string command;
  command = "type xysize";
  grace[3] << command.c_str();
}

void PetriDish::ReconstructNetworks(list<Cell *> & list){
  for(cells_iter = list.begin(); cells_iter != list.end(); cells_iter++){
    (*cells_iter) -> reconstruct_network();
  }
}

void PetriDish::OrderGenes(list<Cell *> & list){
  for(cells_iter = list.begin(); cells_iter != list.end(); cells_iter++){
    (*cells_iter) -> order_genes();
  }
}

void PetriDish::SaveDish(const string filename){
  string save = savedirectory + filename;
  std::ofstream ofs(save.c_str());
  boost::archive::binary_oarchive oa(ofs); 
  //boost::archive::xml_oarchive oa(ofs); 
  cout << "start saving" << endl;
  PetriDish & d = *this;
  oa << BOOST_SERIALIZATION_NVP(d);
  ofs.close();
  cout << "saving done" << endl;
}


void PetriDish::RestoreDish(const string filename, const string dirname,
			    int evo_seed = 0,int env_seed = 0){
  std::ifstream ifs(filename.c_str());
  boost::archive::binary_iarchive ia(ifs); 
  cout << "START RESTORATION " << endl;
  PetriDish & d = *this;
  ia >> d;
  cout << "out of loading" << endl;
  if (filename.substr(0,3) =="ver")
	{
	RestoreParamRead();	
	}
  else
	{
	repairParamRead();
        }
  savedirectory = dirname;
  pop_rand.init(pop_seed_used);   // use same seed as former run.
  if(evo_seed == 0) evo_rand.init(evo_seed_used);
  else{ evo_rand.init(evo_seed); evo_seed_used = evo_seed;}
  if(env_seed == 0)  env_rand.init(env_seed_used);
  else{ env_rand.init(env_seed); env_seed_used = env_seed;}
  ReconstructNetworks(ancestors);           // this includes current pop cells because they
  OrderGenes(ancestors);                    // are included in the ancestors 
  resetConc(cells);                         // reset to start with fixed initial condition
  saveParams();
  
  cout << "RESTORATION DONE" << endl;

}

void PetriDish::RestoreParamRead()
  {
   /*generations = int(generation_set.back());
    pop_size = int(pop_size_set.back());
    Time = int(Time_set.back());
    mutationrate = mutationrate_set.back();
    change_rate = change_rate_set.back();
    evo_seed_used = int(evo_seed_used_set.back());
    env_seed_used = int(env_seed_used_set.back()); */
    //generations = int(param_option_vector.back()[0]); //don't use this because you don't want the generation to be set back to the time of the last change.
    /*pop_size = int(param_option_vector.back()[1]);
    Time = int(param_option_vector.back()[2]);
    mutationrate = param_option_vector.back()[3];
    change_rate = param_option_vector.back()[4];
    evo_seed_used = int(param_option_vector.back()[5]);
    env_seed_used = int(param_option_vector.back()[6]);
    TARGETA = param_option_vector.back()[7];
    TARGETX = param_option_vector.back()[8];
    Perm = param_option_vector.back()[9];
    N = param_option_vector.back()[10];
    Degr = param_option_vector.back()[11];*/
   pop_size = param_option_vector.back() ->get_populationsize();
   mutationrate = param_option_vector.back() ->get_mutationrate();
   change_rate = param_option_vector.back() ->get_change_rate();
   evo_seed_used = param_option_vector.back() -> get_evo_seed();
   env_seed_used = param_option_vector.back() -> get_env_seed();
    
   TARGETA = param_option_vector.back() -> get_Target_A();
   TARGETX = param_option_vector.back() ->get_Target_X();
   Perm = param_option_vector.back() -> get_Perm();
   N = param_option_vector.back() -> get_N();
   Degr = param_option_vector.back() -> get_Degr();
    
  }
void PetriDish::repairParamRead()
  {
   param_option_vector.back() -> set_generation(0); 
   param_option_vector.back() -> set_populationsize(pop_size);
   param_option_vector.back() -> set_Time(0);
   param_option_vector.back() -> set_mutationrate(mutationrate);
   param_option_vector.back() -> set_change_rate(change_rate);
   param_option_vector.back() -> set_evo_seed(evo_seed_used);
   param_option_vector.back() -> set_env_seed(env_seed_used);
   param_option_vector.back() -> set_Target_A(TARGETA);
   param_option_vector.back() -> set_Target_X(TARGETX);
   param_option_vector.back() -> set_Perm(Perm);
   param_option_vector.back() -> set_N(N);
   param_option_vector.back() -> set_Degr(Degr);
  }

void PetriDish::restoreParamRead(int check_generation)
  {
   for(std::vector<Parameter_Option_Set *>::iterator paramopt_iter = param_option_vector.begin()  ; paramopt_iter != param_option_vector.end(); paramopt_iter++)
    {

     if  (   (  (*paramopt_iter) -> get_generation() > check_generation )
	  ||( ( paramopt_iter +1 ) == param_option_vector.end()  )   )
       { 
         if (  (*paramopt_iter) -> get_generation() > check_generation )
	   {
           paramopt_iter --; 

           }
         pop_size = (*paramopt_iter) ->get_populationsize();
         mutationrate = (*paramopt_iter) ->get_mutationrate();
         change_rate = (*paramopt_iter) ->get_change_rate();
         evo_seed_used = (*paramopt_iter) -> get_evo_seed();
         env_seed_used = (*paramopt_iter) -> get_env_seed();
    
         TARGETA = (*paramopt_iter) -> get_Target_A();
         TARGETX = (*paramopt_iter) ->get_Target_X();
         Perm = (*paramopt_iter) -> get_Perm();
         N = (*paramopt_iter) -> get_N();
         Degr = (*paramopt_iter) -> get_Degr();
	 //paramopt_iter ++;

         break;

       }
    }

  }

int PetriDish::readoutEnvNumberUp(int check_generation, int startEnvcounter)
{
  //int t1 = 1;
  std::vector<Parameter_Option_Set *>::iterator paramopt_start = param_option_vector.begin();
 // while (t1<startEnvcounter){
    paramopt_start +=(startEnvcounter-1);
 // }
  
  for(std::vector<Parameter_Option_Set *>::iterator paramopt_iter = paramopt_start; paramopt_iter != param_option_vector.end(); paramopt_iter++)
    {

     if  (   (  (*paramopt_iter) -> get_generation() > check_generation )
	  ||( ( paramopt_iter +1 ) == param_option_vector.end()  )   )
       { 
          break;
       }
    startEnvcounter ++;
    }
  return startEnvcounter; 
}
int PetriDish::readoutEnvNumberDown(int check_generation, int startEnvcounter){
  std::vector<Parameter_Option_Set *>::iterator paramopt_start = param_option_vector.begin();
  paramopt_start +=(startEnvcounter-1);

  for(std::vector<Parameter_Option_Set *>::iterator paramopt_iter = paramopt_start; paramopt_iter != param_option_vector.begin(); paramopt_iter--)
    {

     if  (  (*paramopt_iter) -> get_generation() < check_generation )
       { 
         return startEnvcounter;
       }
    startEnvcounter --;
    }
 if (startEnvcounter ==1) return startEnvcounter;
 else{
   return 1;
   cout << "startEnvcounter is wrong" << std::endl;
 }
}

void PetriDish::restoreParamReadSP(int envCounter){
//  int t1=1;
  std::vector<Parameter_Option_Set *>::iterator paramopt_iter = param_option_vector.begin();
//  while (t1<envCounter){
    paramopt_iter += (envCounter-1);
//  }
  pop_size = (*paramopt_iter) ->get_populationsize();
  mutationrate = (*paramopt_iter) ->get_mutationrate();
  change_rate = (*paramopt_iter) ->get_change_rate();
  evo_seed_used = (*paramopt_iter) -> get_evo_seed();
  env_seed_used = (*paramopt_iter) -> get_env_seed();
    
  TARGETA = (*paramopt_iter) -> get_Target_A();
  TARGETX = (*paramopt_iter) ->get_Target_X();
  Perm = (*paramopt_iter) -> get_Perm();
  N = (*paramopt_iter) -> get_N();
  Degr = (*paramopt_iter) -> get_Degr();
  
}

void PetriDish::exitGrace(){
  cout << "exiting grace early" << std::endl;
  if(grace.open()){
    string command = "saveall \""+savedirectory+ "graceplot.agr\"" ;
    grace << command.c_str();
    grace.close();
    cout << "grace done" << endl;
  }
}

BOOST_CLASS_VERSION(PetriDish, 2);

ostream & operator << (ostream & out,PetriDish & p){
  out<< p.toString();
  return out;
};

bool Cell_Production_Compare (const Cell* const& c1, const Cell* const& c2){
  return c1 -> get_production() < c2 -> get_production();
}

bool DishSortPredicate(const PetriDish * lhs, const PetriDish* rhs)
{
  return lhs->get_generations() < rhs->get_generations();
}

