#include "spatial_dish.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
//#include "gracesc.h"
#include <boost/tokenizer.hpp>
#include <boost/tuple/tuple.hpp>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

namespace bf = boost::filesystem;
namespace po = boost::program_options;

//#define MUTATION_RATE 0.05
//#define CHANGE_RATE 0.4


TYPE2** Dish;
void leave(int sig);
//extern pid_t pid;
SpatialDish::SpatialDish(int nmbr_cells,int genes,string savedir, 
			 string grace_file,int enable = 0,
			 int pop_seed =2,int evo_seed =2,
			 int env_seed = 2, int graphs = 8,
			 double mut_rate = 0.05,double ch_rate = 0.2){
  p = new PetriDish(nmbr_cells,genes,savedir+'/',grace_file,
		    enable,pop_seed,evo_seed,env_seed,
		    graphs,mut_rate,ch_rate);
}

// constructor to create a dish full of clones of an ancestor of a given generation
SpatialDish::SpatialDish(int clone_generation,int nmbr_cells,string dishsave, string savedir,string grace_file,int enable, int evo = 0, int env = 0){
  p = new PetriDish(grace_file,enable);
  p -> RestoreDish(dishsave,savedir+'/',evo,env);
  p -> set_pop_size(nmbr_cells);
  p -> clone_ancestor(clone_generation);
  p -> mutateCells(); // to introduce some variety in the population, avoiding getting stuck on 0 fitness
}
//dish that starts by loading and restoring a previously run petridish.
SpatialDish::SpatialDish(string dishsave, string savedir,string grace_file,int enable, int evo = 0, int env = 0){
  p = new PetriDish(grace_file,enable);
  p -> RestoreDish(dishsave,savedir+'/',evo,env);
  p -> set_change_rate(CHANGE_RATE);
  p -> set_mutationrate(MUTATION_RATE);
}

void SpatialDish::place_cells(TYPE2** plane){
  list<Cell *>::iterator cells_iter = p -> get_cells_iter();
  list<Cell *> cells = p -> get_cells();
  cells_iter = cells.begin() ;
  for(int i=1;i<=nrow;i++)
    for(int j=1;j<=ncol;j++){
      if(cells_iter == cells.end()){
	std::cout << "not enough cells to fill the plane " << std::endl;
	break;
      }
      AttachCell(plane,i,j, *cells_iter,p);
      cells_iter++;
    }
  if(cells_iter != cells.end())
    std::cout << "not all cells placed on the plane" << std::endl;
}

// namespace po = boost::program_options;

// void conflicting_options(const po::variables_map& vm, 
//                          const char* opt1, const char* opt2)
// {
//     if (vm.count(opt1) && !vm[opt1].defaulted() 
//         && vm.count(opt2) && !vm[opt2].defaulted())
//         throw logic_error(string("Conflicting options '") 
//                           + opt1 + "' and '" + opt2 + "'.");
// }

// /* Function used to check that of 'for_what' is specified, then
//    'required_option' is specified too. */
// void option_dependency(const po::variables_map& vm,
//                         const char* for_what, const char* required_option)
// {
//     if (vm.count(for_what) && !vm[for_what].defaulted())
//         if (vm.count(required_option) == 0 || vm[required_option].defaulted())
//             throw logic_error(string("Option '") + for_what 
//                               + "' requires option '" + required_option + "'.");
// }

 

extern int poseG;
extern int stopG;
//extern PetriDish * cash_dish;
int main (int argc, char *argv[])
{
  try{

    /* increase stack size to avoid stack overflow when serializing */
    const rlim_t kStackSize = 1024L * 1024L * 1024L;   // min stack size = 64 Mb
    struct rlimit rl;
    int result;

    result = getrlimit(RLIMIT_STACK, &rl);
    if (result == 0)
    {
      if (rl.rlim_cur < kStackSize)
        {
	  std::cout << "rlimit: " << rl.rlim_cur  << "smaller than " << kStackSize << "; resetting" << std::endl;
	  rl.rlim_cur = kStackSize;
	  result = setrlimit(RLIMIT_STACK, &rl);
	  if (result != 0)
            {
	      fprintf(stderr, "setrlimit returned result = %d\n", result);
            }
        }
    }
    

    string config_file = "/home/thocu/Projecten/VirtualCell/WorkBench/config_files/s_config.cfg";
    string change_config = "./change_config.cfg";
    string local_config = "local_config.cfg";
    int generations,dimension,genes,pop_seed,
      evo_seed,env_seed,switch_seed,clone_gen;
    bool depleting_resource;
    double depletion_rate;
    bool plotting,space;
    int env_change_time;
    bool env_system_switch;
    int env_switch_time;
    double env_switch_rate;
    int switch_delay;
    bool no_config = false;
    bool defaults = false;
    int lastenvironment = 0;
    vector<vector<std::pair<std::string, double> > >environmentset;
    vector<vector<boost::tuple<std::string, double, double> > >envrateset;
    string save_dir,load_file;
    string grace_par = "/home/jaap/project_2/8field.par";

    //********************* Option Specification ***********************//

    po::options_description generic("Generic");
    generic.add_options()
      ("help", "help on standard options")    
      ("Help-Module", po::value<string>(), "produce a help for a given module\n"
       "modules: \n"
       "  Cell-Parameters: \tset cell specific parameters \n"
       "  Dish-Parameters: \tset experiment specific parameters \n"
       "  Output-Configuration: \tconfigure output"
)
      ("all-help", "help on standard and hidden options")
      ;

    po::options_description cell_visible;
    //[visible] INS, DUP, DEL, POINT, 
    cell_visible.add_options()
      ("mutation-ratios,r", po::value<string>()->default_value("1:1.5:1.5:5"),"ratio INS:DUP:DEL:POINT")
      ("wgd-param", po::value(&WGD)->default_value(0.001), "parameter determines chance of wgd per round of mutations")
      ("mutation-scale-ratios,R", po::value<string>()->default_value("4:4:1:1"),"ratio GCR:SG:POLYP:NOTHING")
      ("gcr-maximum-fraction,F", po::value(&MAX_GCR_FRAC)->default_value(0.5),"maximum fraction of total genome length affected by GCR")
      ;
    
    po::options_description cell_hidden;
    //[hidden]  N, Perm, Degr, startAin, startXin, TARGETA, TARGETX, ASSAYS, TF_RAT, PUMP_RAT, ENZ_RAT, CHANGE_TRHESHOLD
    cell_hidden.add_options()
       ("conversion-rate", po::value(&N)->default_value(4.), "rate of conversion A to X")
      ("passive-diffusion", po::value(&Perm)->default_value(0.1),"passive diffusion of A over cell membrane")
      ("enzyme-start-concentration", po::value(&STARTCONC)->default_value(1.), "start concentration of gene products")
      ("start-A-in", po::value(&startAin)->default_value(0.5),"start concentration A")
      ("start-X-in", po::value(&startXin)->default_value(0.5),"start concentration X")
      ("target-A-in", po::value(&TARGETA)->default_value(1.), "target A concentration")
      ("target-X-in", po::value(&TARGETX)->default_value(1.), "target X concentration")
      ("gene-ratios", po::value<string>()->default_value("2.:1.:1."), "ratio TFs:PUMPs:ENZYMEs")
      ("assays", po::value(&ASSAYS)->default_value(3), "number of environments per generation")
      ("enzyme-degradation", po::value(&Degr)->default_value(1.), "degradation rate of gene products")
      ("geometric-dist-length", po::value(&GEO_DIST_LENGTH)->default_value(0.1), "parameter to set up probability density function for stretch-lengths")
      ("error-rate", po::value(&ERROR_RATE)->default_value(.95), "if errors in pop above this fraction, reduce step size")
      ("step-reduction", po::value(&STEP_REDUCTION)->default_value(10.), "fold reduction in step size upon integration error dection")
   
      //      ("exponential_dist_events", po::value(&EXP_DIST_EVENTS)->default_value(0.5), "parameter to set up probability density function for number of events")
      ;
    po::options_description cell_config("Configure Cell paramters");
    cell_config.add(cell_visible).add(cell_hidden);


    po::options_description dish_visible;
    dish_visible.add_options()
      ("generations", po::value(&generations)->default_value(10000),"number of generations to simulate")
      ("dimension", po::value(&dimension)->default_value(32), "dimension^2 = population size, must be an even number")
      ("number-of-genes", po::value(&genes)->default_value(10), "average genome size of initial population")
      ("pop-seed", po::value(&pop_seed), "seed for initializing population")
      ("evo-seed", po::value(&evo_seed)->default_value(1), "seed for mutation operations")
      ("env-seed", po::value(&env_seed)->default_value(1), "seed for the (changing) resource concentration")
      ("switch-seed", po::value(&switch_seed)->default_value(1), "seed for the (changing) environment if switching is rate based." )
      ("clone-pop,C", po::value(&clone_gen),"clone a common ancestor to form a new population")
      ("mutation-rate,m", po::value(&MUTATION_RATE)->default_value(0.05), "per gene mutation rate")
      ("change-rate,h", po::value(&CHANGE_RATE)->default_value(0.4),"resource concentration change rate ")
      ("sparse,s", po::bool_switch(&SPARSE)->default_value(false), " ")
      ("depleting-resource", po::bool_switch(&depleting_resource)->default_value(false), "if sparse, resource depletes over time")
      ("depletion-rate", po::value(&depletion_rate)->default_value(0), "speed at which resource depletes per timestep")
      ("mutation-type,t", po::value(&PER_GENE_MUTATION)->default_value(3),
       "Values: \n"
       "  1: \tno major mutations \n"
       "  2: \tmajor mutations, no ordered genome \n"
       "  3: \tmajor mutations on ordered genome \n"
       "  4: \tsmall scale geometrically distributed mutations with wgd \n"
       "  5: \tno major mutations; with wgd \n"
       "  6: \tmajor mutations with wgd \n"
       "  7: \tsmall scale + major mutations + wgd, following mutation-scale-ratios"
       )
      ("production,p", po::bool_switch(&DEATH)->default_value(false), "production is an additional fitness criterium")
      ("space,S", po::bool_switch(&space)->default_value(false), "add spatial structure to the population")
      ("env-switching,e",po::value(&ENV_SWITCHING)->default_value(false), "switch to new environmental config upon reaching fitness criterium")
      ("env-system-switch",po::value(&env_system_switch)->default_value(false), "switch to a predetermined set of new environments")
      ("env-switch-time", po::value(&env_switch_time)->default_value(0),"switch between preset environments after this many generations")
      ("env-switch-rate", po::value(&env_switch_rate)->default_value(0),"switch between preset environments with this chance")
      ("env-change-time",po::value(&env_change_time)->default_value(6000),"switch environment at this time, if fitness was not reached")
      ("env-config", po::value(&change_config)->default_value("0"),"new environment config file")
      ("switch-fit-min", po::value(&ENV_CHANGE_FIT_MIN)->default_value(0.85), "switch environment upon reaching this fitness value")
      ("switch-delay",po::value(&switch_delay)->default_value(0),"delay switching the environment, after fitness threshold was reached")
      ;
    po::options_description dish_hidden;
    dish_hidden.add_options()
      ("lifetime", po::value(&LIFETIME)->default_value(1000), "allowed time for the cells to equilibriate their internal state")
      ("skewed-environment", po::bool_switch(&ENV_SKEWED), "skew environment distribution towards lower lower concentrations")
      ("3-environments", po::bool_switch(&_3ENV), "when running sparse, use standard environments only")
      ("change-threshold", po::value(&CHANGE_THRESHOLD)->default_value(0.001), "fraction in concentration change considere no change")
      ("rank-cut-off", po::value(&RANK_CUT_OFF)->default_value(0.3), "selected fraction of production-ranked population" )
      ("integration-step", po::value(&H)->default_value(0.1), "Euler integration step size")
      ("no-target-conc", po::bool_switch(&NO_TARGET_CONC)->default_value(false), "no target concentration as fitness criterium")
      ("restrict-size", po::bool_switch(&RESTRICT_SIZE)->default_value(false), "restrict genome size by setting a penalty on raw fitness")
      ("size-max", po::value(&SIZE_MAX)->default_value(500),"genome size above which a penalty will be applied")
      ("size-penalty", po::value(&SIZE_PENALTY)->default_value(0.1),"penalty per gene on raw fitness data")
      ("reset", po::value(&RESET)->default_value(true),"reset concentrations")
      ;
    po::options_description dish_config("Configure Dish parameters");
    dish_config.add(dish_visible).add(dish_hidden);

    po::options_description output_visible;
    output_visible.add_options()
      ("plotting,g", po::bool_switch(&plotting)->default_value(false), "give visual online grace output")
      ;
    po::options_description output_hidden;
    output_hidden.add_options()
      ("low-profile", po::bool_switch(&LOWPROFILE)->default_value(true), "reduce output")
      ("tree-cut-off", po::value(&TREE_CUTOFF)->default_value(50), "don't add nodes of last generations to tree")
      ("save-time", po::value(&SAVETIME)->default_value(500), "save population after this many generation")
      ("analyze-time",po::value(&ANALYZE_TIME)->default_value(10),"analyze population after this many generations")
      ("measure-step", po::value(&MEASURESTEP)->default_value(0.1), "Do measurements after this time interval")
      ("save-dir", po::value(&save_dir), "save output here")
      ("grace-par,x" , po::value(&grace_par)->default_value(grace_par), "grace parameter file")
      ;
    po::options_description output_config("Configure Output options");
    output_config.add(output_visible).add(output_hidden);



    po::options_description config("Configuration");
    config.add_options()
      ("config-file,c", po::value(&config_file)->default_value(config_file), "load parameters from non-default config file")
      ("default-options" , po::bool_switch(&defaults), "print defaulted values in local config file as well")
      ("no-config-file", po::bool_switch(&no_config), "only read options from command line, no default configuration file used")
      ("load-file,l", po::value(&load_file), "dish object file name")
      ("pre-wgd-load-file", po::bool_switch(&BEFORE_WGD)->default_value(false),"refrain from serializing the non-existent wgds parameter in old saves (patch)")
      ;
    config.add(dish_visible).add(cell_visible).add(output_visible);

    po::options_description hidden("Hidden options");
    hidden.add(cell_hidden).add(dish_hidden).add(output_hidden);

    po::options_description visible("Allowed options");
    visible.add(generic).add(config);

    po::options_description cmdline_options;
    cmdline_options.add(generic).add(config).add(hidden);

    po::options_description config_file_options;
    config_file_options.add(config).add(hidden);

    //END********************* Option Specification ***********************END//
  
    po::positional_options_description p;
    p.add("save-dir", 1);
    p.add("generations",1);
    p.add("dimension",1);
    p.add("number-of-genes",1);
    p.add("pop-seed",1);
    p.add("evo-seed",1);
    p.add("env-seed",1);

    po::variables_map vm;
    po::variables_map::iterator vmit;
    //parse command line first to have priority over config file options
    po::store(po::command_line_parser(argc,argv).
	      options(cmdline_options).positional(p).run(),vm);

    po::notify(vm);

    bf::path save_path;
    if(vm.count("load-file")){
      if(!vm.count("save-dir") ){
	save_path = bf::path(load_file);
	save_path /= "../restore_dish/";
	save_path.normalize();
      }
      else
	save_path = bf::path(save_dir);
    }

    bf::path config_path;
    // if we are loading a dish, when nothing else defined, we load the local configuration
    if(vm.count("load-file") && !no_config && vm["config-file"].defaulted()){
      config_path = bf::path(load_file);
      config_path /= "../";
      config_path /= local_config;
      config_path.normalize();
      ifstream ifs(config_path.string().c_str());    
      if(!ifs.fail()){               // check if file exists
	po::store(parse_config_file(ifs,config_file_options), vm);
	std::cout << "loading local configurations" << std::endl;
      }
      else std::cerr << "cannot find '"+ config_path.string() + "'" << std::endl;
    }
    else if(!no_config){
      ifstream ifs(config_file.c_str());    
      if(!ifs.fail()){               // check if file exists
	po::store(parse_config_file(ifs,config_file_options), vm);
      }
      else std::cerr << "cannot find '" <<  config_file << "'" << std::endl;
    }


    po::notify(vm);

    if(vm.count("help")){
      std::cout << visible << std::endl;
      return 0;
    }
    if (vm.count("Help-Module")) {
      const string& s = vm["Help-Module"].as<string>();
      if (s == "Cell-Parameters") {
        cout << cell_config;
      } else if (s == "Dish-Parameters" ) {
        cout << dish_config;
      } else if (s == "Output-Configuration"){
	cout << output_config;
      } else {
        cout << "Unknown module '" 
             << s << "' in the --help-module option\n";
        return 1;
      }
      return 0;
    }
     
    if(vm.count("all-help")){
      std::cout << visible << std::endl << hidden << std:: endl;
      return 0;
    }
    
    //some consistency checks : 
    conflicting_options(vm,"config-file","no-config-file");
    option_dependency(vm,"clone-pop","load-file");
    option_dependency(vm,"clone-pop","dimension");
    option_dependency(vm,"env-switching","env-config");
    option_dependency(vm,"env-change-time","env-switching");
    option_dependency(vm,"env-switch-time","env-config");


    required_in_absence_of(vm,"save-dir", "load-file");
    required_in_absence_of(vm,"mutation-rate","load-file");
    required_in_absence_of(vm,"change-rate","load-file");
    required_in_absence_of(vm,"pop-seed","load-file");
 
    //parsing environment switch seed value
    switch_rand.init(switch_seed);
 
    //parsing special MUTATIONS RATIO option value

    if(vm.count("mutation-ratios")){
      std::string str = vm["mutation-ratios"].as<string>();
      typedef boost::tokenizer<boost::char_separator<char> > 
	tokenizer;
      boost::char_separator<char> sep(":");
      tokenizer tokens(str, sep);
      list<double> rats(0);
      for (tokenizer::iterator tok_iter = tokens.begin();tok_iter != tokens.end(); ++tok_iter){
	stringstream ss(*tok_iter);
	double n; 
	ss >> n; 
	rats.push_back(n);
      }
      if(rats.size() != 4) {
	std::cerr << "mutation-ratios not well formed" << std::endl;
	return 1;
      }
      INS = rats.front();
      rats.pop_front();
      DUP = rats.front();
      rats.pop_front();
      DEL = rats.front();
      rats.pop_front();
      POINT = rats.front();
    }

    if(vm.count("mutation-scale-ratios")){
      std::string str = vm["mutation-scale-ratios"].as<string>();
      typedef boost::tokenizer<boost::char_separator<char> > 
	tokenizer;
      boost::char_separator<char> sep(":");
      tokenizer tokens(str, sep);
      list<double> rats(0);
      for (tokenizer::iterator tok_iter = tokens.begin();tok_iter != tokens.end(); ++tok_iter){
	stringstream ss(*tok_iter);
	double n; 
	ss >> n; 
	rats.push_back(n);
      }
      if(rats.size() != 4) {
	std::cerr << "mutation-scale-ratios not well formed" << std::endl;
	return 1;
      }
      GCR = rats.front();
      rats.pop_front();
      SG = rats.front();
      rats.pop_front();
      POLYP = rats.front();
      rats.pop_front();
      NOTHING = rats.front();
    }

    //parsing special GENE RATIO option value

    if(vm.count("gene-ratios")){
      std::string str = vm["gene-ratios"].as<string>();
      typedef boost::tokenizer<boost::char_separator<char> > 
	tokenizer;
      boost::char_separator<char> sep(":");
      tokenizer tokens(str, sep);
      list<double> rats(0);
      for (tokenizer::iterator tok_iter = tokens.begin();tok_iter != tokens.end(); ++tok_iter){
	stringstream ss(*tok_iter);
	double n; 
	ss >> n; 
	rats.push_back(n);
      }
      if(rats.size() != 3) {
	std::cerr << "gene-ratios not well defined" << std::endl;
	return 1;
      }
      TF_RAT = rats.front();
      rats.pop_front();
      PUMP_RAT = rats.front();
      rats.pop_front();
      ENZ_RAT = rats.front();
    }

    // print out chosen configuration options
    string configuration = map_to_string(vm,defaults);
    std::cout << configuration;
 


    (void) signal(SIGSEGV,leave);
    SpatialDish * sd;

    if(!vm.count("load-file"))
      save_path = bf::path(save_dir);

    if(! bf::exists(save_path)){
      std::cout << "creating directory '" << save_path.string() << "'" << std::endl;
      bf::create_directory(save_path);
    }
    else if(bf::is_directory(save_path)){
      std::cout << save_path.string() << " exists" << std::endl;
      if(! is_empty(save_path)){
	std::cout << "and is not empty, removing all and creating new" << std::endl;
	bf::remove_all(save_path);
	bf::create_directory(save_path);
      }
    }
    else {
      std::cerr << "given savedirectory exists as (regular) file," << std::endl
		<< "cannot save" << std::endl;
      return 1;
    }

    if(vm.count("load-file")){ 
      // when loading a dish, evo and env seed default
      //values should be set to '0' , which tells RestoreDish(..) we are using the 
      //stored seeds;
      if(!vm.count("evo-seed"))
	evo_seed = 0;
      if(!vm.count("env-seed"))
	env_seed = 0;
      if(vm.count("clone-pop")) // create a dish from one ancestor, where dimension must be given
	sd = new SpatialDish(vm["clone-pop"].as<int>(),dimension*dimension, load_file, save_path.string(),grace_par,plotting,evo_seed,env_seed);
      else{
	sd = new SpatialDish(load_file,save_path.string(),grace_par,plotting,evo_seed,env_seed);
	dimension = int( sqrt(  (sd -> p) -> get_pop_size() )); 
      }
      // after loading the old style save file we toggle the Boolean
      // such that in the new dish save, we do save the wgds parameter
      if(vm.count("pre-wgd-load-file")) BEFORE_WGD = false; 
    }

    else{
      sd = new SpatialDish(dimension*dimension,genes,
			   save_path.string(),grace_par,plotting,
			   pop_seed,evo_seed,env_seed,
			   8,MUTATION_RATE,CHANGE_RATE); //suspicious six found again, replacing it with 8
    }

    string png_folder = save_dir + "/png";
    size_t len = png_folder.length();
    savedir_cash = new char [ len +1 ];
    strcpy( savedir_cash, png_folder.c_str() );
 
    /* make arguments available */
    argc_g = argc;
    argv_g = argv;

    InitialMain(dimension);
   
    sd -> place_cells(Dish); 
    PetriDish * pd = sd -> p;

    // store explicitly set configuration options in local configuration file, 
    // and save in save directory 
    ofstream run_config_file; 
    string savefile = pd -> getSaveDirectory() + local_config; 
    run_config_file.open(savefile.c_str());
    run_config_file << configuration;
    run_config_file.close();

// writing out the environments to be seen in the case of
// switch between multiple environments that have been predefined.
    if (env_system_switch)
    {
     cout << "multienvironmentsave" <<std::endl;
      vector<std::pair<std::string, double> > currentenvironment;
      currentenvironment.push_back(make_pair("target-A-in", TARGETA));    
      currentenvironment.push_back(make_pair("target-X-in", TARGETX));
      currentenvironment.push_back(make_pair("passive-diffusion", Perm)); 
      currentenvironment.push_back(make_pair("conversion", N)); 
      currentenvironment.push_back(make_pair("enzyme-degradation", Degr)); 

      environmentset.push_back(currentenvironment);

      ifstream environmentfile(change_config.c_str());
      vector<std::string> rawenvironment;
      if (environmentfile.is_open())
      {
         std::string line;

         while ( getline(environmentfile,line) )
         { 
           if (line.find("=")!=line.npos)
           {
             rawenvironment.push_back(line);
           }
         }
         environmentfile.close();
      }
      int looptimes = rawenvironment.size() /currentenvironment.size();
      int looplength = rawenvironment.size()/looptimes;
      int looped = 0;
      while (looped<looptimes)
      {
        vector<pair<std::string, double> > newenvironment;
        for( int t1 =0; t1<looplength ; t1++)       
        {
          int pos = rawenvironment[(looped*looplength +t1)].find("=");
          newenvironment.push_back(   make_pair(  rawenvironment[(looped*looplength  +t1)].substr(0,pos), atof( rawenvironment[(looped*looplength  +t1)].substr(pos+1).c_str()) ));
        }
        environmentset.push_back(newenvironment);
        looped ++;
      }
    }
    //when switching between interval values,
    //the specifics of the environmental changes can be handed down in the config file.
    else if ((env_switch_rate!=0)||(env_switch_time!=0) ){
      ifstream env_ratefile(change_config.c_str());
      vector<std::string> conditions;
      if (env_ratefile.is_open())
      {
        std::string line;
        while ( getline(env_ratefile,line) )
        {
          if (line.find("=")!=line.npos)
          {
             conditions.push_back(line);
          }
    	}
      }
    	env_ratefile.close();
    	vector<boost::tuple<std::string, double,double> > newenvrate;
    	  for( int t1 =0; t1<conditions.size() ; t1++)
    	  {
    	    int pos = conditions[t1].find("=");
    	    int secpos = conditions[t1].find("~");
    	    if (conditions[t1].find("generation") != std::string::npos){ //so if the line contains the word generation:
    	    	envrateset.push_back(newenvrate);
    	      newenvrate.clear();
    	      newenvrate.push_back(   boost::make_tuple(  conditions[t1].substr(0,pos), atoi( conditions[t1].substr(pos+1,(secpos-pos-1)).c_str()), 0 ));
    	    }
    	    else{
    	      newenvrate.push_back(   boost::make_tuple(  conditions[t1].substr(0,pos), atof( conditions[t1].substr(pos+1,(secpos-pos-1)).c_str()), atof(conditions[t1].substr(secpos+1).c_str()) ));
    	    }
    	  }
    	  envrateset.push_back(newenvrate);
    	  for (int t1 =0; t1<envrateset.size(); t1++)
    	  {
    	    for (int t2 = 0; t2 < envrateset[t1].size(); t2++)
    	    {
    	       cout << boost::get<0>(envrateset[t1][t2]) << boost::get<1>(envrateset[t1][t2]) << std::endl;
    	    }
    	  }
    	}

       	
// writing relevant options to a configuration file for knockout
// analysis; we do not save all values as in 'map_to_string(..) 
// because not all options exist in knockout analysis or, some values
// such as configuration files might be different between the methods
    ostringstream formatter;
    formatter << "3-environments=" << vm["3-environments"].as<bool>() << std::endl
	      << "assays=" << vm["assays"].as<int>() << std::endl
	      << "change-threshold=" << vm["change-threshold"].as<double>() << std::endl
	      << "conversion-rate=" << vm["conversion-rate"].as<double>() << std::endl
      	      << "env-change-time=" << vm["env-change-time"].as<int>() << std::endl
	      << "env-switching=" << vm["env-switching"].as<bool>() << std::endl
	      << "env-config=" << vm["env-config"].as<string>() << std::endl
      	      << "enzyme-degradation=" << vm["enzyme-degradation"].as<double>() << std::endl
	      << "enzyme-start-concentration=" << vm["enzyme-start-concentration"].as<double>() << std::endl
	      << "gcr-maximum-fraction=" << vm["gcr-maximum-fraction"].as<double>() << std::endl
	      << "geometric-dist-length=" << vm["geometric-dist-length"].as<double>() << std::endl
      	      << "integration-step=" << vm["integration-step"].as<double>() << std::endl
	      << "lifetime=" << vm["lifetime"].as<int>() << std::endl
	      << "low-profile=" << vm["low-profile"].as<bool>() << std::endl
	      << "measure-step=" << vm["measure-step"].as<double>() << std::endl
	      << "mutation-rate=" <<  vm["mutation-rate"].as<double>() << std::endl
	      << "mutation-ratios=" << vm["mutation-ratios"].as<string>() << std::endl
	      << "mutation-scale-ratios=" << vm["mutation-scale-ratios"].as<string>() << std::endl
	      << "mutation-type="  << vm["mutation-type"].as<int>() << std::endl
	      << "passive-diffusion=" << vm["passive-diffusion"].as<double>() << std::endl
	      << "production=" << vm["production"].as<bool>() << std::endl
	      << "restrict-size=" << vm["restrict-size"].as<bool>() << std::endl
	      << "save-time=" << vm["save-time"].as<int>() << std::endl
	      << "size-max=" << vm["size-max"].as<int>() << std::endl
	      << "size-penalty=" << vm["size-penalty"].as<double>() << std::endl
	      << "skewed-environment=" << vm["skewed-environment"].as<bool>() << std::endl
	      << "start-A-in=" << vm["start-A-in"].as<double>() << std::endl
	      << "start-X-in=" << vm["start-X-in"].as<double>() << std::endl
	      << "step-reduction=" << vm["step-reduction"].as<double>() << std::endl
	      << "target-A-in=" << vm["target-A-in"].as<double>() << std::endl
	      << "target-X-in=" << vm["target-X-in"].as<double>() << std::endl
	      << "tree-cut-off=" << vm["tree-cut-off"].as<int>() << std::endl
	            ;
    ofstream knockout_config_file;
    savefile = pd -> getSaveDirectory() + "local_k_config.cfg";
    knockout_config_file.open(savefile.c_str());
    knockout_config_file << formatter.str();
    knockout_config_file.close();

 

    // START RUN
    int to_env_switch = 0;
    lastenvironment = 1;
    int currenttimer = 0;
    bool count_to_switch = false;
    if(_3ENV) pd -> set_Aout_3env(1.0);                            
    else pd -> set_Aout(1.0);

    Time=0;
    if(vm.count("load-file")){
      if( vm.count("clone-pop")){
	Time = vm["clone-pop"].as<int>() ;
      }
      else{
	Time = pd -> get_generations();
      }
    }
    int starttime = Time;
   if(SPARSE)
   {
     pd -> set_depleting_resource(depleting_resource);
     pd -> set_depletion_rate(depletion_rate);
   }
    //bool first_time=true;
      //Time is declared an external int in cash2-s.hpp and is kept to
      //maintain time synchronization between "cash" and "virtual
      //cell" ; TODO: discriminate from other 'time' measures in
      //petridish.cpp and cell.cpp
    for(;Time<generations;Time++){
      std::cout << "to first step" << std::endl;
      if(SPARSE) pd -> step_sparse();
      else pd -> step();
      if(space){ 
	/* reproduction and mutation are performed in the NextState
	   function of cash2-s.cpp; NextState will be called by
	   Synchronous or Asynchronous; One of these will be called in
	   the UpdateSpace function*/
	     UpdateSpace();
	     pd -> cleanList();
      }
      else {
	    Update();
	    if(REPRODUCE==2||DEATH) pd -> reproduce2();
	    else pd -> newreproduce();      // similar to spatial reproduction as implemented now

        pd -> mutateCells();
	    sd -> place_cells(Dish);
      }
      /* this part switches between a preset set of environment configuration settings,
       * either at set interval or at random times.
       */
      if (env_system_switch)
      {
       cout << "time is " << Time << std::endl;
        if (
        ((env_switch_time != 0 ) &&(Time-starttime) % env_switch_time == 0)
         ||((env_switch_rate !=0)&& (switch_rand.uniform() <env_switch_rate)))
        {
           TARGETA = environmentset[lastenvironment ][0].second;
           TARGETX = environmentset[lastenvironment ][1].second;
           Perm =    environmentset[lastenvironment ][2].second;
           N =       environmentset[lastenvironment ][3].second;
           Degr =    environmentset[lastenvironment ][4].second;
           lastenvironment = ((lastenvironment +1)%environmentset.size());
  	       pd -> DishParamUpdate(); ///new to make the file remember the environmental change!
	       std::cout << "CHANGING TO ENVIRONMENT " << lastenvironment << std::endl;
           cout << "TARGETA " << TARGETA <<std::endl;
           cout << "TARGETX " << TARGETX <<std::endl;
           cout << "Perm " << Perm <<std::endl;
           cout << "N " << N <<std::endl;
           cout << "Degr " << Degr <<std::endl;
         }
        /*This part switches environments between random values,
         * either at set interval or at random times.
         */
       }
       else  {
         if ((env_switch_time != 0 )||(env_switch_rate !=0)){
      	   if      (((env_switch_time != 0 ) &&((Time-starttime) % env_switch_time == 0))
        	  ||((env_switch_rate !=0)&& (switch_rand.uniform() <env_switch_rate)))
    	   {
      		 if (envrateset.size() > (currenttimer+1)){
    		   if (boost::get<1>(envrateset[currenttimer+1][0]) <= (Time-starttime)){
      		     currenttimer +=1;
      			 cout << "switch to regime generations start at:" << (boost::get<1>(envrateset[currenttimer][0])) <<std::endl;
      		   }
    		 }
      		   bool Xswitch = false;
      		   bool Aswitch = false;
      		   bool Permswitch = false;
      		   bool Nswitch = false;
      		   bool Degrswitch = false;
      		   double Xlow, Xhigh, Alow, Ahigh, Permlow, Permhigh, Nlow, Nhigh, Degrlow, Degrhigh;
      		   for (int t1 = 1; t1<envrateset[currenttimer].size();t1++)
      		   {
      		     if (boost::get<0>(envrateset[currenttimer][t1]) == "Xswitch"){
      		       Xswitch = true;
      			   Xlow = boost::get<1>(envrateset[currenttimer][t1]);
      			   Xhigh = boost::get<2>(envrateset[currenttimer][t1]);
      			 }
          	     if (boost::get<0>(envrateset[currenttimer][t1]) == "Aswitch"){
          		   Aswitch = true;
          		   Alow = boost::get<1>(envrateset[currenttimer][t1]);
          		   Ahigh =boost::get<2>(envrateset[currenttimer][t1]);
          	     }
      			 if (boost::get<0>(envrateset[currenttimer][t1]) == "Permswitch"){
      			   Permswitch = true;
      			   Permlow = boost::get<1>(envrateset[currenttimer][t1]);
      			   Permhigh = boost::get<2>(envrateset[currenttimer][t1]);
      			 }
          	     if (boost::get<0>(envrateset[currenttimer][t1]) == "Nswitch"){
          		   Nswitch = true;
          		   Nlow = boost::get<1>(envrateset[currenttimer][t1]);
          		   Nhigh = boost::get<2>(envrateset[currenttimer][t1]);
          	     }
          	     if (boost::get<0>(envrateset[currenttimer][t1]) == "Degrswitch"){
          		   Degrswitch = true;
          	 	   Degrlow = boost::get<1>(envrateset[currenttimer][t1]);
          	  	   Degrhigh = boost::get<2>(envrateset[currenttimer][t1]);
          	     }
      		   }
        	   if (Xswitch) TARGETX =  Xlow + switch_rand.uniform()*(Xhigh-Xlow);
          	   if (Aswitch) TARGETA = Alow  + switch_rand.uniform()*(Ahigh-Alow);
               if (Permswitch) Perm =   Permlow + switch_rand.uniform()*(Permhigh-Permlow);
               if (Nswitch) N = Nlow + switch_rand.uniform()* (Nhigh-Nlow);
               if (Degrswitch) Degr = Degrlow + switch_rand.uniform()*(Degrhigh-Degrlow);
               pd -> DishParamUpdate(); ///new to make the file remember the environmental change!
   	           cout << "CHANGING TO NEW ENVIRONMENT " << std::endl;
     	       cout << "TARGETA " << TARGETA <<std::endl;
               cout << "TARGETX " << TARGETX <<std::endl;
               cout << "Perm " << Perm <<std::endl;
               cout << "N " << N <<std::endl;
               cout << "Degr " << Degr <<std::endl;
       	   }
    	 }


      /* this part loads new configuration settings in the special
	 case of switching environment during the simulation. It is
	 necessary to make a new variables mapping by parsing the new
	 variables first and then augmenting with the old command line
	 and configuration file options. */
         else
         {
           if(count_to_switch){
              to_env_switch ++;
           }
           if(ENV_SWITCHING && CHANGE_ENV ){
	         count_to_switch = true;
	         CHANGE_ENV = false;
           }
           if((ENV_SWITCHING && ((to_env_switch >= switch_delay  && Time % SAVETIME == 0) || Time == env_change_time))){ // && first_time){
	         po::variables_map env_change_map;
     	     ifstream ifs(change_config.c_str());
	         if(!ifs.fail()){               // check if file exists
	            po::store(parse_config_file(ifs,config_file_options), env_change_map);
	            po::notify(env_change_map);

	           /*From here on, parsing proceeds in the same way as the
	            initial option parsing */
	            po::store(po::command_line_parser(argc,argv).
	            options(cmdline_options).positional(p).run(),env_change_map);
	            po::notify(env_change_map);
	            bf::path config_path;
	            if(env_change_map.count("load-file") &&
	            !no_config && env_change_map["config-file"].defaulted()){
	              config_path = bf::path(load_file);
	              config_path /= "../";
	              config_path /= local_config;
	              config_path.normalize();
	              ifstream ifs(config_path.string().c_str());
	              if(!ifs.fail()){               // check if file exists
	                po::store(parse_config_file(ifs,config_file_options), env_change_map);
	                std::cout << "loading local configurations" << std::endl;
	              }
	              else std::cerr << "cannot find '"+ config_path.string() + "'" << std::endl;
	              }
	              else if(!no_config){
	                ifstream ifs(config_file.c_str());
	                if(!ifs.fail()){               // check if file exists
	                  po::store(parse_config_file(ifs,config_file_options), env_change_map);
	                }
	                else std::cerr << "cannot find '" <<  config_file << "'" << std::endl;
	              }
	              po::notify(env_change_map);
               }
  	           else std::cerr << "cannot find '" <<  change_config << "'" << std::endl;
 	           pd -> DishParamUpdate(); ///new to make the file remember the environmental change!
	           std::cout << "CHANGING ENVIRONMENT THE OLD WAY" << std::endl;
	           std::cout << map_to_string(env_change_map,defaults);
	     	  /* save a switching population only once */
	          ENV_SWITCHING = false;
	          CHANGE_ENV = false;
	          count_to_switch = false;
	          to_env_switch = 0;
	          //	first_time = false;
              }
            }
            if(display==1)
	          if(Mouse()==1||poseG==1)gotMouse();

            if(stopG==1)
              break;
          }
       }
       pd -> AnalysePop();
       if(generations % SAVETIME != 0){
         ostringstream formatter;
         formatter << "ver3_generation" << generations;
         pd -> SaveDish(formatter.str());
       }
       pd -> exitGrace();
       if(display==1)
         CloseDisplay();
       if(GraceIsOpen()&&display==0)
         GraceClose();
       if(movie_g==1)
         ClosePNG();
       return 0;
     }
     catch(exception& e) {
       cerr << e.what() << "\n";
    }
  }

void leave(int sig) {
  printf("SEGFAULT OCCURED, ATTEMPTING TO CLOSE GRACEBAT... \n");
  gracesc::Xmgrace::close();  
  exit(sig);
}
