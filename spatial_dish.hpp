#include <boost/program_options.hpp> 
#include "cash2-s.hpp"

#ifndef SPATIAL_DISH_HPP_
#define SPATIAL_DISH_HPP_

//#include "petridish.hpp"  

using namespace std;

class SpatialDish{
 public:
  SpatialDish(int,int,string,string,int,int,int,int,int,double,double);
  SpatialDish(int,int,string,string,string,int,int,int);
  SpatialDish(string,string,string,int,int,int);
  void place_cells(TYPE2**);
  PetriDish * p;
  double change_rate;
  double env_change_rate;
  int env_switch_time;
  double mutationrate;
  bool depleting_resource;
  double depletion_rate;
 private:
  

};



#endif
