/*****************--ToDo's--************************

Fix het total_nmbr_genes tellersysteem ( zie copyconstrs Gene, TF etc & dup mutation.
*****************--ToDo's--************************/

#include <vector>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <stdio.h>
#include <math.h>

//#include "temp.hpp"
#include "genestructs.hpp"

#ifndef GENE_HPP_
#define GENE_HPP_

using namespace std;

// #define CHANGE_THRESHOLD 0.001
// #define Degr 1.
// #define H 0.1
// #define STARTCONC 1.

ostream & operator <<(ostream &,Type);
istream & operator >>(istream &,Type&);
ostream & operator << (ostream &,Ligand);

void conflicting_options(const boost::program_options::variables_map&,const char*, const char*);
void option_dependency(const boost::program_options::variables_map&,const char*, const char*);
void required_option(const boost::program_options::variables_map&,const char*);
void required_in_absence_of(const boost::program_options::variables_map&,const char*, const char*);
string map_to_string(boost::program_options::variables_map& , bool);


class TF; //forward declaration
class Enzyme;
class GeneList; //forward declaration
class GeneCollection;

class Gene{
  

 public:
  static long int total_nmbr_genes;  //total nmbr of genes ever created (and possibly lost)
  Gene();
  Gene(Type);
  Gene(Type,int,double);
  Gene(const Gene&);
  Gene(const Gene&,int);
  virtual ~Gene(){};
  const Type getType()const {return typ;};
  void addTFtoList(TF *);
  int get_op_type(){ return operator_type; };
  void set_op_type(int op){operator_type = op;};
  //  GeneList & Gene::get_tf_list(){ return tf_list;};
  GeneList & get_tf_list(){ return tf_list;};
  virtual string toString();
  void set_bind_pol();
  double getConcentration(){ return concentration; };
  double getNextConcentr(){return next_concentr;};
  void setNextConcentr(double d){ next_concentr = d; };
  double getPromStr(){ return promotor_strength;};
  void setPromStr(double pr){promotor_strength = pr;};
  void setConcentration(double con){concentration = con;};
  void resetConcentration();
  double Reg();
  bool EulerStep();
  void setNumber(int n){number = n;};
  int getNumber()const {return number;};
  void notify_tfs();
  void notify_tfs_loss();
  void setContribution(double c){fitness_contribution = c;};
  void setColour(double h,double s,double b){ hue = h; sat = s; bright = b;};
  double getHue(){ return hue;};
  double getSat(){ return sat;};
  double getBright(){return bright;};
  double getFitnessContribution(){return fitness_contribution;};
  void setChild(Gene *);
  bool noChildren();
  void removeChild(Gene *);
  void removeParent();
  void informChildren();
  void informParent();
  const Gene * getParent(){ return parent;};
  Gene * getAncestor(){ return parent;};
  list<Gene *> getChildren(){return children;};
  list<Gene *> getOffspring(){return children;};
  list<Gene *>::iterator getChildIter(){return child_iter;};
  Gene * findRoot();
  list<Gene *> buildTreeList(int,int);
  void addNodes(list<Gene *> &, Gene *, int, int, int);

 private:
  Type typ;
  int operator_type;
  //  bool deleted;
  double promotor_strength;
  //  bool error;
  GeneList tf_list;  // the gene knows which TF's bind to it.
  long int number;   //unique gene number, generated sequentially
  //  string name;
  double binding_polynomial; // 1+ sum of [all binding TFs in a particular form] * Kb terms
  double concentration;
  double next_concentr;
  bool nochange;
  double hue,sat,bright; //used in graph colouring

  double fitness_contribution;
  Gene* parent;
  list<Gene*> children;
  list<Gene*>::iterator child_iter;
  list<int> ancestor_list;
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int){

    ar & BOOST_SERIALIZATION_NVP(typ); 
   ar & BOOST_SERIALIZATION_NVP(operator_type);
   ar & BOOST_SERIALIZATION_NVP(promotor_strength);
   ar & BOOST_SERIALIZATION_NVP(number) ;
   //Memory minimization :
   //   ar & BOOST_SERIALIZATION_NVP(tf_list);
   //   ar & BOOST_SERIALIZATION_NVP(name);
   //   ar & BOOST_SERIALIZATION_NVP(binding_polynomial) ;
   //   ar & BOOST_SERIALIZATION_NVP(concentration);
   //   ar & BOOST_SERIALIZATION_NVP(next_concentr);
 };
};



ostream & operator <<(ostream &, Gene &);

class TF: public Gene{
 public:
  TF();
  TF(int,double,Ligand, int,double,double,double,double);
  TF(const TF&);
  TF(const TF&,int);
  virtual ~TF(){};
  void update_bind_list(GeneCollection *);
  bool op_bind_match(Gene *);
  int get_bind_seq(){return binding_sequence;};
  void set_bind_seq(int bs){binding_sequence = bs;};
  Ligand getLigand(){return ligand;};
  void setLigand(Ligand li){ ligand = li; };
  void setW(double w);
  double getW(){return W;}; 
  // GeneCollection & TF::get_bind_list(){return bind_list;};
  GeneCollection & get_bind_list(){return bind_list;};
  string toString();
  double getKd(){return Kd;};
  void setKd(double kd){Kd = kd;};
  double getKb(){return Kb;};
  void setKb(double kb){Kb = kb;};
  double getEffBound(){return EffBound;};
  void setEffBound(double eb){EffBound = eb;};
  double getEffApo(){return EffApo;};
  void setEffApo(double ea){EffApo = ea;};
  void notify_genes();
  void notify_genes_loss();
  bool double_check_pointers();
  void setAttributes();
  string getAttribute1(){return attribute1;};
  string getAttribute2(){return attribute2;};
 private:
  Ligand ligand;
  double W;  //fraction of TF molecules bound to its ligand
  int binding_sequence;
  double Kd; //constant of dissociation of ligand
  double Kb; //binding constant of tf to gene operator.
  double EffApo; //effect of TF unbound by ligand on transcription.
  double EffBound; //effect of TF bound by ligand on transcription.
  string attribute1;  //used in graph drawing
  string attribute2;
  GeneCollection bind_list;  //a list of Genelists of the genes with promotors, this TF binds to
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int){
    //    ar & boost::serialization::base_object<Gene>(*this);
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Gene);
    /*
    ar & ligand & W & binding_sequence & Kd & Kb 
      & EffApo & EffBound & bind_list;
    */
    //    cout << "start tf" << endl;
    ar & BOOST_SERIALIZATION_NVP(ligand) 
      & BOOST_SERIALIZATION_NVP(W)
      & BOOST_SERIALIZATION_NVP(binding_sequence) 
      & BOOST_SERIALIZATION_NVP(Kd)
      & BOOST_SERIALIZATION_NVP(Kb) 
      & BOOST_SERIALIZATION_NVP(EffApo)
      & BOOST_SERIALIZATION_NVP(EffBound);
    //    cout << "start bind_list" << endl;
    //    ar  & BOOST_SERIALIZATION_NVP(bind_list);
    //    cout << "end tf " << endl;
  }
};		

class Pump: public Gene{
 public:
  Pump();
  Pump(int, double,double, double, double);
  Pump(const Pump&);
  Pump(const Pump&,int);
  virtual ~Pump(){};
  double getVmax(){return Vmax;};
  void setVmax(double vmax){Vmax = vmax;};
  double getKpA(){return KpA;};
  void setKpA(double kpa){KpA = kpa;};
  double getKpX(){return KpX;};
  void setKpX(double kpx){KpX = kpx;};
  string toString();
 private:
  double KpA;
  double KpX;
  double Vmax;
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int){

    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Gene);
    ar & BOOST_SERIALIZATION_NVP(KpA) 
      & BOOST_SERIALIZATION_NVP(KpX)
      & BOOST_SERIALIZATION_NVP(Vmax);
  }
};


class Enzyme: public Gene{
 public:	
  Enzyme();
  Enzyme(int,double,double,double,double,bool);
  Enzyme(const Enzyme&);
  Enzyme(const Enzyme&,int);
  virtual ~Enzyme(){};
  bool isAnabolic(){ return Anabolic;};
  double getKA(){return KA;};
  void setKA(double ka){KA = ka;};
  double getKX(){return KX;};
  void setKX(double kx){KX = kx;};
  double getVmax(){return Vmax;};
  void setVmax(double vmax){Vmax = vmax;};
  string toString();
 private: 
  bool Anabolic; // synthesizes product from both A & X
  double KA;    //Michaelis constant for A
  double KX;	 //Michaelis constant for X
  double Vmax;   // max rate of catabolism/anabolism reaction
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int){

    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Gene);
    ar & BOOST_SERIALIZATION_NVP(Anabolic) ;
    ar & BOOST_SERIALIZATION_NVP(KA);
    ar & BOOST_SERIALIZATION_NVP(KX) ;
    ar & BOOST_SERIALIZATION_NVP(Vmax);
  }
};


#endif
