

#include "histogram.h"

/* Size of the canvas used to draw a histogram. This must be
   equal or smaller than the size of window. If it is set
   smaller than the size of window, one can do something extra
   in the region of the window not used for histogram such as
   making an x-axis. The histogram will be drawn in the window
   from upper left to lower right. */
static int num_row;
static int num_col;

/* Range of y-axis */
static double max_y;

/* Number of bins. This must be smaller than or equal to
   num_row. */
static int n_bins;

/* Number of pixel per bin. This will be set to
   num_row/n_bins. It may be that
   n_bins*n_pixel_per_bin<num_row. In that case, the right
   side of the canvas will be filled with
   color_index_background.
*/
static int n_pixel_per_bin;

/* Color of bars in DrawHistogram(). */
static int color_index_filled;
/* Color of the non-bar part. */ 
static int color_index_blank;
/* Color of the right side of picture. See the explanation for
   n_pixel_per_bin. */
static int color_index_background;

/* 
   DrawHistgram() will draw histgram. This will color the
   color of bars by one color, which is color_index_filled.
   Arg1: "frequencies" should be an array that contains frequencies
   for each bin. The number of elements should be equal to
   "n_bins".

   Arg2: "canvas" should be an 2D array of integers. This
   should be given to PlaneDisplay() of CASH.
   
 */
void DrawHistogram(double* frequencies, int** canvas)
{
  int row=1,col=1,bin,height;
  int col_limit, anti_height;

  /* Draw each bin */
  for(bin=0; bin<n_bins; ++bin){
    height = (int)(frequencies[bin]/max_y*num_row + 0.5);

    /* If height is out of boundary, set it to maximum */
    height = (height < num_row) ? height : num_row;
    anti_height = num_row - height;

    col_limit = (bin+1)*n_pixel_per_bin+1;
    /* Draw each column */
    for(col=bin*n_pixel_per_bin+1; col<col_limit; ++col){
      /* Draw each row */
      for(row=num_row; row>0; --row){
	canvas[row][col] = (row > anti_height) ? color_index_filled : color_index_blank;
      }
    }
  }
  /* Fill the rest by blank */
  for(col=n_bins*n_pixel_per_bin+1; col <= num_col; ++col)
    for(row = num_row; row>0; --row){
      canvas[row][col] = color_index_background;
    }

}

/*
  InitHistogram() will do the initialization for DrawHistogram().
*/
void InitHistogram(int a_num_row, int a_num_col, double a_max_y, 
		   int a_n_bins, int a_color_index_filled, 
		   int a_color_index_blank,int a_color_index_background)
{
  num_row = a_num_row;
  num_col = a_num_col;
  max_y = a_max_y;
  if(max_y <= 0.){
    fprintf(stderr,"InitHistogram() Error, max_y must be greater than 0. max_y is set to 1.\n");
    max_y = 1.;
  }

  n_bins = a_n_bins;
  color_index_filled = a_color_index_filled;
  color_index_blank = a_color_index_blank;
  color_index_background = a_color_index_background;

  n_pixel_per_bin = num_col/n_bins;
  if(n_pixel_per_bin < 1){
    fprintf(stderr,"InitHistogram() Error, the number of bins must be smaller than num_row. n_pixel_per_bin is set to 0.\n");
    n_pixel_per_bin = 0;
  }
}

/*
  DrawHistogramColorful() can be more colorful than
  DrawHistogram() in the drawing of bars. It does not use
  color_index_filled, but instead uses an array supplied by
  the user.

  Argument 1 and 2 are the same as DrawHistogram().

  Argument 3 takes a pointer to the array of color
  indexes. The index of color_of_bars corresponds to the index
  of frequencies. Thus, the length color_of_bars must be equal
  to or greater than that of frequencies.

*/
void DrawHistogramColorful(double* frequencies, int** canvas, int* color_of_bars)
{
  int row=1,col=1,bin,height;
  int col_limit, anti_height;

  /* Draw each bin */
  for(bin=0; bin<n_bins; ++bin){
    height = (int)(frequencies[bin]/max_y*num_row + 0.5);

    /* If height is out of boundary, set it to maximum */
    height = (height < num_row) ? height : num_row;
    anti_height = num_row - height;

    col_limit = (bin+1)*n_pixel_per_bin+1;
    /* Draw each column */
    for(col=bin*n_pixel_per_bin+1; col<col_limit; ++col){
      /* Draw each row */
      for(row=num_row; row>0; --row){
	canvas[row][col] = (row > anti_height) ? (*color_of_bars) : color_index_blank;
      }
    }
    /* Incliment the array of colors. */
    ++color_of_bars;
  }
  /* Fill the rest by blank */
  for(col=n_bins*n_pixel_per_bin+1; col <= num_col; ++col)
    for(row = num_row; row>0; --row){
      canvas[row][col] = color_index_background;
    }

}

/*
  InitHistogramColorful() will do the initilization for
  InitHistogramColorful(). It will also make x-axis that is
  colored according to "color_of_bars". But this is optional:
  if you don't want it, set "width_x_axis" to zero. It must be
  that a_num_row + width_x_axis <= nrow.

  color_of_bars: the same as appers in DrawHistogramColorful()

  width_x_axis: the width (in row) of the x_axis.

  canvas: the same as appers in DrawHistogramColorful()
*/
void InitHistogramColorful(int a_num_row, int a_num_col, double a_max_y, 
			   int a_n_bins, int a_color_index_blank,
			   int a_color_index_background, int width_x_axis, 
			   int* color_of_bars, int** canvas)
{
  int bin,col_limit,col,width;
  
  num_row = a_num_row;
  num_col = a_num_col;
  max_y = a_max_y;
  if(max_y <= 0.){
    fprintf(stderr,"InitHistogram() Error, max_y must be greater than 0. max_y is set to 1.\n");
    max_y = 1.;
  }

  n_bins = a_n_bins;
  color_index_blank = a_color_index_blank;
  color_index_background = a_color_index_background;

  n_pixel_per_bin = num_col/n_bins;
  if(n_pixel_per_bin < 1){
    fprintf(stderr,"InitHistogram() Error, the number of bins must be smaller than num_col. n_pixel_per_bin is set to 0.\n");
    n_pixel_per_bin = 0;
  }

  if(width_x_axis > 0){
    /* Draw a colored pixel for each bin. This is the x-axis */
    for(bin=0; bin<n_bins; ++bin){
      col_limit = (bin+1)*n_pixel_per_bin;
      /* Draw each column until the pixel per bin */
      for(col=bin*n_pixel_per_bin+1; col<=col_limit; ++col){
	/* Draw each row until the width of x-axis */
	for(width=0; width<width_x_axis; ++width){
	  canvas[num_row+width+1][col] = (*color_of_bars);
	}
      }
      /* Incliment the array of colors. */
      ++color_of_bars;
    }

    for(col=n_bins*n_pixel_per_bin+1; col<=num_col; ++col){
      for(width=0; width<width_x_axis; ++width)
	canvas[num_row+width+1][col] = color_index_background;
    }
  }
}
