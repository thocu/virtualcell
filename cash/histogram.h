#ifdef __cplusplus
extern "C" {
#endif


#include <stdio.h>
void DrawHistogram(double* frequencies, int** canvas);
void DrawHistogramColorful(double* frequencies, int** canvas, int* color_of_bars);
void InitHistogram(int a_num_row, int a_num_col, double a_max_y, int a_n_bins, int a_color_index_filled, int a_color_index_blank,int a_color_index_background);
void InitHistogramColorful(int a_num_row, int a_num_col, double a_max_y, int a_n_bins, int a_color_index_blank,int a_color_index_background, int width_x_axis, int* color_of_bars, int** canvas);

#ifdef __cplusplus
}
#endif
