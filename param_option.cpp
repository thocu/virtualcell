#include "param_option.hpp"
#include <boost/serialization/export.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/split_member.hpp>


Parameter_Option_Set::Parameter_Option_Set(int generation, int populationsize, int Time, double mutationrate, double change_rate, int evo_seed, int env_seed, double Target_A, double Target_X, double Perm, double N, double Degr){}

//Parameter_Option_Set::Parameter_Option_Set(const Parameter_Option_Set& p):generation(p.generation), populationsize(p.populationsize), Time(p.Time), mutationrate(p.mutationrate),change_rate(p.change_rate), evo_seed(p.evo_seed), env_seed(p.env_seed), Target_A(p.Target_A), Target_X(p.Target_X), Perm(p.Perm), N(p.N), Degr(p.Degr){ throw ();}
//Parameter_Option_Set::Parameter_Option_Set(){}
Parameter_Option_Set::Parameter_Option_Set():generation(0),populationsize(0),Time(0),mutationrate(0),change_rate(0),evo_seed(0),env_seed(0),Target_A(0),Target_X(0),Perm(0),N(0), Degr(0){}

BOOST_CLASS_VERSION(Parameter_Option_Set, 0);
BOOST_CLASS_EXPORT(Parameter_Option_Set);


