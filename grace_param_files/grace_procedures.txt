frequency of values above a certain threshold:
do: 	- if necessary first: data set operations: join sets, and sort them
	- evaluate expression:  y=(y > threshold)  -> this will put all values either
to 0 or 1 . 
	- integration: cummulative sum will give you a line describing
	cumulative count of your above threshold values
