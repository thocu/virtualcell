#ifndef KNOCKOUT_HPP_
#define KNOCKOUT_HPP_

#include <boost/program_options.hpp>
#include <boost/lexical_cast.hpp>
#include "petridish.hpp"
#include <boost/tuple/tuple.hpp>
#include <vector>


#define NR_MUTANTS 50
#define NR_CLOSEST 10
#define NEUTRAL_CUT_OFF 0.05      // below this value, we will call a fitness contribution neutral
#define BINS 5                    // nr of bins to score gene contribution
#define SWEEP_STEPS 20           // when sweeping a parameter, use this many steps to walk through;

namespace bf = boost::filesystem;

class knockout{
 public:
  knockout(string,string,string,int start_gen = 1,int end_gene = 1000000,int _step_size = 1);
  knockout(vector<string>,string,string,int start_gen = 1,int end_gene = 1000000,int _step_size = 1);
  void pick_lineage();
  void last_generation();
  void population_snapshots();
  void pick_individuals(vector<unsigned>);
  // dependent functions
  Cell * find_individual(unsigned,Cell*,int);
    void type_numbers();
    void cumulative_mutations();
    boost::tuple<int,double,double> run_fitness(list<Cell*>::iterator, int, int, int, double, int, int);
    void standard_env_fitness(std::string, std::string,bool);
    std::vector<std::vector<double> > read_standard_envs(std::string, std::string);
    boost::tuple<double,double,double,int> standard_env_fitnesses (Cell*, std::vector<std::vector<double> >);
    boost::tuple<int, double, double,int,int> standard_reduction(Cell*,std::vector<std::vector<double> >,double, double, int,std::string, bool);

    void population_regulation_comparison(std::string, double,double);
    void lineage_reduction_comparison(std::string, std::string, double, double, std::string, bool);
    void ohnologs();
    void WGD_trace();
    void WGD_ancestor_fitness();
    void pre_env_change_fitness(int);
    void lineage_gene_evolution(std::string);

  //  double check_alternative_fitness(Cell*, int, int);
    void mutations_per_gene_type();
    void cumulative_mutation_expectations();
    void expectations_per_gene_type();  
    void size_ancestors();
    void print_cells();
    void param_sweep(Type,int,int,double start_val = 0.1, double end_val = 10.);
    //requires analysis of modified cells
    void cumulative_mutation_contributions();
    void cumulative_mutation_contributions_multienv(std::string);
    void multienv_full_lineage_analysis(std::string,bool,bool,bool,double, int,Type,int,int,double,double,double,double);
    void analyse_lineage();
    void analyse_population();
    void analyse_population_snapshots();

    // dependent functions:
    void single_gen_multiple_mutation_robustness(Cell*, double,std::vector<double> , int,double cutoff= 0.85,int mutantnr = NR_MUTANTS);

    void multiple_mutation_robustness(double);
    void knockout_analyse_lineage();
    // dependent functions: 
      void contribution_per_gene_type();
      void binned_gene_contribution();
      void neutral_gene_mutant_effect(double);
      void single_neutral_gene_mutant_effect(Cell *, double, int,string,string);
      void single_analyse_duplicated_deleted_genes(Cell *, int,string,string, string, string);
      void single_binned_gene_contribution(Cell * ,int);
      void single_contribution_per_gene_type(Cell *, int);
      void major_mutation_robustness();
      void major_mutation_robustness_pop();
      void major_mutation_robustness_pop_vs_ancestor();
      void wgd_robustness();
      void analyse_duplicated_deleted_genes();
      void expression_level_evolution();
       // graphs:
	void gene_contribution_graphs();
	void function_graphs();
	void genome_order_graphs();
          
    // partly dependent (when standard fitness required):
      void fitness_ancestors();
      
  void exitGrace(){
    dish -> exitGrace();
  }
  void set_nr_mutants(int nr){number_of_mutants = nr;};
  void set_nr_closest(int nr){number_of_closest = nr;};
  void plotGrace();
  void closeGrace();
  bool print;
  bool resetting;
 private:
  double knockout_fitness(Cell *,int);  
  double multi_knockout_fitness(Cell *,int,int);

  void number_knockout(Cell *, int);
  double duplicate_fitness(Cell *,int);

  void gene_contributions(Cell *);
  vector<int> single_cell_reduction(Cell *, double, double, std::string, bool);
  double analyse_cell_in_env(Cell *,double,int,double,bool,string);
  void analyse_cell(Cell *,bool,string);
  double analyse_cell_fast(Cell *);
  double analyse_cell_in_env_fast(Cell *, double);

  double point_mutant_fitness(Cell *,int);  
  list<list<double> > point_mutant_neutral_genes(Cell *,int,double);
  list<double> multiple_mutant(Cell *,int,double);
  vector<double> dup_mutants_fitness(Cell *, int);
  vector<double> del_mutants_fitness(Cell *, int);

  double per_gene_mutation_rate();
  double dups_expectation(Cell *,int);
  double dels_expectation(Cell *,int);
  double points_expectation(Cell *,int);
  double major_dups_expectation(Cell *);
  double major_dels_expectation(Cell *);
  double major_ins_expectation(Cell *);

  list<list<double> > dup_analysis(Cell *,int);
  list<list<double> > del_analysis(Cell *,int);

  double point_mutation_contribution(Cell *,int);
  double dup_del_contribution(Cell *,int);
  double WGD_mutation_contributions(Cell *);
  double expression_level(Cell *, double);
  void quickfullsweep_cell(Cell *,int ,double, double, double );
  void sweep_cell(Cell*, Type,int,int,double,double);

  int start_generation;
  int end_generation;
  int step_size;
  int number_of_mutants;
  int number_of_closest;
  bool analyzed;
  bool analyzed_pop;
  bool analyzed_knockouts;
  std::string cumulative_version;
  double parentfitness;
  double currentfitness;
  PetriDish * dish;
  list<PetriDish *> dishes;
  list<Cell *> lineage; //oldest first
  list<Cell *>::iterator lin_iter;
  list<Cell *>::iterator cells_iter;
  list<Cell *> population; //storing a population at 1 point in time
  list<pair<list<Cell*>,int> > population_stores;
  list<pair<list<Cell*>,int> >::iterator pop_stores_it;
  list<Cell *>::iterator pop_iter;

  ofstream fout,fout2,fout3,fout4,fout5,fout6,fout7,fout8,fout9,fout10;
  ofstream binned_gene_out, contribution_per_type_out;
  ofstream alt_env_robustness,multiple_mut_robustness,multiple_mut_dist_robustness;

  bf::path _path;

  double gcr_scaling;
  double major_dup_rate,major_del_rate,major_ins_rate;

};

bool CellFitnessPairsSortPredicate(const pair<Cell *,double> lhs,const pair<Cell *,double> rhs);

#endif
