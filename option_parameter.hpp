#include  <boost/serialization/export.hpp>
#include <boost/serialization/export.hpp><boost/serialization/export.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/vector.hpp>

using namespace std;

class option_parameter
{
  public std::vector<double> optionset;
  option_parameter( generations, pop_size, Time, mutationrate, change_rate, evo_seed_used, env_seed_used)
  {
    optionset.push_back(generations);
    optionset.push_back(pop_size);
    optionset.push_back(Time);
    optionset.push_back(mutationrate);
    optionset.push_back(change_rate);
    optionset.push_back(evo_seed_used);
    optionset.push_back(env_seed_used);
  } 
  std::vector<double> get_optionset() {return optionset};
}
friend class boost::serialization::access;
template<class Archive>
void serialize(Archive & ar, const unsigned int version)
{
  ar & BOOST_SERIALIZATION_NVP(optionset);
}
