#include <vector>
#include "temp.hpp"


using namespace std;

//defines the environment from the generationstart until the next one starts.
class Parameter_Option_Set
{
  public:
  //contained in an object:
  int generation; // at which timepoint 
  int populationsize;
  int Time;
  double mutationrate;
  double change_rate;
  int evo_seed;
  int env_seed;
  double Target_A;
  double Target_X;
  double Perm; //passive diffusion of A and X
  double N; //conversion rate: how much X do you get for an A
  double Degr; //degradation rate of proteins

  //creation method:
  Parameter_Option_Set(int,int,int,double,double,int,int,double,double,double,double,double);
  Parameter_Option_Set();
  //methods for extracting
  int get_generation (){return generation;};
  int get_populationsize(){return populationsize;};
  int get_Time(){return Time;};
  double get_mutationrate(){return mutationrate;};
  double get_change_rate(){return change_rate;};
  int get_evo_seed(){return evo_seed;};
  int get_env_seed(){return env_seed;};
  double get_Target_A(){return Target_A;};
  double get_Target_X(){return Target_X;};
  double get_Perm(){return Perm;};
  double get_N(){return N;};
  double get_Degr(){return Degr;};

  void set_generation(int i){ generation = i;};
  void set_populationsize (int i){ populationsize = i;};
  void set_Time(int i){ Time = i;};
  void set_mutationrate(double i){mutationrate = i;};
  void set_change_rate (double i){change_rate = i;};
  void set_evo_seed(int i){evo_seed = i ;};
  void set_env_seed(int i){ env_seed = i;};
  void set_Target_A(double i){ Target_A = i;};
  void set_Target_X(double i){ Target_X = i;};
  void set_Perm(double i){ Perm = i;};
  void set_N(double i){ N = i;};
  void set_Degr(double i){ Degr = i;};

  friend class boost::serialization::access;
  template<class Archive>
 void save(Archive & ar, const unsigned int version) const
  {
   ar & BOOST_SERIALIZATION_NVP(generation);
   ar & BOOST_SERIALIZATION_NVP(populationsize);
   ar & BOOST_SERIALIZATION_NVP(Time);
   ar & BOOST_SERIALIZATION_NVP(mutationrate);
   ar & BOOST_SERIALIZATION_NVP(change_rate);
   ar & BOOST_SERIALIZATION_NVP(evo_seed);
   ar & BOOST_SERIALIZATION_NVP(env_seed);
   ar & BOOST_SERIALIZATION_NVP(Target_A);
   ar & BOOST_SERIALIZATION_NVP(Target_X);
   ar & BOOST_SERIALIZATION_NVP(Perm);
   ar & BOOST_SERIALIZATION_NVP(N);
   ar & BOOST_SERIALIZATION_NVP(Degr);
  }
template<class Archive>
 void load(Archive & ar, const unsigned int version)
  {
   ar & BOOST_SERIALIZATION_NVP(generation);
   ar & BOOST_SERIALIZATION_NVP(populationsize);
   ar & BOOST_SERIALIZATION_NVP(Time);
   ar & BOOST_SERIALIZATION_NVP(mutationrate);
   ar & BOOST_SERIALIZATION_NVP(change_rate);
   ar & BOOST_SERIALIZATION_NVP(evo_seed);
   ar & BOOST_SERIALIZATION_NVP(env_seed);
   ar & BOOST_SERIALIZATION_NVP(Target_A);
   ar & BOOST_SERIALIZATION_NVP(Target_X);
   ar & BOOST_SERIALIZATION_NVP(Perm);
   ar & BOOST_SERIALIZATION_NVP(N);
   ar & BOOST_SERIALIZATION_NVP(Degr);
  }
   BOOST_SERIALIZATION_SPLIT_MEMBER()
};

