#include "gene.hpp"
#include <fstream>
#include <boost/serialization/export.hpp>



long int Gene::total_nmbr_genes = 0;

Gene::Gene():typ(NOTYPE), operator_type(0),promotor_strength(0.),tf_list(TrF),
	     number(), binding_polynomial(0.),concentration(0.),next_concentr(0.),
	     nochange(false){}
//,deleted(false)error(false),,name("")

Gene::Gene(Type tp):typ(tp),operator_type(0),
		    promotor_strength(0.),tf_list(TrF),
		    number(++Gene::total_nmbr_genes),
		    binding_polynomial(0.),concentration(0.),next_concentr(0.),nochange(false){}
//,deleted(false),error(false),name("")
Gene::Gene(Type tp, int op_type,double pro_str): typ(tp),operator_type(op_type),
						 promotor_strength(pro_str),tf_list(TrF), 
						 number(++Gene::total_nmbr_genes),
						 binding_polynomial(0.),concentration(STARTCONC),
						 next_concentr(0.),nochange(false){

}


// copy ctor 
Gene::Gene(const Gene & g):typ(g.typ),operator_type(g.operator_type),
			   promotor_strength(g.promotor_strength), tf_list(g.tf_list),
			   number(g.number),
			   binding_polynomial(g.binding_polynomial),concentration(g.concentration),
			   next_concentr(g.next_concentr),nochange(g.nochange){
  ancestor_list.push_front(g.getNumber());
}

// alternative copy constructor when copying cells in petri_dish init; leaves tf_list empty.
Gene::Gene(const Gene & g,int i):typ(g.typ),operator_type(g.operator_type),
				 promotor_strength(g.promotor_strength),tf_list(TrF),
				 number(g.number),
				 binding_polynomial(0.),concentration(g.concentration),
				 next_concentr(0.),nochange(false){

}

void Gene::setChild(Gene* g){
  children.push_front(g);
}

bool Gene::noChildren(){
  return children.empty();
}

void Gene::removeChild(Gene *g){
  for(child_iter = children.begin(); child_iter != children.end();child_iter++){
    if(*child_iter==g){
      children.erase(child_iter);
      break;
    }
  }
}

void Gene::removeParent(){
  parent = NULL;
}

void Gene::informParent(){
  if(parent!= NULL){
    parent ->removeChild(this);
    if(parent -> noChildren()){
      parent -> informParent();
      delete parent;
    }
  }
}

void Gene::informChildren(){
  for(child_iter = children.begin();child_iter != children.end(); child_iter++){
    (*child_iter)->removeParent();
  }
}

Gene* Gene::findRoot(){
  Gene* g = this;
  while(( g -> getAncestor()) != NULL){
    g = g-> getAncestor();
  }
  return g;
}

list<Gene *> Gene::buildTreeList(int start_depth=0, int end_depth=-1){
  int depth=1;
  Gene * root = findRoot();
  list<Gene *> tree_list;
  addNodes(tree_list, root, start_depth, end_depth, depth);
  return tree_list;
  
}

void Gene::addNodes(list<Gene *> & tree_list, Gene * g,
	      int start_depth, int end_depth, int depth){
  if(end_depth != -1 && depth > end_depth) return;
  if(depth >= start_depth) 
    tree_list.push_front(g);

  list<Gene *> children = g -> getOffspring();
  list<Gene *>::iterator child_iter;
  for(child_iter = children.begin(); child_iter != children.end(); child_iter++){
    addNodes(tree_list, *child_iter, start_depth, end_depth, depth+1); 
  }
}


void Gene::resetConcentration(){
  concentration = STARTCONC;
}

void Gene::addTFtoList(TF * tf){
  get_tf_list().addGene(tf);
}


void Gene::notify_tfs_loss(){
  vector<Gene *>  temptfs = get_tf_list().getList();
  for(unsigned int i=0;i<temptfs.size();i++){
    TF * ttf = (TF * )temptfs[i];
    if(ttf != NULL){
      GeneCollection & lst = (*ttf).get_bind_list();
      lst.removeGene(this);
    }
  }
  tf_list.removeAll();
}

void Gene::notify_tfs(){ //in case of a duplication of a gene, the tfs binding to it should be notified of it's existence
  for(unsigned int i= 0; i<tf_list.getList().size();i++){
    TF * tf = (TF *)(tf_list.getList())[i];
    if( tf != NULL){
      (tf -> get_bind_list()).addGene(this);
    }
  }
}

void Gene::set_bind_pol(){   // the binding polynomial is 1. plus sum of all [TFx]*Kbx terms for all x
  double bpol = 1. ;
  for(unsigned int i= 0; i<tf_list.getList().size();i++){
    TF * tf = (TF *)(tf_list.getList())[i];
    if( tf != NULL){
      bpol += (tf -> getKb()) * (tf -> getConcentration());
    }
  }
  binding_polynomial = bpol;
}

double Gene::Reg(){  
  double reg = 0.;
  double tf_regulated = 0.;  	// keep track of the fraction of time that promoter is TF regulated
                                // 1 - fraction(bound by TF) = the fraction of time it is not TF regulated ; this will get reg multiplier 1
  double fraction = 0.;         //fraction of time that operator is in a particular state
	
  for(unsigned int i = 0; i< tf_list.getList().size();i++){
    TF * tf = (TF *)(tf_list.getList())[i];
    if(tf){
      fraction = (( (tf -> getW()) * (tf -> getConcentration()) * (tf -> getKb()) )/ 
		     binding_polynomial );
      reg += (tf -> getEffBound()) * fraction;
      tf_regulated += fraction;
		
      fraction = (( (1. - (tf -> getW())) * (tf -> getConcentration()) * (tf -> getKb()) )/ 
		   binding_polynomial );
      reg +=	(tf -> getEffApo()) * fraction;
      tf_regulated += fraction;
    }
  }
  reg+= 1.0 * (1.0 - tf_regulated);    // the unregulated fraction has activation 1.	
  if((1. - tf_regulated) < 0. ) cout << "something wrong in Gene::Reg() " << endl;
  return reg;
}


bool Gene::EulerStep(){
  nochange = false;
  double dt = H* (promotor_strength * Reg() - Degr * concentration);
  if(concentration +dt < 0){ setNextConcentr(0.);}
  else{ setNextConcentr(concentration + dt);}
  if( fabs(concentration - next_concentr)/concentration < (CHANGE_THRESHOLD*H) ){ 
    nochange = true;   //if the rate of change is less then 0.1%, let cell know
  }
  return nochange;
}

string Gene::toString(){
  ostringstream formatter;	
  formatter << "<" << typ << ',' << operator_type << ">";
  return formatter.str();
}

//-------------------------------- TF MEMBER FUNCTIONS ---------------------------------------

TF::TF():Gene(TrF),ligand(NO),W(0.),binding_sequence(0),Kd(0.),Kb(0.),EffApo(0.),EffBound(0.),bind_list(){}

TF::TF(int op_type,double pro_str,Ligand lig, int bi_seq,double kd,double kb,double effapo,double effbound) :
  Gene(TrF,op_type,pro_str), ligand(lig),W(0.), binding_sequence(bi_seq), Kd(kd), Kb(kb), EffApo(effapo),
  EffBound(effbound),bind_list(GeneCollection()){
    //bind_list = GeneCollection();
  }

TF::TF(const TF& tf):Gene(tf),ligand(tf.ligand),W(tf.W),binding_sequence(tf.binding_sequence),
		     Kd(tf.Kd),Kb(tf.Kb),EffApo(tf.EffApo),EffBound(tf.EffBound),bind_list(tf.bind_list){
}

//alternative copy constr, when copying a cell ( petri_dish init); leaves bind_list empty
TF::TF(const TF& tf,int i):Gene(tf,i),ligand(tf.ligand),W(0.),binding_sequence(tf.binding_sequence),
			   Kd(tf.Kd),Kb(tf.Kb),EffApo(tf.EffApo),EffBound(tf.EffBound){
}

bool TF::op_bind_match(Gene * gene){
  bool match = false;
  if(gene -> get_op_type() == get_bind_seq()) match = true;
  return match;
}

void TF::update_bind_list(GeneCollection * pool){	
  for(unsigned int i = 0;i<pool -> getList().size();i++){
    GeneList * temp = pool -> getList()[i];
    for(unsigned int j = 0;j<temp -> getList().size();j++){
      Gene * g = temp ->  getList()[j];
      if(op_bind_match(g)){
	get_bind_list().addGene(g);   	//add matching gene to this tf's bindlist
	g -> addTFtoList(this);			//add this tf to the genes tflist.
      }
    }
  }
}

//in case of a duplication of tf, the genes it binds, should be notified of it's existence
void TF::notify_genes(){		
  for(unsigned int i= 0; i<bind_list.getList().size();i++){
    GeneList * temp = (bind_list.getList())[i] ;
    for(unsigned int j=0; j< temp ->getList().size();j++){ 
      Gene * g = (temp -> getList())[j];
      if( g != NULL){
	(g -> get_tf_list()).addGene(this);
      }
    }
  }
}

void TF::notify_genes_loss(){
  for(unsigned int i= 0; i< bind_list.getList().size(); i++){
    GeneList * temp = (bind_list.getList())[i] ;
    for(unsigned int j = 0; j< temp ->getList().size();j++){
      Gene * g = (temp -> getList())[j];
      if( g != NULL){
	(g -> get_tf_list()).removeGene(this);
      }
    }
		
  }
  bind_list.empty();
}

bool TF::double_check_pointers(){
  bool doubles = false;
  for(unsigned int i= 0; i<bind_list.getList().size();i++){
    GeneList * temp = (bind_list.getList())[i] ;
    for(unsigned int j=0; j< temp ->getList().size();j++){ 
      Gene * g = (temp -> getList())[j];
      for(unsigned int k=j+1; k< temp ->getList().size();k++){ 
	Gene * g2 = (temp-> getList())[k];
	if(g2 != NULL && g != NULL && g -> getNumber() == g2 -> getNumber()){
	  doubles = true;
	  std::cout << "double gene in tf bindlist" << std::endl;
	  if(g==g2) std::cout << "two pointers to same gene" << std::endl;
	  std::cout << "tf: " << *this << "; genes: " << *g << " & " << *g2 << std::endl;
	  break;
	}
      } 
    }
  }
  return doubles;
}

// set the fraction of this TF proteins that is bound by it's ligand
void TF::setW(double lig_concentration){  
  W = (lig_concentration * Kd) /(1+lig_concentration*Kd);
}

//attributes represent tf repression/activation in the Apo and Bound state respectively
void TF::setAttributes(){
  double par = getEffApo();
  if(  par < 0.33) attribute1 = "--";
  else if(par < 0.85 ) attribute1 = "-";
  else if(par < 1.15) attribute1 = "=";
  else if(par < 3.33) attribute1 = "+";
  else attribute1 = "++";
  par = getEffBound();
  if(  par < 0.33) attribute2 = "--";
  else if(par < 0.85 ) attribute2 = "-";
  else if(par < 1.15) attribute2 = "=";
  else if(par < 3.33) attribute2 = "+";
  else attribute2 = "++";
}

string TF::toString(){
  ostringstream formatter;	
  formatter << setw(15) << getNumber() 
	    << setw(3) << " "      << setw(7) << getType() 
	    << setw(5) << "op:"    << setw(3) << get_op_type() 
	    << setw(5) << "pro:"   << setw(13) << getPromStr() 
	    << setw(7) << "Kd_li:" << setw(13) << Kd 
	    << setw(7) << "Kb_op:" << setw(13) << Kb 
	    << setw(7) << "Apo:"   << setw(13) << EffApo 
	    << setw(7) << "Bound:" << setw(13) << EffBound
	    << setw(7) << "bind:"  << setw(3) << binding_sequence
	    << setw(8) << "ligand:"<< setw(5) << ligand ;	  

  return formatter.str();
}

//-------------------------------- PUMP MEMBER FUNCTIONS ---------------------------------------


Pump::Pump():Gene(PUMP),KpA(0.),KpX(0.),Vmax(0.){}

Pump::Pump(int op_type, double pro_str, double kpA, double kpX, double vmax):Gene(PUMP,op_type,pro_str),
									     KpA(kpA),KpX(kpX),Vmax(vmax){
}

Pump::Pump(const Pump& p):Gene(p),KpA(p.KpA),KpX(p.KpX),Vmax(p.Vmax){};

string Pump::toString(){
  ostringstream formatter;	
  formatter << setw(15) << getNumber() 
	    << setw(3) << " "     << setw(7) << getType() 
	    << setw(5) << "op:"   << setw(3) << get_op_type() 
	    << setw(5) << "pro:"  << setw(13) << getPromStr() 
	    << setw(7) << "KpA:"  << setw(13) <<  KpA 
	    << setw(7) << "KpX:"  << setw(13) << KpX 
	    << setw(7) << "Vmax:" << setw(13) << Vmax;
  return formatter.str();
}

//alternative copy constructor when copying cells in petri_dish init;
Pump::Pump(const Pump& p,int i):Gene(p,i),KpA(p.KpA),KpX(p.KpX),Vmax(p.Vmax){};

//-------------------------------- ENZYME MEMBER FUNCTIONS ----------------------------------------

Enzyme::Enzyme():Gene(ENZYME),Anabolic(),KA(0.),KX(0.),Vmax(0.){}

Enzyme::Enzyme(int op_type, double pro_str,double KA,double KX, double Vmax,bool ana):Gene(ENZYME,op_type,pro_str),
  Anabolic(ana),KA(KA),KX(KX),Vmax(Vmax){	
}

Enzyme::Enzyme(const Enzyme& enz):Gene(enz),Anabolic(enz.Anabolic),KA(enz.KA),KX(enz.KX),Vmax(enz.Vmax){}

//alternative copy constr
Enzyme::Enzyme(const Enzyme& enz,int i):Gene(enz,i),Anabolic(enz.Anabolic),KA(enz.KA),KX(enz.KX),Vmax(enz.Vmax){}

string Enzyme::toString(){
  ostringstream formatter;
  formatter << setw(15) << getNumber() 
	    << setw(3) << " "     << setw(7) << getType() 
	    << setw(5) << "op:"   << setw(3) << get_op_type() 
	    << setw(5) << "pro:"  << setw(13) << getPromStr() 
	    << setw(7) << "KA:"   << setw(13) <<  KA 
	    << setw(7) << "KX:"   << setw(13) << KX 
	    << setw(7) << "Vmax:" << setw(13) << Vmax
	    << setw(7) << "Ana:"  << setw(3) <<  Anabolic; 
	
  return formatter.str();
}

ostream & operator << (ostream & out,Gene & g){
  out<< g.toString();
  return out;
};

ostream & operator << (ostream & out, const Ligand lig){
  if(lig == A) out << "(A)";
  if(lig == X) out << "(X)";
  return out;
}

ostream & operator << (ostream & out,const Type typ){
  switch (typ){
  case TrF: out << "TF"; break;
  case ENZYME: out << "ENZYME"; break;
  case PUMP: out << "PUMP"; break;
  default: out << "NoType"; break;
  }
			
  return out;
};

istream & operator >> (istream & in, Type & typ){
  string trans;
  in >> trans;
  if(!trans.compare("TF"))
    typ = TrF; 
  else if(!trans.compare("ENZYME"))
    typ = ENZYME; 
  else if(!trans.compare("PUMP"))
    typ = PUMP; 
  else typ = NOTYPE;
  return in;
}


namespace po = boost::program_options;

void conflicting_options(const po::variables_map& vm, 
                         const char* opt1, const char* opt2)
{
    if (vm.count(opt1) && !vm[opt1].defaulted() 
        && vm.count(opt2) && !vm[opt2].defaulted())
        throw logic_error(string("Conflicting options '") 
                          + opt1 + "' and '" + opt2 + "'.");
}

/* Function used to check that of 'for_what' is specified, then
   'required_option' is specified too. */
void option_dependency(const po::variables_map& vm,
                        const char* for_what, const char* required_option)
{
  //option must be set explicitly, not by default
    if (vm.count(for_what) && !vm[for_what].defaulted())
        if (vm.count(required_option) == 0 || vm[required_option].defaulted())
            throw logic_error(string("Option '") + for_what 
                              + "' requires option '" + required_option + "'.");
}

void required_option(const po::variables_map& vm, const char* opt)
{
  if(!vm.count(opt) && !vm[opt].defaulted())
    throw logic_error(string("Required option '") + opt + "' is not set.");
}

void required_in_absence_of(const po::variables_map& vm ,const char* required_option , const char* absent_option)
{
  // when defaulted, option counts as present 
  if(vm.count(required_option) == 0 && !vm[required_option].defaulted())
    if( vm.count(absent_option) == 0 && !vm[absent_option].defaulted())
      throw logic_error(string("'") +required_option + "' is required if '" + absent_option + "' is absent");
}

string map_to_string(po::variables_map& vm,bool defaults)
{
  ostringstream formatter;// = ostringstream();
  std::map<std::string, po::variable_value>::iterator map_it = vm.begin();
  for(; map_it != vm.end(); map_it++){
    boost::any v_any = (*map_it).second.value();
    if(!defaults && vm[map_it->first].defaulted()) continue; //decide whether to save defaulted options
    // prints '[key]='
    formatter << map_it->first << "=" ;

    // if option was a bool_switch, value is empty, but to parse config file correctly
    // we have to set a value, so if the option is specified, we give it value 'true'
    // any_cast from a boost::any* to a ValueType* returns NULL if the type doesn't match.
    // You can use it!
    string * s_val = boost::any_cast<string>(&v_any);
    if(s_val!=NULL){
      if(! (*s_val).compare(""))
	formatter << "true" << std::endl;
      else
	formatter << *s_val << std::endl;
      continue;
    }
    int * i_val = boost::any_cast<int>(&v_any);
    if(i_val!=NULL){
      formatter << *i_val << std::endl;
      continue;
    }
    double * d_val = boost::any_cast<double>(&v_any);
    if(d_val!=NULL){
      formatter << *d_val << std::endl;
      continue;
    }
    bool * b_val = boost::any_cast<bool>(&v_any);
    if(b_val!=NULL){
      formatter << *b_val << std::endl;
      continue;
    }
    formatter << "no value found" << endl;
  }
  return formatter.str();
}

BOOST_CLASS_EXPORT(Gene);
BOOST_CLASS_EXPORT(TF);
BOOST_CLASS_EXPORT(Pump);
BOOST_CLASS_EXPORT(Enzyme);
//BOOST_CLASS_TRACKING(Gene, boost::serialization::track_never);
//BOOST_CLASS_TRACKING(TF, boost::serialization::track_never);
//BOOST_CLASS_TRACKING(Pump, boost::serialization::track_never);
//BOOST_CLASS_TRACKING(Enzyme, boost::serialization::track_never);


