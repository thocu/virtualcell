double INS,DUP,DEL,POINT,WGD;
double GCR,SG,POLYP,NOTHING;
double MAX_GCR_FRAC;
double TF_RAT, PUMP_RAT, ENZ_RAT;
double Perm, startAin, startXin, TARGETA, TARGETX, N;
double SIZE_PENALTY ;
int SIZE_MAX;
bool RESTRICT_SIZE;

bool BEFORE_WGD;
int ASSAYS;
//from gene.hpp
double Degr,STARTCONC;

#include <ostream>
#include <algorithm>
#include "cell.hpp"

//#include <boost/serialization/export.hpp>

int Cell::total_number_cells = 0;
double A_out = 0.;
time_t open_time;

Cell::Cell():enzyme_list(),tf_list(),pump_list(),genome(),fout(),grout(),dels(0),dups(0),
	     points(0),majordels(0),majordups(0),majorins(0),wgds(0),tfdels(0),tfdups(0),tfpoints(0),
	     enzymedels(0),enzymedups(0),enzymepoints(0),pumpdels(0),pumpdups(0),pumppoints(0),
	     enz_op_muts(0),pump_op_muts(0),tf_op_muts(0),tf_bind_muts(0),
	     A_in(startAin),nextA_in(0.),X_in(startXin),nextX_in(0.),dAdt(0.),dXdt(0.),
	     prod_rate(0.),Anabolite(0.),parent(),children(),duplication_pairs(),fitness(ASSAYS),expr_A_X(ASSAYS),
	     production(ASSAYS),anabolite(ASSAYS),genes(0),nochange(false),update(true){ }

//copy constructor
Cell::Cell(const Cell& c):fout(),grout(),dels(c.dels),dups(c.dups),
			  points(c.points),majordels(c.majordels),
			  majordups(c.majordups),majorins(c.majorins),
			  wgds(c.wgds),
			  tfdels(c.tfdels),tfdups(c.tfdups),tfpoints(c.tfpoints),
			  enzymedels(c.enzymedels),enzymedups(c.enzymedups),
			  enzymepoints(c.enzymepoints),pumpdels(c.pumpdels),
			  pumpdups(c.pumpdups),pumppoints(c.pumppoints),
			  enz_op_muts(c.enz_op_muts),pump_op_muts(c.pump_op_muts),
			  tf_op_muts(c.tf_op_muts),tf_bind_muts(c.tf_bind_muts),
			  name(c.name),graphname(c.graphname),
			  A_in(c.A_in),nextA_in(c.nextA_in),X_in(c.X_in),
			  nextX_in(c.nextX_in),dAdt(c.dAdt),dXdt(c.dXdt),
			  prod_rate(c.prod_rate),Anabolite(c.Anabolite),
			  survived(c.survived),dead(c.dead),x_position(c.x_position),
			  y_position(c.y_position),Time(c.Time),number(c.number),
			  parent_number(c.parent_number),parent(c.parent),
			  children(c.children),duplication_pairs(c.duplication_pairs),
			  fitness(c.fitness),_fitness(c._fitness),expr_A_X(c.expr_A_X),
			  production(c.production),anabolite(c.anabolite),_production(c._production),
			  genes(c.genes),nochange(c.nochange),update(c.update){
  fout.setf(ios::left);
 
  list<Gene_iter_wrapper>::const_iterator genome_orig;
    
  for(genome_orig = c.genome.begin(); genome_orig != c.genome.end(); genome_orig++){
    switch((*genome_orig).getType()){
    case ENZYME:{
      list<Enzyme>::iterator enz_iter = (*genome_orig).get_enz_it();
      enzyme_list.push_front(Enzyme(*enz_iter,1));
      genome.push_back(Gene_iter_wrapper(enzyme_list.begin())); 
      pool.addGene(&*enzyme_list.begin());
      //cout << " default copied an enzyme" << endl;
      break;
    }
    case TrF:{
      list<TF>::iterator tf_iter = (*genome_orig).get_tf_it();
      tf_list.push_front(TF(*tf_iter,1));
      genome.push_back(Gene_iter_wrapper(tf_list.begin()));
      pool.addGene(&*tf_list.begin());
      // cout << "default copied a tf " << endl;
      break;
    }  
    case PUMP:{
      list<Pump>::iterator pump_iter = (*genome_orig).get_pump_it();
      pump_list.push_front(Pump(*pump_iter,1));
      genome.push_back(Gene_iter_wrapper(pump_list.begin()));
      pool.addGene(&*pump_list.begin());
      //cout << "default copied a pump" << endl;
      break;
      }
    case NOTYPE:{ 
      cout << "tried to pass on an invalid gene type" << endl;
      break;
    }
    }
  }
  //  cout << *this << endl << "--------default copy -----------" << endl;
  fish_pool();
}

//cell copying in reproduction : create child from parent; keep ancestor info
Cell::Cell(Cell& c,string dirname = "OutTest"):pool(),fout(),grout(),dels(c.dels),dups(c.dups),
					       points(c.points),majordels(c.majordels),
					       majordups(c.majordups),majorins(c.majorins),
					       wgds(c.wgds),
					       tfdels(c.tfdels),tfdups(c.tfdups),tfpoints(c.tfpoints),
					       enzymedels(c.enzymedels),enzymedups(c.enzymedups),
					       enzymepoints(c.enzymepoints),pumpdels(c.pumpdels),
					       pumpdups(c.pumpdups),pumppoints(c.pumppoints),
					       enz_op_muts(c.enz_op_muts),pump_op_muts(c.pump_op_muts),
					       tf_op_muts(c.tf_op_muts),tf_bind_muts(c.tf_bind_muts),
					       A_in(c.A_in),nextA_in(0.),X_in(c.X_in),nextX_in(0.),
					       dAdt(0.),dXdt(0.),prod_rate(0),Anabolite(0),
					       survived(false),dead(false),x_position(c.x_position),
					       y_position(c.y_position),Time(0),
					       number(++Cell::total_number_cells),
					       parent_number(0),parent(),children(),
					       duplication_pairs(c.duplication_pairs),
					       fitness(ASSAYS),_fitness(0),expr_A_X(ASSAYS),
					       production(ASSAYS),anabolite(ASSAYS),_production(0),
					       genes(c.genes),nochange(false),update(true){

  ostringstream formatter;
  formatter << "cell" << number;
  name = formatter.str();
  fout.setf(ios::left);

  formatter.str("");
  formatter << "graph_cell" << number << ".dot";
  graphname = formatter.str();

  parent = &c;
  parent_number = c.get_number();
  c.setChild(this);
  
  list<Gene_iter_wrapper>::iterator genome_orig;
    
  for(genome_orig = c.genome.begin(); genome_orig != c.genome.end(); genome_orig++){
    switch((*genome_orig).getType()){
    case ENZYME:{
      list<Enzyme>::iterator enz_iter = (*genome_orig).get_enz_it();
      enzyme_list.push_front(Enzyme(*enz_iter,1));
      genome.push_back(Gene_iter_wrapper(enzyme_list.begin())); 
      pool.addGene(&*enzyme_list.begin());
      //      cout << genome.back() << endl;
      break;
    }
    case TrF:{
      list<TF>::iterator tf_iter = (*genome_orig).get_tf_it();
      tf_list.push_front(TF(*tf_iter,1));
      genome.push_back(Gene_iter_wrapper(tf_list.begin()));
      pool.addGene(&*tf_list.begin());
      //cout << "copied tf " << endl;
      break;
    }  
    case PUMP:{
      list<Pump>::iterator pump_iter = (*genome_orig).get_pump_it();
      pump_list.push_front(Pump(*pump_iter,1));
      genome.push_back(Gene_iter_wrapper(pump_list.begin()));
      pool.addGene(&*pump_list.begin());
      // cout << "copied pump " << endl;
      break;
    }
    case NOTYPE:{ 
      cout << "tried to pass on an invalid gene type" << endl;
      break;
    }
    }
  }
  // cout << genome.back() << endl;
  fish_pool();
}

//cell copying in cloning : create child from parent; remove ancestor info
  Cell::Cell(Cell& c,int i,string dirname)    :pool(),fout(),grout(),dels(0),dups(0),
                                               points(0),majordels(0),majordups(0),majorins(0),
					       wgds(0),
					       tfdels(0),tfdups(0),tfpoints(0),enzymedels(0),
                                               enzymedups(0),
					       enzymepoints(0),pumpdels(0),
					       pumpdups(0),pumppoints(0),
					       enz_op_muts(0),pump_op_muts(0),
					       tf_op_muts(0),tf_bind_muts(0),
					       A_in(startAin),nextA_in(0.),X_in(startXin),nextX_in(0.),
					       dAdt(0.),dXdt(0.),prod_rate(0),Anabolite(0),
					       survived(false),dead(false),x_position(0),
					       y_position(0),Time(0),
					       number(++Cell::total_number_cells),
					       parent_number(0),parent(),children(),
					       duplication_pairs(0),
					       fitness(ASSAYS),_fitness(0),expr_A_X(ASSAYS),
					       production(ASSAYS),anabolite(ASSAYS),_production(0),
					       genes(c.genes),nochange(false),update(true){

  ostringstream formatter;
  formatter << "cell" << number;
  name = formatter.str();
  fout.setf(ios::left);

  formatter.str("");
  formatter << "graph_cell" << number << ".dot";
  graphname = formatter.str();

  //  parent = &c;
  //  parent_number = c.get_number();
  //  c.setChild(this);
  
  list<Gene_iter_wrapper>::iterator genome_orig;
    
  for(genome_orig = c.genome.begin(); genome_orig != c.genome.end(); genome_orig++){
    switch((*genome_orig).getType()){
    case ENZYME:{
      list<Enzyme>::iterator enz_iter = (*genome_orig).get_enz_it();
      enzyme_list.push_front(Enzyme(*enz_iter,1));
      genome.push_back(Gene_iter_wrapper(enzyme_list.begin())); 
      pool.addGene(&*enzyme_list.begin());
      //      cout << genome.back() << endl;
      break;
    }
    case TrF:{
      list<TF>::iterator tf_iter = (*genome_orig).get_tf_it();
      tf_list.push_front(TF(*tf_iter,1));
      genome.push_back(Gene_iter_wrapper(tf_list.begin()));
      pool.addGene(&*tf_list.begin());
      //cout << "copied tf " << endl;
      break;
    }  
    case PUMP:{
      list<Pump>::iterator pump_iter = (*genome_orig).get_pump_it();
      pump_list.push_front(Pump(*pump_iter,1));
      genome.push_back(Gene_iter_wrapper(pump_list.begin()));
      pool.addGene(&*pump_list.begin());
      // cout << "copied pump " << endl;
      break;
    }
    case NOTYPE:{ 
      cout << "tried to pass on an invalid gene type" << endl;
      break;
    }
    }
  }
  fish_pool();
}


//primary constructor
Cell::Cell(double nmbr,string dirname = "OutTest"):pool(),fout(),grout(),dels(0),dups(0),
						   points(0),majordels(0),majordups(0),majorins(0),
						   wgds(0),
						   tfdels(0),tfdups(0),tfpoints(0),enzymedels(0),
						   enzymedups(0),enzymepoints(0),pumpdels(0),
						   pumpdups(0),pumppoints(0),
						   enz_op_muts(0),pump_op_muts(0),tf_op_muts(0),tf_bind_muts(0),
						   A_in(startAin),nextA_in(0.),X_in(startXin),nextX_in(0.),
						   dAdt(0.),dXdt(0.),prod_rate(0),Anabolite(0),
						   survived(false),dead(false),x_position(0),
						   y_position(0),Time(0),number(++Cell::total_number_cells),
						   parent_number(0),parent(),children(),
						   fitness(ASSAYS),_fitness(0),
						   expr_A_X(ASSAYS),production(ASSAYS),anabolite(ASSAYS),
						   _production(0),nochange(false),update(true){
  ostringstream formatter;
  formatter << "cell" << number;
  name = formatter.str();
  fout.setf(ios::left);

  formatter.str("");
  formatter << "graph_cell"<< number<<".dot";
  graphname = formatter.str();
							      
  int nmbr_genes = int(nmbr)+ int(sqrt(nmbr)*pop_rand.normal()); 
  // in case nmbr = 10,the cell would have on average 10 genes 
  // with standard dev of sqrt(10)

  if(nmbr_genes < 0) nmbr_genes = 0;
  genes = nmbr_genes;
  
  for(int i = 0; i< nmbr_genes; i++){
  //start adding 'nmbr_genes' genes with equal changes for TF, ENZYME PUMP
    double rand = pop_rand.uniform();
    double total = TF_RAT + PUMP_RAT + ENZ_RAT ;
    if(rand< (PUMP_RAT/total)){  // 1./3.
      pump_list.push_front(Pump(pop_rand.uniform_int10(),pow(10.,pop_rand.uniform2()), 
				pow(10.,pop_rand.uniform2()),pow(10.,pop_rand.uniform2()),
				pow(10.,pop_rand.uniform2())));
      
      genome.push_back( Gene_iter_wrapper(pump_list.begin()) );
      
      pool.addGene(&*(pump_list.begin()));
    }
    else if(rand< ((PUMP_RAT+TF_RAT)/total)){ // 2./3.
      Ligand l = X;
      if(pop_rand.uniform() < 0.5) l = A;
      tf_list.push_front(TF(pop_rand.uniform_int10(),pow(10.,pop_rand.uniform2()),l,
			    pop_rand.uniform_int10(),pow(10.,pop_rand.uniform2()),
			    pow(10.,pop_rand.uniform2()),pow(10.,pop_rand.uniform2()),
			    pow(10.,pop_rand.uniform2())));
      
      genome.push_back( Gene_iter_wrapper(tf_list.begin()) );
      
      pool.addGene(&*(tf_list.begin()));
    }
    else {
      bool is_anabolic = false;
      if(pop_rand.uniform() < 0.5) is_anabolic = true;
      enzyme_list.push_front(Enzyme(pop_rand.uniform_int10(),pow(10.,pop_rand.uniform2()),
				    pow(10.,pop_rand.uniform2()),pow(10.,pop_rand.uniform2()),
				    pow(10.,pop_rand.uniform2()),is_anabolic));
      
      genome.push_back( Gene_iter_wrapper(enzyme_list.begin()) );
      
      pool.addGene(&*(enzyme_list.begin()));
    }
  }  
  fish_pool();
}


// test constructor : cells have strict genome sizes

Cell::Cell(int nmbr, string strict,string dirname = "OutTest"):pool(),fout(),grout(),dels(0),dups(0),
							       points(0),majordels(0),majordups(0),majorins(0),
							       wgds(0),
							       tfdels(0),tfdups(0),tfpoints(0),enzymedels(0),
							       enzymedups(0),enzymepoints(0),pumpdels(0),
							       pumpdups(0),pumppoints(0),
							       enz_op_muts(0),pump_op_muts(0),
							       tf_op_muts(0),tf_bind_muts(0),
							       A_in(startAin),
							       nextA_in(0.),X_in(startXin),nextX_in(0.),
							       dAdt(0.),dXdt(0.),Time(0),
							       number(++Cell::total_number_cells),
							       parent_number(0),parent(),children(),
							       fitness(ASSAYS),expr_A_X(ASSAYS),
							       production(ASSAYS),anabolite(ASSAYS),
							       nochange(false),update(true){
  if(strict == "strict_size"){

  ostringstream formatter;
  formatter << "/cell" << number;
  name = formatter.str();
  fout.setf(ios::left);

  formatter.str("");
  formatter << "/graph_cell"<< number<<".dot";
  graphname = formatter.str();
							      
  //  dels = dups = points =  = 0;

  int nmbr_genes = nmbr;
  genes = nmbr_genes;   
  for(int i = 0; i< nmbr_genes; i++){
    double rand = pop_rand.uniform();
    if(rand< (1./3.)){
      pump_list.push_front(Pump(pop_rand.uniform_int10(),pow(10.,pop_rand.uniform2()),  
				pow(10.,pop_rand.uniform2()),pow(10.,pop_rand.uniform2()),
				pow(10.,pop_rand.uniform2())));
      pool.addGene(&*(pump_list.begin()));
    }
    else if(rand< (2./3.)){
      Ligand l = X;
      if(pop_rand.uniform() < 0.5) l = A;
      tf_list.push_front(TF(pop_rand.uniform_int10(),pow(10.,pop_rand.uniform2()),l,
			    pop_rand.uniform_int10(),pow(10.,pop_rand.uniform2()),
			    pow(10.,pop_rand.uniform2()),pow(10.,pop_rand.uniform2()),
			    pow(10.,pop_rand.uniform2())));
      pool.addGene(&*(tf_list.begin()));
    }
    else {
      bool is_anabolic = false;
      if(pop_rand.uniform() < 0.5) is_anabolic = true; 
      enzyme_list.push_front(Enzyme(pop_rand.uniform_int10(),pow(10.,pop_rand.uniform2()),
				    pow(10.,pop_rand.uniform2()),pow(10.,pop_rand.uniform2()),
				    pow(10.,pop_rand.uniform2()),is_anabolic));
      pool.addGene(&*(enzyme_list.begin()));
    }
  }
  fish_pool();
  }
  else cout << "error in use of test constructor " << endl;
}

Cell::~Cell(){
}

void Cell::fish_pool(){
  for (tfs_iter=tf_list.begin(); tfs_iter != tf_list.end(); tfs_iter++)
    {
      (*tfs_iter).update_bind_list(&pool);
    }
  pool.empty();	
}

void Cell::fish_all_genes(TF & tf){
  for (tfs_iter=tf_list.begin(); tfs_iter != tf_list.end(); tfs_iter++)
    {
      if(tf.op_bind_match(&*tfs_iter)){
	tf.get_bind_list().addGene(&*tfs_iter);
	(*tfs_iter).addTFtoList(&tf);
      }
    }
  for(enzymes_iter= enzyme_list.begin(); enzymes_iter != enzyme_list.end(); enzymes_iter++){
    if(tf.op_bind_match(&*enzymes_iter)){
      tf.get_bind_list().addGene(&*enzymes_iter);
      (*enzymes_iter).addTFtoList(&tf);
    }
  }
  for(pumps_iter= pump_list.begin(); pumps_iter != pump_list.end(); pumps_iter++){
    if(tf.op_bind_match(&*pumps_iter)){
      tf.get_bind_list().addGene(&*pumps_iter);
      (*pumps_iter).addTFtoList(&tf);
    }
  }
}

Gene_iter_wrapper Cell::getGene(int id){
  Gene_iter_wrapper giwi = Gene_iter_wrapper();
  for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
    if( genome_iter -> get_gene_number() == id ){
      giwi = *genome_iter;
      break;
    }
  }
  return giwi;
}

void Cell::setChild(Cell* c){
  children.push_front(c);
}

bool Cell::noChildren(){
  return children.empty();
}

void Cell::removeChild(Cell* c){
  for(child_iter = children.begin(); child_iter !=children.end();child_iter++){
    if(*child_iter== c){ 
      children.erase(child_iter);
      break;
    }
  }
}

void Cell::removeParent(){
  parent = NULL;
}

void Cell::informParent(){
  if(parent != NULL){
    parent->removeChild(this);
    if(parent -> noChildren()){
      parent -> informParent();
      delete parent;
    }
  }
}

void Cell::informChildren(){
  for(child_iter = children.begin();child_iter != children.end();child_iter++){
    (*child_iter)->removeParent();
  }
}

Cell*  Cell::findRoot(){
  Cell * p = this;
  while((p -> getAncestor()) != NULL){
    p = p -> getAncestor();
  }
  return p;
}

bool Cell::CalcNextProt(){
  nochange = true;
  bool nocellchange;
  for (tfs_iter=tf_list.begin(); tfs_iter != tf_list.end(); tfs_iter++)
    {		
      setW((*tfs_iter));
      (*tfs_iter).set_bind_pol();
      nocellchange = (*tfs_iter).EulerStep();
      nochange = nochange && nocellchange;
	
    }  
	
  for (enzymes_iter=enzyme_list.begin(); enzymes_iter != enzyme_list.end(); enzymes_iter++)
    {	
      (*enzymes_iter).set_bind_pol();
      nocellchange = (*enzymes_iter).EulerStep();
      nochange = nochange && nocellchange;
    }

  for (pumps_iter=pump_list.begin(); pumps_iter != pump_list.end(); pumps_iter++)
    {		
      (*pumps_iter).set_bind_pol();
      nocellchange = (*pumps_iter).EulerStep();
      nochange = nochange && nocellchange;
    }
  return nochange;
}

void Cell::SetNewProt(){
  for (enzymes_iter=enzyme_list.begin(); enzymes_iter != enzyme_list.end(); enzymes_iter++)
    {
      (*enzymes_iter).setConcentration((*enzymes_iter).getNextConcentr());
    }
  for (tfs_iter=tf_list.begin(); tfs_iter != tf_list.end(); tfs_iter++)
    {
      (*tfs_iter).setConcentration((*tfs_iter).getNextConcentr());
    }
  for (pumps_iter=pump_list.begin(); pumps_iter != pump_list.end(); pumps_iter++)
    {
      (*pumps_iter).setConcentration((*pumps_iter).getNextConcentr());
    }
}

void Cell::setW(TF &tf){	
// set the fraction of this TF proteins that is bound by it's ligand
  if(tf.getLigand() == A){
    tf.setW( A_in ); 
  }
  if(tf.getLigand() == X){
    tf.setW( X_in );
  }	
}

bool Cell::CalcNextMol(){
  pump_rate = 0.;
  anabolic_rate = 0.;
  catabolic_rate = 0.;
  leak_rate = 0.;

  nochange = false;
  error = false;
  dAdt = 0.;
  dXdt = 0.;
  for (enzymes_iter=enzyme_list.begin(); enzymes_iter != enzyme_list.end(); enzymes_iter++)
    {
      if((*enzymes_iter).isAnabolic()){
	anabolic_rate += Anabolise(*enzymes_iter);
      }
      else{
	catabolic_rate += Catabolise(*enzymes_iter);
      }
    }
  for (pumps_iter=pump_list.begin(); pumps_iter != pump_list.end(); pumps_iter++)
    {
      pump_rate += Pumping(*pumps_iter,A_out);
    }
  leak_rate += PassiveA(A_out);
  if(A_in + dAdt < 0.){
    nextA_in = 0.;
    error=true;
  }
  else{ nextA_in = A_in + dAdt;}
  if(X_in + dXdt < 0.){ 
    nextX_in = 0.;
    error = true;
  }
  else{ nextX_in = X_in + dXdt;}
  
  bool noAchange = false;
  bool noXchange = false;
  if(A_in == nextA_in ){
    noAchange = true;
  }
  else if(fabs(A_in - nextA_in)/A_in < (CHANGE_THRESHOLD*H) ){    
       //if concentration A_in changes less then 0.1% set noAchange = true
    noAchange = true;
  }
  if(X_in == nextX_in  ){
    noXchange = true;
  }
  else if(fabs(X_in - nextX_in)/X_in < (CHANGE_THRESHOLD*H)) {
    noXchange = true;
  }
  nochange = noAchange && noXchange;
  return nochange;
}

void Cell::SetNewMol(){
  A_in = nextA_in;
  X_in = nextX_in;
}

double Cell::Anabolise(Enzyme& enz){
  double consumption = enz.getConcentration() * 
    A_in* X_in * enz.getVmax() / ( (A_in + enz.getKA())*(X_in + enz.getKX()) );
  dAdt += Euler( -1.*consumption);
  dXdt += Euler( -1.*consumption);
  //  prod_rate = consumption;
  return consumption;
}

double Cell::Catabolise(Enzyme& enz){
  double consumption = enz.getConcentration() * A_in * enz.getVmax()/ (A_in + enz.getKA() ); 
  dAdt += Euler( -1.* consumption );
  dXdt += Euler( N * consumption);
  return consumption;
}

double Cell::Pumping(Pump& pump, double Aout){
  double flux = Aout* X_in* pump.getVmax() * pump.getConcentration()/
    ((Aout + pump.getKpA()) * (X_in + pump.getKpX()));
  dAdt +=Euler(flux);
  dXdt += Euler(-flux);
  return flux;
}

double Cell::PassiveA(double Aout){
  double flux = ( Aout - A_in ) *Perm;
  dAdt += Euler( flux );
  return flux;
}


double Cell::Euler(double change){
  return H* change;
}

//return if the Update step was successful

bool Cell::Update(int steps=1){
  if(update){ 
    for(int i = 0; i < steps; i++){
      //if concentrations of geneproducts & A_in X_in haven't changed more then
      bool no_mol_change = CalcNextMol();          
      if(error){
	double old_H = H;
	H=H/STEP_REDUCTION;
	//	std::cout << H <<", ";
	Update(int(STEP_REDUCTION));
	H=old_H;
      }
      // 0.1% last timestep, don't update anymore.
      bool no_prot_change = CalcNextProt();       
      update = !(no_mol_change && no_prot_change);
      
      SetNewMol();
      SetNewProt();
      Anabolite += anabolic_rate *H;
      //update = !(no_mol_change && no_prot_change);
    }
  }
  else Anabolite += anabolic_rate*H;
  //  Time++;
  return update;
}

// contribution of a gene type is an average of contributions of 
// all genes belonging to that gene type
double Cell::tfs_contribution(){
  double type_contribution = 0.;
  for (tfs_iter=tf_list.begin(); tfs_iter != tf_list.end(); tfs_iter++){
    type_contribution += (*tfs_iter).getFitnessContribution();  
  }
  if(tf_list.size() > 0) type_contribution /= tf_list.size();
  return type_contribution;
}
double Cell::enzymes_contribution(){
  double type_contribution = 0.;
  for(enzymes_iter= enzyme_list.begin(); enzymes_iter != enzyme_list.end(); enzymes_iter++){
    type_contribution += (*enzymes_iter).getFitnessContribution();
  }
  if(enzyme_list.size() > 0) type_contribution /= enzyme_list.size();
  return type_contribution;
}
double Cell::pumps_contribution(){
  double type_contribution = 0.;
  for(pumps_iter= pump_list.begin(); pumps_iter != pump_list.end(); pumps_iter++){
     type_contribution += (*pumps_iter).getFitnessContribution();
  }
  if(pump_list.size() > 0) type_contribution /= pump_list.size();
  return type_contribution;
}

int Cell::gene_nmbr_max(){
  int max_gene_nmbr = 0;  
  for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
    int gene_number = genome_iter -> get_gene_number();
    if(gene_number > max_gene_nmbr) max_gene_nmbr = gene_number;
  }
  return max_gene_nmbr;
}


// if the template cell has a genes with identical # as that of genes in genome, those genes'
// params will be reset to values in the template-cell gene.
void Cell::reset_genes_to(Cell * _template,bool has_duplication){
  list<Gene_iter_wrapper>::iterator genome_iter2;
  for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
    int gene_number = genome_iter -> get_gene_number();
    for(genome_iter2 = (_template -> genome).begin(); genome_iter2 != (_template->genome).end(); genome_iter2++){
      if(gene_number == genome_iter2 -> get_gene_number()){
	//	std::cout << "reseting a gene" << std::endl;
	switch(genome_iter -> getType()){
	case TrF:   
	  reset_gene(*(genome_iter -> get_tf_it()),*(genome_iter2 -> get_tf_it()));
	  break;
	case ENZYME: 
	  reset_gene(*(genome_iter -> get_enz_it()),*(genome_iter2 -> get_enz_it()));
	  break;
	case PUMP:
	  reset_gene(*(genome_iter -> get_pump_it()),*(genome_iter2 -> get_pump_it()));
	  break;
	case NOTYPE: cout << "tried to reset an invalid gene type " << endl;
	  break;
	}      
      }
    }
  }
  if(has_duplication){
    list<pair<int,int> > duplications;
    if(duplication_pairs.size() > 0)
      duplications = duplication_pairs.front();
    else return;
    list<pair<int,int> >::iterator dup_iter;
    for(dup_iter = duplications.begin(); dup_iter != duplications.end(); dup_iter++){
      list<Gene_iter_wrapper>::iterator parent_gene;
      list<Gene_iter_wrapper>::iterator child_gene;
      bool child_gene_found = false;
      bool parent_gene_found = false;
      for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
	if(dup_iter -> second == genome_iter -> get_gene_number()){
	  child_gene = genome_iter; 
	  child_gene_found = true;
	  break;
	}
      }
      for(genome_iter2 = (_template -> genome).begin(); genome_iter2 != (_template->genome).end(); genome_iter2++){
	if(dup_iter -> first == genome_iter2 -> get_gene_number()){
	  parent_gene = genome_iter2; 
	  parent_gene_found = true;
	  break;
	}
      }
      if(! (child_gene_found && parent_gene_found)){
	std::cout << "no pair found" << std::endl;
	continue; 
      }
      switch(parent_gene -> getType()){
      case TrF:   
	reset_gene(*(child_gene -> get_tf_it()),*(parent_gene -> get_tf_it()));
	break;
      case ENZYME: 
	reset_gene(*(child_gene -> get_enz_it()),*(parent_gene -> get_enz_it()));
	break;
      case PUMP:
	reset_gene(*(child_gene -> get_pump_it()),*(parent_gene -> get_pump_it()));
	break;
      case NOTYPE: cout << "tried to reset an invalid gene type " << endl;
	break;
      }
    }
  }
}

void Cell::reset_gene(Enzyme & enz, Enzyme & _template){
  if(enz.get_op_type() != _template.get_op_type()){
    enz.set_op_type(_template.get_op_type());
    enz.notify_tfs_loss();
    pool.addGene(&enz);
  }
  enz.setPromStr(_template.getPromStr());
  enz.setKA(_template.getKA());
  enz.setKX(_template.getKX());
  enz.setVmax(_template.getVmax());
}

void Cell::reset_gene(Pump & pump, Pump & _template){
  if(pump.get_op_type() != _template.get_op_type()){
    pump.set_op_type(_template.get_op_type());
    pump.notify_tfs_loss();
    pool.addGene(&pump);
  }
  pump.setPromStr(_template.getPromStr());
  pump.setKpA(_template.getKpA());
  pump.setKpX(_template.getKpX());
  pump.setVmax(_template.getVmax());
}

void Cell::reset_gene(TF & tf, TF & _template){
 if(tf.get_op_type() != _template.get_op_type()){
    tf.set_op_type(_template.get_op_type());
    tf.notify_tfs_loss();
    pool.addGene(&tf);
  }
  tf.setPromStr(_template.getPromStr());
  if(tf.get_bind_seq() != _template.get_bind_seq()){
    tf.set_bind_seq(_template.get_bind_seq());
    tf.notify_genes_loss();
    fish_all_genes(tf);
  }
  tf.setKd(_template.getKd());
  tf.setKb(_template.getKb());
  tf.setEffApo(_template.getEffApo());
  tf.setEffBound(_template.getEffBound());
  tf.setLigand(_template.getLigand());
}


double Cell::set_par(Pump & pump,int par,double val){
  double old_val = 0.;
  switch (par){
  case 2:
    old_val = pump.getPromStr();
    pump.setPromStr(val);
    break;
  case 3:
    old_val = pump.getKpA();
    pump.setKpA(val);
    break;
  case 4:
    old_val = pump.getKpX();
    pump.setKpX(val);
    break;
  case 5:
    old_val = pump.getVmax();
    pump.setVmax(val);
    break;
  default:
    std::cout << "wrong parameter choice" << std::endl;
  }
  return old_val;
}

double Cell::set_par(TF & tf,int par,double val){
  double old_val = 0.;
  switch (par){
  case 2:
    old_val = tf.getPromStr();
    tf.setPromStr(val);
    break;
  case 3:
    old_val = tf.getKd();
    tf.setKd(val);
    break;
  case 4:
    old_val = tf.getKb();
    tf.setKb(val);
    break;
  case 5:
    old_val = tf.getEffBound();
    tf.setEffBound(val);
    break;
  case 6:
    old_val = tf.getEffApo();
    tf.setEffApo(val);
    break;
  default:
    std::cout << "wrong parameter choice" << std::endl;
  }
  return old_val;
}

double Cell::set_par(Enzyme & enzyme,int par,double val){
  double old_val = 0.;
  switch (par){
  case 2:
    old_val = enzyme.getPromStr();
    enzyme.setPromStr(val);
    break;
  case 3:
    old_val = enzyme.getKA();
    enzyme.setKA(val);
    break;
  case 4:
    old_val = enzyme.getKX();
    enzyme.setKX(val);
    break;
  case 5:
    old_val = enzyme.getVmax();
    enzyme.setVmax(val);
    break;
  default:
    std::cout << "wrong parameter choice" << std::endl;
  }
  return old_val;
}

void Cell::pointMutate(Enzyme & enz){
	
  double rand = evo_rand.uniform();
  int par = int(evo_rand.uniform() * 5. +1.);    
  //choose which parameter to mutatate ( only 1 per round)
  int rand_int;
  double newval;

  switch (par){
  case 1: rand_int = evo_rand.uniform_int10();// int(evo_rand.uniform()*10 + 1.);
    enz.set_op_type(rand_int);
    if(rand_int != enz.get_op_type()){
      enz.notify_tfs_loss();
      pool.addGene( &enz);
    }
    enz_op_muts++;
    break;
  case 2: newval = enz.getPromStr() * pow(10.,(rand - 0.5));
    if(newval <0.1) newval = 0.1;       //multiply with pow(10, [-0.5,0.5])
    if(newval > 10.) newval = 10.;
    enz.setPromStr(newval);
    break;
  case 3: newval = enz.getKA()* pow(10.,(rand - 0.5));
    if(newval <0.1) newval = 0.1;                         
    if(newval > 10.) newval = 10.;
    enz.setKA(newval);
    break;
  case 4: newval = enz.getKX() * pow(10.,(rand - 0.5));
    if(newval <0.1) newval = 0.1; 
    if(newval > 10.) newval = 10.;
    enz.setKX(newval);
    break;
  case 5: newval = enz.getVmax() * pow(10.,(rand - 0.5));
    if(newval <0.1) newval = 0.1; 
    if(newval > 10.) newval = 10.; 
    enz.setVmax(newval);
    break;
  }
  enzymepoints++;
  points++;		
    // case 6: ;//switch from anabolic to catabolic or vice versa
}

void Cell::pointMutate(Pump & p){

  double rand = evo_rand.uniform();
  int par = int(evo_rand.uniform() * 5. +1.);
  int rand_int;
  double newval;

  switch (par){
  case 1: rand_int = evo_rand.uniform_int10(); //int(evo_rand.uniform()*10 +1.);
    p.set_op_type(rand_int);
    if(rand_int != p.get_op_type()){
      p.notify_tfs_loss();
      pool.addGene( &p);
    }
    pump_op_muts++;
    break;
  case 2: newval = p.getPromStr() * pow(10.,(rand - 0.5));
    if(newval <0.1) newval = 0.1;  //multiply with pow(10, [-0.5,0.5])
    if(newval > 10.) newval = 10.;
    p.setPromStr(newval);
    break;
  case 3: newval = p.getKpA() * pow(10.,(rand - 0.5));
    if(newval <0.1) newval = 0.1;  //multiply with pow(10, [-0.5,0.5])
    if(newval > 10.) newval = 10.;
    p.setKpA(newval);
    break;
  case 4: newval = p.getKpX() * pow(10.,(rand - 0.5));
    if(newval <0.1) newval = 0.1;  //multiply with pow(10, [-0.5,0.5])
    if(newval > 10.) newval = 10.;
    p.setKpX(newval);
    break;
  case 5: newval = p.getVmax() * pow(10.,(rand - 0.5));
    if(newval <0.1) newval = 0.1;  //multiply with pow(10, [-0.5,0.5])
    if(newval > 10.) newval = 10.;
    p.setVmax(newval);
    break;
  }
  pumppoints++;
  points++;		
}

void Cell::pointMutate(TF & tf){ 

  double rand = evo_rand.uniform();
  int par = int(evo_rand.uniform() * 8. +1.);
  int rand_int;
  double newval;

  switch (par){
  case 1: rand_int = evo_rand.uniform_int10(); // int(evo_rand.uniform()*10 + 1.);
    tf.set_op_type(rand_int);
    if(rand_int != tf.get_op_type()){
      tf.notify_tfs_loss();
      pool.addGene(&tf);
    }
    tf_op_muts++;
    break;
  case 2: newval = tf.getPromStr() * pow(10.,(rand - 0.5));
    if(newval <0.1) newval = 0.1;  //multiply with pow(10, [-0.5,0.5])
    if(newval > 10.) newval = 10.;
    tf.setPromStr( newval);    
    break;
  case 3: rand_int = evo_rand.uniform_int10(); // int(evo_rand.uniform()*10. + 1.);
    tf.set_bind_seq(rand_int);
    if(rand_int != tf.get_bind_seq()){
      tf.notify_genes_loss();
      fish_all_genes(tf);
    }
    tf_bind_muts++;
    break;
  case 4: newval = tf.getKd() * pow(10.,(rand - 0.5));
    if(newval <0.1) newval = 0.1;  //multiply with pow(10, [-0.5,0.5])
    if(newval > 10.) newval = 10.;
    tf.setKd(newval);
    break;
  case 5: newval = tf.getKb() * pow(10.,(rand - 0.5));
    if(newval <0.1) newval = 0.1;  //multiply with pow(10, [-0.5,0.5])
    if(newval > 10.) newval = 10.;
    tf.setKb(newval);
    break;
  case 6: newval = tf.getEffApo() * pow(10.,(rand - 0.5));
    if(newval <0.1) newval = 0.1;  //multiply with pow(10, [-0.5,0.5])
    if(newval > 10.) newval = 10.;
    tf.setEffApo(newval);
    break;
  case 7: newval = tf.getEffBound() * pow(10.,(rand - 0.5));
    if(newval <0.1) newval = 0.1;  //multiply with pow(10, [-0.5,0.5])
    if(newval > 10.) newval = 10.;
    tf.setEffBound(newval);
    break;
  case 8: if( rand < 0.5 ) tf.setLigand(X);
    else tf.setLigand(A);
    break;
  }
  tfpoints++;
  points++;
}

//used by mutateGenes3(..) only
void Cell::pointMutate(list<Gene_iter_wrapper>::iterator wrap_iter){
  switch((*wrap_iter).getType()){
  case TrF:   
    pointMutate( *((*wrap_iter).get_tf_it()) );
    break;
  case ENZYME: 
    pointMutate( *((*wrap_iter).get_enz_it()) );

    break;
  case PUMP:
    pointMutate( *((*wrap_iter).get_pump_it()) );

    break;
  case NOTYPE: cout << "tried to pointmutate an invalid gene type " << endl;
    break;
  }
}

void Cell::Insertion(list<Gene_iter_wrapper>::iterator insert_before,
		     list<Gene_iter_wrapper> sequence){
  genome.splice(insert_before,sequence);
  majorins++;
}

void Cell::majorDup(list<Gene_iter_wrapper>::iterator insert_before,
		    list<Gene_iter_wrapper> sequence){
  list<Gene_iter_wrapper>::iterator temp = insert_before;
  temp--;
  genome.splice(insert_before,sequence);     
  //duplicate a stretch of genome
  temp++;  // now points to first insterted element from sequence
  pair<int, int> dup_pair;
  list<pair<int,int> > dup_sequence;
  for(genome_iter = temp; genome_iter != insert_before; genome_iter++){   
  //now make the gene wrappers point to new gene duplicates
    dup_pair = Dup_helper(genome_iter);
    dup_sequence.push_front(dup_pair);
  }
  duplication_pairs.push_front(dup_sequence);
  majordups++;
}

void Cell::singleDupInPlace(list<Gene_iter_wrapper>::iterator insert_before){
  Gene_iter_wrapper _template = *insert_before;
  list<Gene_iter_wrapper>::iterator temp = insert_before;
  temp--;
  genome.insert(insert_before,_template);     
  temp++; // now points to the inserted element
  pair<int, int> dup_pair;
  list<pair<int,int> > dup_sequence;
  //now make the gene wrapper point to the new gene duplicate and create the duplicate in the process
  dup_pair = Dup_helper(temp);
  dup_sequence.push_front(dup_pair);
  duplication_pairs.push_front(dup_sequence);
}


pair<int,int> Cell::Dup_helper(list<Gene_iter_wrapper>::iterator wrap_iter){
  Gene_iter_wrapper wrap;
  int original;
  int copy;
  pair<int,int> dup_pair;
  switch((*wrap_iter).getType()){
  case ENZYME:{
    original = (*(*wrap_iter).get_enz_it()).getNumber();
    wrap = Gene_iter_wrapper(Dup( (*wrap_iter).get_enz_it(), enzyme_list));
    *wrap_iter = wrap;
    copy = (*(*wrap_iter).get_enz_it()).getNumber();
    dup_pair = make_pair(original,copy);
    enzymedups++;
    break;

  }
  case TrF:{
    original = (*(*wrap_iter).get_tf_it()).getNumber();
    list<TF>::iterator tf_iter = Dup( (*wrap_iter).get_tf_it(), tf_list);
    wrap = Gene_iter_wrapper(tf_iter);
    (*tf_iter).notify_genes();
    *wrap_iter = wrap;
    copy = (*(*wrap_iter).get_tf_it()).getNumber();
    dup_pair = make_pair(original,copy);
    tfdups++;
    break;
  }  
  case PUMP:{
    original = (*(*wrap_iter).get_pump_it()).getNumber();
    wrap = Gene_iter_wrapper(Dup( (*wrap_iter).get_pump_it(), pump_list));
    *wrap_iter = wrap;
    copy = (*(*wrap_iter).get_pump_it()).getNumber();
    dup_pair = make_pair(original,copy);
    pumpdups++;
    break;
  }
  case NOTYPE:{ 
    cout << "tried to duplicate an invalid gene type" << endl;
    dup_pair =  make_pair(0,0);
    break;
  }
  }
  return dup_pair;
}

template <class T>
typename list<T>::iterator Cell::Dup(const typename list<T>::iterator& it, list<T>& lst){	
  typename list<T>::iterator it2 = lst.insert(it, *it);
  (*it2).notify_tfs();  
  (*it2).setNumber(++Gene::total_nmbr_genes);        
  //increase total nmbr of genes, give this new gene a nmbr
  genes++;    //increase nmbr of genes of this cell;
  dups++;
  return it2;
}


//function not currently in use ( instead a series of Del_helper will be directly called from the mutation function;
void Cell::majorDel( list<Gene_iter_wrapper>::iterator begin,
		     list<Gene_iter_wrapper>::iterator end){
  for(genome_iter = begin; genome_iter !=end;){
    Del_helper(genome_iter); 
    genome_iter = genome.erase(genome_iter);
  }
  majordels++;
}

void Cell::knockout_gene( list<Gene_iter_wrapper>::iterator giwi ){
  Del_helper(giwi);
  genome.erase(giwi);
}
 
void Cell::Del_helper(list<Gene_iter_wrapper>::iterator wrap_iter){
  switch((*wrap_iter).getType()){
  case TrF:{
    list<TF>::iterator tfs_iter =  (*wrap_iter).get_tf_it();
    (*tfs_iter).notify_genes_loss();
    Del(tfs_iter,tf_list);
    tfdels++;
    break;
  }
  case ENZYME: {
    list<Enzyme>::iterator enz_iter =  (*wrap_iter).get_enz_it();
    Del(enz_iter, enzyme_list );
    enzymedels++;
    break;
  }
  case PUMP: {
    list<Pump>::iterator pump_iter = (*wrap_iter).get_pump_it();
    Del( pump_iter, pump_list );
    pumpdels++;
    break;
  }
  case NOTYPE:{ cout << "tried to delete an invalid gene type" << endl;
    break;
  }
  }
}

template <class T>
void Cell::Del(typename list<T>::iterator& it,list<T>& lst){
  (*it).notify_tfs_loss();
  it = lst.erase(it);  
  genes--;
  dels++;
}

void Cell::mutateAgene(double mu_rate){
  if(survived) return;

  int previous_dels = dels;
  double rand = evo_rand.uniform();
  double total_mu = DUP + DEL + POINT;
  if(rand < mu_rate){                                 
  //Do 1 mutation for this cell
    int nmbr_genes = enzyme_list.size() + tf_list.size() + pump_list.size();
    unsigned int this_gene;
   
    if(nmbr_genes <=0) return;

    if(rand <  mu_rate * (DUP / total_mu )){         
    //do a duplication mutation
      rand = evo_rand.uniform();
      this_gene = int(rand * double(nmbr_genes)  +1.);	
      //pick a gene to mutate		
     
      if(this_gene <= tf_list.size()){                  
      //see if the chosen gene is a tf
	tfs_iter = tf_list.begin();
	for(unsigned int i=1;i<this_gene;i++){
	  tfs_iter++;
	}
	Dup(tfs_iter,tf_list);
	(*(--tfs_iter)).notify_genes();				
      }
      else{ 
	this_gene -= tf_list.size();                    
        //else see if the gene is an enzyme
	
	if(this_gene <= enzyme_list.size()){
	  enzymes_iter = enzyme_list.begin();
	  for(unsigned int i = 1; i < this_gene; i++){
	    enzymes_iter++;
	  }
	  Dup(enzymes_iter,enzyme_list);
	}			
	else{
	  this_gene -=enzyme_list.size();
	  pumps_iter = pump_list.begin();
	  for(unsigned int i=1; i< this_gene; i++){
	    pumps_iter++;
	  }
	  Dup(pumps_iter,pump_list);
	}
      }
      //      duplications++;
    }
    else if(rand < mu_rate * ((DUP + DEL)/ total_mu)){ 
         // do a deletion mutation
      rand = evo_rand.uniform();
      this_gene = int(rand * double(nmbr_genes)  +1.);			
      
      if(this_gene <= tf_list.size()){
	tfs_iter = tf_list.begin();
	for(unsigned int i=1;i<this_gene;i++){
	  tfs_iter++;
	}
	(*tfs_iter).notify_genes_loss();
	Del(tfs_iter,tf_list);			
      }
      else{ 
	this_gene -= tf_list.size();
	
	if(this_gene <= enzyme_list.size()){
	  enzymes_iter = enzyme_list.begin();
	  for(unsigned int i = 1; i < this_gene; i++){
	    enzymes_iter++;
	  }
	  Del(enzymes_iter,enzyme_list);
	}
	else{
	  this_gene -=enzyme_list.size();
	  pumps_iter = pump_list.begin();
	  for(unsigned int i=1; i< this_gene; i++){
	    pumps_iter++;
	  }
	  Del(pumps_iter,pump_list);
	}			
      }
      //      deletions++;
    }
    else {                                                 
    //do a pointmutation 
      rand = evo_rand.uniform();
      this_gene = int(rand * double(nmbr_genes) +1.);			
      
      if(this_gene <= tf_list.size()){
	tfs_iter = tf_list.begin();
	for(unsigned int i=1;i<this_gene;i++){
	  tfs_iter++;
	}
	pointMutate(*tfs_iter);			
      }
      else{ 
	this_gene -= tf_list.size();
	
	if(this_gene <= enzyme_list.size()){
	  enzymes_iter = enzyme_list.begin();
	  for(unsigned int i = 1; i < this_gene; i++){
	    enzymes_iter++;
	  }
	  pointMutate(*enzymes_iter);
	}
	else{
	  this_gene -=enzyme_list.size();
	  pumps_iter = pump_list.begin();
	  for(unsigned int i=1; i< this_gene; i++){
	    pumps_iter++;
	  }
	  pointMutate(*pumps_iter);
	}			
      }
      //      pointmutations++;
    }	
  }
  fish_pool();
  if(dels > previous_dels)
    CleanDupPairs();
}


//with with wgd and gross chromosomal rearrangements + single dup/del, based on mutateGenes6 + 5
void Cell::mutateGenes7(double mut_rate,double ins, double dup, double del, 
			double point, double gcr, double sg, double pp, double nothing, double max_gcr){  
//makes use of circular, ordered genome
  if(survived) return; //a cell surviving from the previous generation will not be mutated 

  if(genes <= 0) return;

  int previous_dels = dels;

  double total_mut = ins+dup+del+point;
  double p_ins = mut_rate*(ins/total_mut);
  double p_dup = mut_rate*(dup/total_mut);
  double p_del = mut_rate*(del/total_mut);
  double p_point = mut_rate*(point/total_mut);
  
  double all_mut_scales = gcr+sg+pp+nothing;
  double p_gcr = gcr/all_mut_scales;
  double p_sg = sg/all_mut_scales;
  double p_pp = pp/all_mut_scales;
  double p_nothing = nothing/all_mut_scales;

  double p_dup_sg = p_dup / (p_gcr + p_sg) ; 
  double p_del_sg = p_del / (p_gcr + p_sg) ; 
  
  double p_ins_gcr = (p_ins/p_gcr) / (0.5*max_gcr);
  double p_dup_gcr = p_dup_sg/ (0.5*max_gcr) ;
  double p_del_gcr = p_del_sg/ (0.5*max_gcr) ; 

  double scale_rand=evo_rand.uniform();
  enum {_NOTHING,_PP,_GCR,_SG};
  int mut_scale;
  if( scale_rand < p_nothing) mut_scale=_NOTHING;
  else if( scale_rand < p_nothing+p_pp ) mut_scale=_PP;
  else if( scale_rand < p_nothing+p_pp+p_gcr ) mut_scale=_GCR;
  else mut_scale=_SG;


  /* we chose the total per gene rate of dup, del, ins, point
     according to INS, DUP, DEL, POINT; */
  /* the relative rates of mutations on different scales are
     determined by GCR (gross chromosomal rearrangement, SG (single
     gene), POLYP (polyploidization/wgd) and NOTHING (no mutations
     happening */
  /* to ensure that the number of events per gene are matched to the
     their respective rates, we scale the rates given a certain event
     scale (gcr, sg, wgd); as a rule, given that a certain type of
     mutation (point, dup, del,ins) occurs on a certain event scale,
     the rates are the same over the event scale (except in case of
     wgd) */
  /* e.g. duplication of genes occurs in gcr, sg as well as wgd. To
     arrive at the correct rate of gene duplication, we have to scale
     the conditional probabilities (give an event scale), such that
     the probabilities of gene duplication in all scenarios sum up to
     the overall gene duplication rate */
  /* for duplications we get: global_dup_rate_per_gene = P(sg)*dup_rate +
     P(gcr)*4*dup_rate + P(wgd)*1; Now we can calculate dup_rate.  */
  /* By using multiplication factor 4 in case of gcr, we ensure that
     the per gene dup rate is the same for gcr and sg, since in gcr on
     average 1/4 of the genes are affected */
  /* the wgd rate is a joint probability of being in the PP scale of
     events and the overall cell mutation rate (mut_rate) */

  /*  double gcr_scaling = 2.*(1./max_gcr); //the average stretch length for gcr is 0.5* max_gcr.

  double dup_rate =  (mut_rate*dup/total_mut); // - mut_rate*pp/all_mut_scales)/ ((sg+gcr_scaling*gcr)/all_mut_scales) ;
  double del_rate = (mut_rate*del/total_mut);  // /((sg+gcr_scaling*gcr)/all_mut_scales) ;
  double point_rate = (mut_rate*point/total_mut); // /((sg+gcr+pp)/all_mut_scales);
  double ins_rate = (mut_rate*ins/total_mut); // /(gcr_scaling*gcr/all_mut_scales);
  // std::cout << "dup: " << dup_rate << ", del:"<< del_rate << ", point: "<<point_rate << ", ins: "<< ins_rate << std::endl;
  double scale_rand=evo_rand.uniform();
  enum {_NOTHING,_PP,_GCR,_SG};
  int mut_scale;
  if( scale_rand < (nothing)/all_mut_scales) mut_scale=_NOTHING;
  else if( scale_rand < (nothing+pp)/all_mut_scales) mut_scale=_PP;
  else if( scale_rand < (nothing+pp+gcr)/all_mut_scales) mut_scale=_GCR;
  else mut_scale=_SG;
  */



  switch( mut_scale){
  case _NOTHING:
    break; //return; // why return? no point mutations can happen...

  case _PP:{
    if(evo_rand.uniform() < mut_rate){
      bool wgd = true;
      bool wgded = false;
      list<Gene_iter_wrapper> gene_stretch;
      list<Gene_iter_wrapper>::iterator insertion_point;
      int stretch_length;
      while(wgd){ 
	stretch_length = genes;
	if(stretch_length > genes) stretch_length = genes;
	for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
	  if(evo_rand.uniform() < (1./double(genes))){
	    insertion_point = genome_iter;
	    for(int i = 0; i< stretch_length; i++){  
	      if(genome_iter == genome.end()) genome_iter = genome.begin();  
	      // simulate circular genome
	      gene_stretch.push_back(*genome_iter);
	      genome_iter++; 
	    }
	    wgd = false;
	    wgded = true;
	    break;
	  }
	}
      }
      if(wgded){
	majorDup(insertion_point,gene_stretch);
	wgds++;
      }
    }
    break;
  }
  case _GCR:{
    if(evo_rand.uniform() < p_ins_gcr ){ //< ins_rate*gcr_scaling){ 
      //insertions
      bool inserting = false;
      bool inserted = false;
      int n = genes;
      if(n % 2){                            //in case there is an odd number of genes, we flip a 
	//coin to make it even by adding or subtracting
	if(evo_rand.uniform() < 0.5) n++;   //to compensate for the fact that halves are always 
	else n--;                           //rounded down when converting to integer
      }
      int major_ins = int(evo_rand.uniform()*(double(n)*max_gcr)+1.);
      //determine length of copied stretch, half the genome being the maximum
      if(major_ins > 0) inserting = true;
      list<Gene_iter_wrapper> gene_stretch;
      list<Gene_iter_wrapper>::iterator insertion_point;
      while(inserting){ 
	for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
	  if(evo_rand.uniform() < (1./double(genes))){
	    for(int i = 0; i< major_ins; i++){  
	      if(genome_iter == genome.end()) genome_iter = genome.begin();  
	      // simulate circular genome
	      gene_stretch.push_back(*genome_iter);
	      genome_iter = genome.erase(genome_iter);
	    }
	    for(genome_iter = genome.begin(); ; genome_iter++){
	      if(genome_iter == genome.end()) genome_iter = genome.begin();
	      if(evo_rand.uniform() < 1./double(genes)){
		insertion_point = genome_iter;
		break;
	      }
	    }
	    inserting = false;
	    inserted = true;
	    break;
	  }
	}
      }
      if(inserted) Insertion(insertion_point,gene_stretch);
    }
  
    if(evo_rand.uniform() < p_dup_gcr){ // dup_rate*gcr_scaling){
      //duplications
      bool duplicating = false;
      bool duplicated = false;
      int n = genes;
      if(n % 2){                            //in case there is an odd number of genes, we flip a 
	//coin to make it even by adding or subtracting
	if(evo_rand.uniform() < 0.5) n++;   //to compensate for the fact that halves are always 
	else n--;                           //rounded down when converting to integer
      }
      int major_dup = int(evo_rand.uniform()*(double(n)*max_gcr)+1.);  //made change from 0.5 
      if(major_dup > 0) duplicating = true;
      list<Gene_iter_wrapper> gene_stretch;
      list<Gene_iter_wrapper>::iterator insertion_point;
      while(duplicating){ 
	for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
	  if(evo_rand.uniform() < (1./double(genes))){
	    insertion_point = genome_iter;
	    for(int i = 0; i< major_dup; i++){  
	      if(genome_iter == genome.end()) genome_iter = genome.begin();  
	      // simulate circular genome
	      gene_stretch.push_back(*genome_iter);
	      genome_iter++; 
	    }
	    duplicating = false;
	    duplicated = true;
	    break;
	  }
	}
      }
      if(duplicated) majorDup(insertion_point,gene_stretch);
    }
    if(evo_rand.uniform() < p_del_gcr){ //del_rate*gcr_scaling){
      //deletions
      bool deleting = false;
      int n = genes;
      if(n % 2){                    
	if(evo_rand.uniform() < 0.5) n++;
	else n--;                        
      }
      int major_del = int(evo_rand.uniform()*(double(n)*max_gcr)+1.);
      if(major_del > 0){
	deleting = true;
	//bookkeeping of mutation events:
	majordels++;
      }
      while(deleting){
	for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
	  if(evo_rand.uniform() < (1./double(genes))){
	    for(int i = 0; i< major_del; i++){  
	      if(genome_iter == genome.end()) genome_iter = genome.begin();  
	      // simulate circular genome
	      Del_helper(genome_iter);
	      genome_iter = genome.erase(genome_iter);
	    }
	    deleting = false;
	    break;
	  }
	}
      }
    }
    break;
  }
  case _SG:{
  //single gene duplications
    list<Gene_iter_wrapper>::iterator insertion_point;
    for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
      if(evo_rand.uniform() < p_dup_sg ){//(dup_rate)){
	insertion_point = genome_iter;
	// simulate circular genome
	genome_iter++; 
	singleDupInPlace(insertion_point);
      }
    }

    //single gene deletions 
    for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
      if(evo_rand.uniform() < p_del_sg){ //(del_rate)){
	// simulate circular genome
	Del_helper(genome_iter);
	genome_iter = genome.erase(genome_iter);
      }
    }
    break;
  }
  }

  //point mutations
  for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
    if(evo_rand.uniform() < p_point){ //point_rate){
      pointMutate(genome_iter);
    }
  }  
  fish_pool();
  if(dels > previous_dels)
    CleanDupPairs();
}


//with with wgd and gross chromosomal rearrangements, based on mutateGenes3
void Cell::mutateGenes6(double ins, double dup, double del, double point, double wgd){  
//makes use of circular, ordered genome
  if(survived) return; //a cell surviving from the previous generation will not be mutated 

  if(genes <= 0) return;

  int previous_dels = dels;

  if(evo_rand.uniform() < wgd){
    bool wgd = true;
    bool wgded = false;
    list<Gene_iter_wrapper> gene_stretch;
    list<Gene_iter_wrapper>::iterator insertion_point;
    int stretch_length;
    while(wgd){ 
      stretch_length = genes;
      if(stretch_length > genes) stretch_length = genes;
      for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
	if(evo_rand.uniform() < (1./double(genes))){
	  insertion_point = genome_iter;
	  for(int i = 0; i< stretch_length; i++){  
	    if(genome_iter == genome.end()) genome_iter = genome.begin();  
	    // simulate circular genome
	    gene_stretch.push_back(*genome_iter);
	    genome_iter++; 
	  }
	  wgd = false;
	  wgded = true;
	  break;
	}
      }
    }
    if(wgded){
      majorDup(insertion_point,gene_stretch);
      wgds++;
    }
  }

  if(evo_rand.uniform() < ins){ 
  //insertions
    bool inserting = false;
    bool inserted = false;
    int n = genes;
    if(n % 2){                            //in case there is an odd number of genes, we flip a 
                                          //coin to make it even by adding or subtracting
      if(evo_rand.uniform() < 0.5) n++;   //to compensate for the fact that halves are always 
      else n--;                           //rounded down when converting to integer
    }
    int major_ins = int(evo_rand.uniform()*(double(n)/2.)+1.);
    //determine length of copied stretch, half the genome being the maximum
    if(major_ins > 0) inserting = true;
    list<Gene_iter_wrapper> gene_stretch;
    list<Gene_iter_wrapper>::iterator insertion_point;
    while(inserting){ 
      for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
	if(evo_rand.uniform() < (1./double(genes))){
	  for(int i = 0; i< major_ins; i++){  
	    if(genome_iter == genome.end()) genome_iter = genome.begin();  
	    // simulate circular genome
	    gene_stretch.push_back(*genome_iter);
	    genome_iter = genome.erase(genome_iter);
	  }
	  for(genome_iter = genome.begin(); ; genome_iter++){
	    if(genome_iter == genome.end()) genome_iter = genome.begin();
	    if(evo_rand.uniform() < 1./double(genes)){
	      insertion_point = genome_iter;
	      break;
	    }
	  }
	  inserting = false;
	  inserted = true;
	  break;
	}
      }
    }
    if(inserted) Insertion(insertion_point,gene_stretch);
  }
  
  if(evo_rand.uniform() < dup){
  //duplications
    bool duplicating = false;
    bool duplicated = false;
    int n = genes;
    if(n % 2){                            //in case there is an odd number of genes, we flip a 
                                          //coin to make it even by adding or subtracting
      if(evo_rand.uniform() < 0.5) n++;   //to compensate for the fact that halves are always 
      else n--;                           //rounded down when converting to integer
    }
    int major_dup = int(evo_rand.uniform()*(double(n)/2.)+1.);  //made change from 0.5 
    if(major_dup > 0) duplicating = true;
    list<Gene_iter_wrapper> gene_stretch;
    list<Gene_iter_wrapper>::iterator insertion_point;
    while(duplicating){ 
      for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
	if(evo_rand.uniform() < (1./double(genes))){
	  insertion_point = genome_iter;
	  for(int i = 0; i< major_dup; i++){  
	    if(genome_iter == genome.end()) genome_iter = genome.begin();  
	    // simulate circular genome
	    gene_stretch.push_back(*genome_iter);
	    genome_iter++; 
	  }
	  duplicating = false;
	  duplicated = true;
	  break;
	}
      }
    }
    if(duplicated) majorDup(insertion_point,gene_stretch);
  }
  if(evo_rand.uniform() < del){
  //deletions
    bool deleting = false;
    int n = genes;
    if(n % 2){                    
      if(evo_rand.uniform() < 0.5) n++;
      else n--;                        
    }
    int major_del = int(evo_rand.uniform()*(double(n)/2.)+1.);
    if(major_del > 0){
      deleting = true;
      //bookkeeping of mutation events:
      majordels++;
    }
    while(deleting){
      for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
	if(evo_rand.uniform() < (1./double(genes))){
	  for(int i = 0; i< major_del; i++){  
	    if(genome_iter == genome.end()) genome_iter = genome.begin();  
	    // simulate circular genome
	    Del_helper(genome_iter);
	    genome_iter = genome.erase(genome_iter);
	  }
	  deleting = false;
	  break;
	}
      }
    }
  }
  //point mutations
  for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
    if(evo_rand.uniform() < point){
      pointMutate(genome_iter);
    }
  }  
  fish_pool();
  if(dels > previous_dels)
    CleanDupPairs();
}



//mutate on a per gene basis. Do WGD on a per genome basis. (derived
//from mutateGenes(double,double,double) ; there is no order implied
//for single gene duplications
void Cell::mutateGenes5(double dup, double del, double point,double wgd){
  if(survived) return;

  int previous_dels = dels; //number of deletions sustained up till now

  int original;
  int copy;    
  pair<int, int> dup_pair;
  list<pair<int,int> > dup_sequence;

  double rand;

  if(evo_rand.uniform() < wgd){
    bool wgd = true;
    bool wgded = false;
    list<Gene_iter_wrapper> gene_stretch;
    list<Gene_iter_wrapper>::iterator insertion_point;
    int stretch_length;
    while(wgd){ 
      stretch_length = genes;
      if(stretch_length > genes) stretch_length = genes;
      for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
	if(evo_rand.uniform() < (1./double(genes))){
	  insertion_point = genome_iter;
	  for(int i = 0; i< stretch_length; i++){  
	    if(genome_iter == genome.end()) genome_iter = genome.begin();  
	    // simulate circular genome
	    gene_stretch.push_back(*genome_iter);
	    genome_iter++; 
	  }
	  wgd = false;
	  wgded = true;
	  break;
	}
      }
    }
    if(wgded){
      majorDup(insertion_point,gene_stretch);
      wgds++;
    }
  }

  for(tfs_iter=tf_list.begin(); tfs_iter != tf_list.end();){
    if(evo_rand.uniform() < dup){ 
      original = (*tfs_iter).getNumber();
      list<TF>::iterator gene_copy = Dup(tfs_iter,tf_list);
      copy = (*gene_copy).getNumber();
      dup_pair = make_pair(original,copy);
      dup_sequence.push_front(dup_pair);
      (*gene_copy).notify_genes();	
      tfdups++;
    }
    if(evo_rand.uniform() < del){
      (*tfs_iter).notify_genes_loss();
      Del(tfs_iter,tf_list);
      tfdels++;			
      continue ;
    }
    rand = evo_rand.uniform();
    if(rand < point){
      pointMutate(*tfs_iter);
    }
    tfs_iter++;
  }
  for(enzymes_iter = enzyme_list.begin(); enzymes_iter != enzyme_list.end(); ){
    if(evo_rand.uniform() < dup){ 
      original = (*enzymes_iter).getNumber();
      list<Enzyme>::iterator gene_copy = Dup(enzymes_iter,enzyme_list);
      copy = (*gene_copy).getNumber();
      dup_pair = make_pair(original,copy);
      dup_sequence.push_front(dup_pair);
      enzymedups++;
    }
    if(evo_rand.uniform() < del){
      Del(enzymes_iter,enzyme_list);
      enzymedels++;			
      continue;
    }
    rand = evo_rand.uniform();
    if(rand < point){
      pointMutate(*enzymes_iter);
    }
    enzymes_iter++;
  }
  for(pumps_iter=pump_list.begin(); pumps_iter != pump_list.end(); ){
    if(evo_rand.uniform() < dup){ 
      original = (*pumps_iter).getNumber();
      list<Pump>::iterator gene_copy = Dup(pumps_iter,pump_list);
      copy = (*gene_copy).getNumber();
      dup_pair = make_pair(original,copy);
      dup_sequence.push_front(dup_pair);
      pumpdups++;
    }
    if(evo_rand.uniform() < del){
      Del(pumps_iter,pump_list);			
      pumpdels++;
      continue;
    }
    rand = evo_rand.uniform();
    if(rand < point){
      pointMutate(*pumps_iter);
    }
    pumps_iter++;
  }
  
  duplication_pairs.push_front(dup_sequence);

  genome.clear(); // although no genome order is used, we need to update the 
  //genome representation in order to let copy construction succeed. So we
  //clear the genome and push all genes again in the coming for-loops
  
  for(tfs_iter=tf_list.begin(); tfs_iter != tf_list.end();  tfs_iter++){
    genome.push_back(Gene_iter_wrapper(tfs_iter));
  }
  for(enzymes_iter = enzyme_list.begin(); enzymes_iter != enzyme_list.end();enzymes_iter++){
    genome.push_back(Gene_iter_wrapper(enzymes_iter));
  }
  for(pumps_iter=pump_list.begin(); pumps_iter != pump_list.end();pumps_iter++){
    genome.push_back(Gene_iter_wrapper(pumps_iter));
  }

  fish_pool();
  if(dels > previous_dels)
    CleanDupPairs();
}



void Cell::mutateGenes4(double ins, double dup, double del, double point, double wgd,double mut_rate){
//makes use of circular, ordered genome
  if(survived) return; //a cell surviving from the previous generation will not be mutated 

  if(genes <= 0) return; //nothing to mutate here


  int nr_dups, nr_dels, nr_ins,stretch_length;

  double expected_stretch_length = 1./(1-GEO_DIST_LENGTH);
  /* because geometric distributions in boost are implemented as 
     "the number of bernoulli trials with probability p required to get one that fails", 
     then the expected value becomes: 1/(1-p) */
  
/* we use the formula T=-ln(U)/lambda, where U is a random variable from 
   the uniform distribution [0..1), to generate an exponential 
   random variable determining number of events. To get the desired average 
   (such that expected #events*expected_stretch_length == dup* #genes) we 
   use a scaled lambda parameter to generate the distribution 
   ( 1/lambda = expected value) */


    //  double universal_lambda=expected_stretch_length/(double)(genes);
    //  int modifier = 1; //use a modifier to avoid consistent 0 events in small genomes, due to scaling

    //  if(evo_rand.uniform() > 0.5) modifier *= -1;
  double lambda_dup=expected_stretch_length/(dup*double(genes)); //when dup=0. lambda_dup -> inf and nr_dups -> 0
  nr_dups = (int)(-log(evo_rand.uniform())/lambda_dup + 0.5); //changed: from lambda_dup to universal_lambda
  //  if(evo_rand.uniform()< dup) nr_dups+=modifier;  //apply the modifier sparsely

  //  if(evo_rand.uniform() > 0.5) modifier *= -1;
  double lambda_del=expected_stretch_length/(del*double(genes));
  nr_dels = (int)(-log(evo_rand.uniform())/lambda_del + 0.5);
  //  if(evo_rand.uniform()< del) nr_dels+=modifier;

  //  if(evo_rand.uniform() > 0.5) modifier *= -1;
  double lambda_ins=expected_stretch_length/(ins*double(genes));
  nr_ins = (int)(-log(evo_rand.uniform())/lambda_ins + 0.5);
  //  if(evo_rand.uniform()< dup) nr_ins+=modifier;
  

  std::cout << "ins:dups:dels " << nr_ins << ":"<< nr_dups << ":" << nr_dels << std::endl;
  int previous_dels = dels; //total of deletions incurred up till now


  if(evo_rand.uniform() < wgd){
    bool wgd = true;
    bool wgded = false;
    list<Gene_iter_wrapper> gene_stretch;
    list<Gene_iter_wrapper>::iterator insertion_point;
    while(wgd){ 
      stretch_length = genes;
      if(stretch_length > genes) stretch_length = genes;
      for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
	if(evo_rand.uniform() < (1./double(genes))){
	  insertion_point = genome_iter;
	  for(int i = 0; i< stretch_length; i++){  
	    if(genome_iter == genome.end()) genome_iter = genome.begin();  
	    // simulate circular genome
	    gene_stretch.push_back(*genome_iter);
	    genome_iter++; 
	  }
	  wgd = false;
	  wgded = true;
	  break;
	}
      }
    }
    if(wgded){
      majorDup(insertion_point,gene_stretch);
      wgds++;
    }
  }

  int ins_to_do = nr_ins;
  std::cout << "inserting: " ; 
  if(evo_rand.uniform()<mut_rate){
  while(ins_to_do > 0){

    //insertions
    bool inserting = true;
    bool inserted = false;

      //    if(major_ins > 0) inserting = true;
    list<Gene_iter_wrapper> gene_stretch;
    list<Gene_iter_wrapper>::iterator insertion_point;

    stretch_length = evo_rand.geometric1();
    std::cout << stretch_length << ", "; 
    if(stretch_length > genes) stretch_length = genes - 1;

    while(inserting){ 
      for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
	if(evo_rand.uniform() < (1./double(genes))){ //choose the point where gene stretch is extracted
	  for(int i = 0; i< stretch_length; i++){  
	    if(genome_iter == genome.end()) genome_iter = genome.begin();  
	    // simulate circular genome
	    gene_stretch.push_back(*genome_iter);
	    genome_iter = genome.erase(genome_iter);
	  }
	  for(genome_iter = genome.begin(); ; genome_iter++){
	    if(genome_iter == genome.end()) genome_iter = genome.begin();
	    if(evo_rand.uniform() < 1./double(genes)){  //choose insertion point
	      insertion_point = genome_iter;
	      break;
	    }
	  }
	  inserting = false;
	  inserted = true;
	  break;
	}
      }
    }
    if(inserted) Insertion(insertion_point,gene_stretch); // perform actual insertion
    ins_to_do--;
    
  }
  }
  std::cout << std::endl << "duplicating: ";
  int dups_to_do = nr_dups;
 
  if(evo_rand.uniform()<mut_rate){
  while(dups_to_do > 0){
  //duplications
    bool duplicating = true;
    bool duplicated = false;

    list<Gene_iter_wrapper> gene_stretch;
    list<Gene_iter_wrapper>::iterator insertion_point;

    stretch_length = evo_rand.geometric1();
    std::cout << stretch_length << ", ";
    if(stretch_length > genes) stretch_length = genes;

    while(duplicating){ 
      for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
	if(evo_rand.uniform() < (1./double(genes))){
	  insertion_point = genome_iter;
	  for(int i = 0; i< stretch_length; i++){  
	    if(genome_iter == genome.end()) genome_iter = genome.begin();  
	    // simulate circular genome
	    gene_stretch.push_back(*genome_iter);
	    genome_iter++; 
	  }
	  duplicating = false;
	  duplicated = true;
	  break;
	}
      }
    }
    if(duplicated) majorDup(insertion_point,gene_stretch);
    dups_to_do--;
    
  }
  }
  std::cout << std::endl << "deleting: ";
  int dels_to_do = nr_dels;

  if(evo_rand.uniform()<mut_rate){
  while(dels_to_do > 0){
  //deletions
    bool deleting = false;
    deleting = true;
    
    majordels++;   //bookkeeping of mutation events:
    
    stretch_length = evo_rand.geometric1();
    std::cout << stretch_length << ", ";
    if(stretch_length > genes) stretch_length = genes;
     
    while(deleting){
      for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
	if(evo_rand.uniform() < (1./double(genes))){
	  for(int i = 0; i< stretch_length; i++){  
	    if(genome_iter == genome.end()) genome_iter = genome.begin();  
	    // simulate circular genome
	    Del_helper(genome_iter);
	    genome_iter = genome.erase(genome_iter);
	  }
	  deleting = false;
	  break;
	}
      }
    }
    if(genes <= 0) break;
    dels_to_do--;
   
  }
  }
  std::cout << std::endl;
  //point mutations
  for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
    if(evo_rand.uniform() < point){
      pointMutate(genome_iter);
    }
  }  
  fish_pool();
  if(dels > previous_dels)
    CleanDupPairs(); 
}

void Cell::mutateGenes3(double ins, double dup, double del, double point){  
//makes use of circular, ordered genome
  if(survived) return; //a cell surviving from the previous generation will not be mutated 

  if(genes <= 0) return;

  int previous_dels = dels;
  if(evo_rand.uniform() < ins){ 
  //insertions
    bool inserting = false;
    bool inserted = false;
    int n = genes;
    if(n % 2){                            //in case there is an odd number of genes, we flip a 
                                          //coin to make it even by adding or subtracting
      if(evo_rand.uniform() < 0.5) n++;   //to compensate for the fact that halves are always 
      else n--;                           //rounded down when converting to integer
    }
    int major_ins = int(evo_rand.uniform()*(double(n)/2.)+1.);
    //determine length of copied stretch, half the genome being the maximum
    if(major_ins > 0) inserting = true;
    list<Gene_iter_wrapper> gene_stretch;
    list<Gene_iter_wrapper>::iterator insertion_point;
    while(inserting){ 
      for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
	if(evo_rand.uniform() < (1./double(genes))){
	  for(int i = 0; i< major_ins; i++){  
	    if(genome_iter == genome.end()) genome_iter = genome.begin();  
	    // simulate circular genome
	    gene_stretch.push_back(*genome_iter);
	    genome_iter = genome.erase(genome_iter);
	  }
	  for(genome_iter = genome.begin(); ; genome_iter++){
	    if(genome_iter == genome.end()) genome_iter = genome.begin();
	    if(evo_rand.uniform() < 1./double(genes)){
	      insertion_point = genome_iter;
	      break;
	    }
	  }
	  inserting = false;
	  inserted = true;
	  break;
	}
      }
    }
    if(inserted) Insertion(insertion_point,gene_stretch);
  }
  
  if(evo_rand.uniform() < dup){
  //duplications
    bool duplicating = false;
    bool duplicated = false;
    int n = genes;
    if(n % 2){                            //in case there is an odd number of genes, we flip a 
                                          //coin to make it even by adding or subtracting
      if(evo_rand.uniform() < 0.5) n++;   //to compensate for the fact that halves are always 
      else n--;                           //rounded down when converting to integer
    }
    int major_dup = int(evo_rand.uniform()*(double(n)/2.)+1.);  //made change from 0.5 
    if(major_dup > 0) duplicating = true;
    list<Gene_iter_wrapper> gene_stretch;
    list<Gene_iter_wrapper>::iterator insertion_point;
    while(duplicating){ 
      for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
	if(evo_rand.uniform() < (1./double(genes))){
	  insertion_point = genome_iter;
	  for(int i = 0; i< major_dup; i++){  
	    if(genome_iter == genome.end()) genome_iter = genome.begin();  
	    // simulate circular genome
	    gene_stretch.push_back(*genome_iter);
	    genome_iter++; 
	  }
	  duplicating = false;
	  duplicated = true;
	  break;
	}
      }
    }
    if(duplicated) majorDup(insertion_point,gene_stretch);
  }
  if(evo_rand.uniform() < del){
  //deletions
    bool deleting = false;
    int n = genes;
    if(n % 2){                    
      if(evo_rand.uniform() < 0.5) n++;
      else n--;                        
    }
    int major_del = int(evo_rand.uniform()*(double(n)/2.)+1.);
    if(major_del > 0){
      deleting = true;
      //bookkeeping of mutation events:
      majordels++;
    }
    while(deleting){
      for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
	if(evo_rand.uniform() < (1./double(genes))){
	  for(int i = 0; i< major_del; i++){  
	    if(genome_iter == genome.end()) genome_iter = genome.begin();  
	    // simulate circular genome
	    Del_helper(genome_iter);
	    genome_iter = genome.erase(genome_iter);
	  }
	  deleting = false;
	  break;
	}
      }
    }
  }
  //point mutations
  for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
    if(evo_rand.uniform() < point){
      pointMutate(genome_iter);
    }
  }  
  fish_pool();
  if(dels > previous_dels)
    CleanDupPairs();
}

//we define major mutations, but applied to an unordered genome
void Cell::mutateGenes2(double dup, double del, double point){  

  if(survived) return;

  if(genes <= 0) return;

  int previous_dels = dels;  
  if(evo_rand.uniform() < dup){
  //duplications
    bool duplicated = false;
    int n = genes;
    if(n % 2){                            //in case there is an odd number of genes, we flip a 
                                          //coin to make it even by adding or subtracting
      if(evo_rand.uniform() < 0.5) n++;   //to compensate for the fact that halves are always 
      else n--;                           //rounded down when converting to integer
    }
    int major_dup = int(evo_rand.uniform()*(double(n)/2.)+1.);  //made change from 0.5 
    if(major_dup > 0) duplicated = true;
    int dups_done = 0;

    list<Gene_iter_wrapper> gene_stretch;
    list<Gene_iter_wrapper>::iterator insertion_point;

    genome_iter = genome.begin();
    while(dups_done < major_dup){
      genome_iter++; 
      if(genome_iter == genome.end()) genome_iter = genome.begin();  
      // simulate circular genome
      if(evo_rand.uniform() < 0.2 && ! genome_iter -> is_duped()){
	gene_stretch.push_back(*genome_iter);
	dups_done++;
      }
    }
    if(duplicated) majorDup(genome.begin(),gene_stretch);
  }
  
  if(evo_rand.uniform() < del){
  //deletions
    int n = genes;
    if(n % 2){                    
      if(evo_rand.uniform() < 0.5) n++;
      else n--;                        
    }
    int major_del = int(evo_rand.uniform()*(double(n)/2.)+1.);
    if(major_del > 0){
      //bookkeeping of mutation events:
      majordels++;
    }

    int dels_done = 0;
    genome_iter = genome.begin();
    while(dels_done < major_del){
      if(genome_iter == genome.end()) genome_iter = genome.begin();  
      // simulate circular genome
      if(evo_rand.uniform() < 0.2 ){
	Del_helper(genome_iter);
	genome_iter = genome.erase(genome_iter);
	dels_done++;
      }
      // when deleting, we set genome_iter to point to next giw ; and else we ++
      else  genome_iter++; 
    }
  }
  //point mutations
  for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
    genome_iter -> reset_duped();
    if(evo_rand.uniform() < point){
      pointMutate(genome_iter);
    }
  }  
  fish_pool();
  if(dels > previous_dels)
    CleanDupPairs();
}

//mutate on a per gene basis
void Cell::mutateGenes(double dup, double del, double point){
  if(survived) return;

  int previous_dels = dels; //number of deletions sustained up till now

  int original;
  int copy;    
  pair<int, int> dup_pair;
  list<pair<int,int> > dup_sequence;

  double rand;
  for(tfs_iter=tf_list.begin(); tfs_iter != tf_list.end();){
    if(evo_rand.uniform() < dup){ 
      original = (*tfs_iter).getNumber();
      list<TF>::iterator gene_copy = Dup(tfs_iter,tf_list);
      copy = (*gene_copy).getNumber();
      dup_pair = make_pair(original,copy);
      dup_sequence.push_front(dup_pair);
      (*gene_copy).notify_genes();	
      tfdups++;
    }
    if(evo_rand.uniform() < del){
      (*tfs_iter).notify_genes_loss();
      Del(tfs_iter,tf_list);
      tfdels++;			
      continue ;
    }
    rand = evo_rand.uniform();
    if(rand < point){
      pointMutate(*tfs_iter);
    }
    tfs_iter++;
  }
  for(enzymes_iter = enzyme_list.begin(); enzymes_iter != enzyme_list.end(); ){
    if(evo_rand.uniform() < dup){ 
      original = (*enzymes_iter).getNumber();
      list<Enzyme>::iterator gene_copy = Dup(enzymes_iter,enzyme_list);
      copy = (*gene_copy).getNumber();
      dup_pair = make_pair(original,copy);
      dup_sequence.push_front(dup_pair);
      enzymedups++;
    }
    if(evo_rand.uniform() < del){
      Del(enzymes_iter,enzyme_list);
      enzymedels++;			
      continue;
    }
    rand = evo_rand.uniform();
    if(rand < point){
      pointMutate(*enzymes_iter);
    }
    enzymes_iter++;
  }
  for(pumps_iter=pump_list.begin(); pumps_iter != pump_list.end(); ){
    if(evo_rand.uniform() < dup){ 
      original = (*pumps_iter).getNumber();
      list<Pump>::iterator gene_copy = Dup(pumps_iter,pump_list);
      copy = (*gene_copy).getNumber();
      dup_pair = make_pair(original,copy);
      dup_sequence.push_front(dup_pair);
      pumpdups++;
    }
    if(evo_rand.uniform() < del){
      Del(pumps_iter,pump_list);			
      pumpdels++;
      continue;
    }
    rand = evo_rand.uniform();
    if(rand < point){
      pointMutate(*pumps_iter);
    }
    pumps_iter++;
  }
  
  duplication_pairs.push_front(dup_sequence);

  genome.clear(); // although no genome order is used, we need to update the 
  //genome representation in order to let copy construction succeed. So we
  //clear the genome and push all genes again in the coming for-loops
  
  for(tfs_iter=tf_list.begin(); tfs_iter != tf_list.end();  tfs_iter++){
    genome.push_back(Gene_iter_wrapper(tfs_iter));
  }
  for(enzymes_iter = enzyme_list.begin(); enzymes_iter != enzyme_list.end();enzymes_iter++){
    genome.push_back(Gene_iter_wrapper(enzymes_iter));
  }
  for(pumps_iter=pump_list.begin(); pumps_iter != pump_list.end();pumps_iter++){
    genome.push_back(Gene_iter_wrapper(pumps_iter));
  }

  fish_pool();
  if(dels > previous_dels)
    CleanDupPairs();
}


void Cell::tfAttributes(){
  for(tfs_iter=tf_list.begin(); tfs_iter != tf_list.end(); tfs_iter++)
    {
      (*tfs_iter).setAttributes();
    }
}

void Cell::colourGenesStrength(){
  double hue;
  double sat;
  double bright = 1.0;
  for(tfs_iter=tf_list.begin(); tfs_iter != tf_list.end(); tfs_iter++){
    sat = (*tfs_iter).getPromStr()/10.;
    hue = 0.167; //yellow
    if( (*tfs_iter).getLigand()== A ) hue = 0.667; //blue
    (*tfs_iter).setColour(hue,sat,bright);
  }
  for(enzymes_iter=enzyme_list.begin(); enzymes_iter != enzyme_list.end(); enzymes_iter++){		
    sat = (*enzymes_iter).getPromStr() *(*enzymes_iter).getVmax() / 100. ;
    hue = 0.0;  //red
    if( (*enzymes_iter).isAnabolic() ) hue = 0.333 ; // green
    (*enzymes_iter).setColour(hue,sat,bright);
  }
  for(pumps_iter=pump_list.begin(); pumps_iter != pump_list.end(); pumps_iter++){
    sat = (*pumps_iter).getPromStr()*(*pumps_iter).getVmax() / 100. ;
    hue = 0.833 ; // purple
    (*pumps_iter).setColour(hue,sat,bright);
  }
}

void Cell::colourGenesFitness(){
  double hue = 0.0;  //red -> knocking out this gene has a negative effect on fitness;
  double sat = 1.;
  double bright = 1.0;
  for(tfs_iter=tf_list.begin(); tfs_iter != tf_list.end(); tfs_iter++){
    hue = 0.0;
    if( (*tfs_iter).getFitnessContribution() < 0) hue = 0.333; //green
    sat = pow( abs((*tfs_iter).getFitnessContribution()),1);
    (*tfs_iter).setColour(hue,sat,bright);
  }
  for(enzymes_iter=enzyme_list.begin(); enzymes_iter != enzyme_list.end(); enzymes_iter++){		
    hue = 0.0;
    if( (*enzymes_iter).getFitnessContribution() < 0) hue = 0.333;
    sat = pow( abs((*enzymes_iter).getFitnessContribution()),1);
    (*enzymes_iter).setColour(hue,sat,bright);
  }
  for(pumps_iter=pump_list.begin(); pumps_iter != pump_list.end(); pumps_iter++){
    hue = 0.0;
    if( (*pumps_iter).getFitnessContribution() < 0) hue = 0.333;
    sat = pow ( abs((*pumps_iter).getFitnessContribution()) ,1);
    (*pumps_iter).setColour(hue,sat,bright);
  }
}

void Cell::Graph(string savedir){
  namedGraph(savedir,graphname);
}

void Cell::namedGraph(string savedir, string filename){
  tfAttributes();
  colourGenesStrength();
  graphScheme(savedir,filename);
}

void Cell::geneContributionGraph(string savedir){
  string pre = "fitness";
  string filename = pre +graphname;
  tfAttributes();
  colourGenesFitness();
  graphScheme(savedir,filename);  
}

void Cell::genomeOrderGraph(string savedir){
  string pre = "genome";
  string savefile = savedir + pre + graphname;
  if(  bf::exists(savefile) )
    bf::remove(savefile);
  grout.open(savefile.c_str(),ios_base::out | ios_base::app);
  colourGenesFitness();

  grout << "graph VirtualCell {" << endl;
  grout << "node [fontsize = 10, heigth=.1, width=.3];" << endl;
  for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){ 
    grout << (*genome_iter).get_gene_number() << "[ label = \""<< (*genome_iter).getType() 
	  << "\\n" << (*genome_iter).get_gene_number()            //temp addition
	  << " \", color = \"" << (*(*genome_iter).getGene()).getHue() <<"," 
	  << (*(*genome_iter).getGene()).getSat() << "," << (*(*genome_iter).getGene()).getBright() 
	  << "\", style = filled ]" <<  endl ;		
  }

  for(genome_iter = genome.begin();genome_iter != genome.end() ;){
    grout << (*genome_iter).get_gene_number() << "--" ;
    genome_iter++;
    if(genome_iter == genome.end()) break;
    grout << (*genome_iter).get_gene_number() << "[style=bold,weight=3]" << endl;
  }
  if(genome.begin() != genome.end() ){
    grout << (*(genome.begin())).get_gene_number() << "[style=bold,weight=3]"<< endl;
  }
  grout << "}"<< endl;
  grout.close();
}

//colouring of genes and setting attribute is to be done separately
void Cell::graphScheme(string savedir, string filename){
  string savefile = savedir + filename;
  if(  bf::exists(savefile) )
    bf::remove(savefile);
  grout.open(savefile.c_str(),ios_base::out | ios_base::app);
  grout << "digraph VirtualCell {"<< endl;
  grout << "ranksep = .4; nodesep = .12; ratio = auto;" << endl;
  grout << "node [fontsize = 10,heigth=.1,width=.3];" << endl;
  int cluster = 0;
  for (tfs_iter=tf_list.begin(); tfs_iter != tf_list.end(); tfs_iter++)
    {
      grout << "subgraph cluster" << cluster << "{" << endl;
      grout << "style=invis" << endl;
      grout << (*tfs_iter).getNumber() << "[ label = \"" << (*tfs_iter).getAttribute1() << (*tfs_iter).getType() 
	    << (*tfs_iter).getLigand() << (*tfs_iter).getAttribute2() 
	    << "\\n" << (*tfs_iter).getNumber()     //temp addition
	    << " \", color = \"" << (*tfs_iter).getHue() << "," << (*tfs_iter).getSat() << "," << (*tfs_iter).getBright() 
	    << "\", style = filled ]"<< endl ;
      GeneCollection & col = (*tfs_iter).get_bind_list(); 
      for(unsigned int i = 0;i<col.getList().size();i++){
	GeneList * temp = col.getList()[i];
	for(unsigned int j = 0;j<temp -> getList().size();j++){
	  Gene * g = temp ->  getList()[j];
	  if(g != NULL) {
	    grout << (*tfs_iter).getNumber() << "->" << g -> getNumber() 
		  << "[style=bold,weight=3]" <<  endl;
	  }
	}		
      }
      grout << "}" << endl;
      //      cluster++;
    }  
  for(enzymes_iter=enzyme_list.begin(); enzymes_iter != enzyme_list.end(); enzymes_iter++){
    string function = "(C)";
    if( (*enzymes_iter).isAnabolic() ) function = "(A)";		
    grout << (*enzymes_iter).getNumber() << "[ label = \""<< (*enzymes_iter).getType() << function 
	  << "\\n" << (*enzymes_iter).getNumber()            //temp addition
	  << " \", color = \"" << (*enzymes_iter).getHue() <<"," << (*enzymes_iter).getSat() << "," << (*enzymes_iter).getBright() 
	  << "\", style = filled ]"<< endl ;		
  }
  for(pumps_iter=pump_list.begin(); pumps_iter != pump_list.end(); pumps_iter++){
   grout << (*pumps_iter).getNumber() << "[ label = \""<< (*pumps_iter).getType() 
	 << "\\n" << (*pumps_iter).getNumber()              //temp addition
	  << " \", color = \"" <<(*pumps_iter).getHue() <<"," << (*pumps_iter).getSat() << "," << (*pumps_iter).getBright() 
	  << "\", style = filled ]"<< endl ;		
  }
  list<list<pair<int,int> > >::iterator it = duplication_pairs.begin();
  double hue = 0.555;
  double sat = 1.0;
  double bright = 1.0;
  for(int i = 1; it!= duplication_pairs.end() ; i++){ 
  // iterate through duplication stretches
    list<pair<int,int> >::iterator it2;
    for(it2 = (*it).begin(); it2 != (*it).end();){  
    //iterate through duplication pairs
      bool _delete = true;
      for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){ 
      //iterate through current genome
	if( ((*genome_iter).getGene()) -> getNumber() ==  (*it2).first ) { 
	// first gene of pair is present in genome
	  list<Gene_iter_wrapper>::iterator g_iter_temp;
	  for(g_iter_temp = genome.begin(); g_iter_temp != genome.end(); g_iter_temp++){ 
          //iterate through genome	    
	    if( ((*g_iter_temp).getGene()) -> getNumber() ==  (*it2).second ) {  
            //second gene of pair is present in genome
	      grout << (*it2).first << "->" << (*it2).second << "[weight=0.01, color = \"" 
		    << hue <<"," << sat << "," << bright << "\"]" << endl;
	      _delete = false;
	      break;
	    }
	  }
	  break;
	}	
      }
      if(_delete) it2 = (*it).erase(it2);
      else it2++;
    }
    sat *= 0.75;
    hue += i*0.1;
    bright -= i*0.005;
    it++;
  }		
  grout << "}"<< endl;
  grout.close();
}


void Cell::CleanDupPairs(){
  list<list<pair<int,int> > >::iterator it; 
  for(it = duplication_pairs.begin(); it!= duplication_pairs.end() ;){ 
  // iterate through duplication stretches
    list<pair<int,int> >::iterator it2;
    if(it->size() == 0){
      it = duplication_pairs.erase(it);
      continue;
    }
    for(it2 = (*it).begin(); it2 != (*it).end();){  
    //iterate through duplication pairs
      bool _delete = true;
      for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){ 
      //iterate through current genome
	if( ((*genome_iter).getGene()) -> getNumber() ==  (*it2).first ) { 
	// first gene of pair is present in genome
	  list<Gene_iter_wrapper>::iterator g_iter_temp;
	  for(g_iter_temp = genome.begin(); g_iter_temp != genome.end(); g_iter_temp++){ 
          //iterate through genome	    
	    if( ((*g_iter_temp).getGene()) -> getNumber() ==  (*it2).second ) {  
            //second gene of pair is present in genome
	      _delete = false;
	      break;
	    }
	  }
	  break;
	}	
      }
      if(_delete) it2 = (*it).erase(it2);
      else it2++;
    }
    it++;
  }		
}

string Cell::ConcPoint(){
  ostringstream formatter;
  formatter << setw(13) << A_in << setw(13) << X_in << setw(13) << Anabolite ;
  formatter << setw(13) << update;
  for (enzymes_iter=enzyme_list.begin(); enzymes_iter != enzyme_list.end(); enzymes_iter++)
    {
      formatter << setw(13) << (*enzymes_iter).getConcentration() ;
    }
  for (tfs_iter=tf_list.begin(); tfs_iter != tf_list.end(); tfs_iter++)
    {
      formatter << setw(13)<< (*tfs_iter).getConcentration() ;
    }
  for (pumps_iter=pump_list.begin(); pumps_iter != pump_list.end(); pumps_iter++)
    {
      formatter << setw(13)<< (*pumps_iter).getConcentration()  ;
    }
  return formatter.str();
}

void Cell::printConc(string savedir, double _time){
  ostringstream formatter;
  formatter << "conc" << number << ".dat";
  string savefile = savedir + formatter.str();
  if(bf::exists(savefile) &&  bf::last_write_time(savefile)  < open_time )
    bf::remove(savefile);
  fout.open(savefile.c_str(),ios_base::out | ios_base::app);
  fout << setw(13) << _time ;
  fout << setw(13) << A_in << setw(13) << X_in << setw(13) << Anabolite ;
  for (enzymes_iter=enzyme_list.begin(); enzymes_iter != enzyme_list.end(); enzymes_iter++)
    {
      fout << setw(13) << (*enzymes_iter).getConcentration() ;
    }
  for (tfs_iter=tf_list.begin(); tfs_iter != tf_list.end(); tfs_iter++)
    {
      fout << setw(13)<< (*tfs_iter).getConcentration() ;
    }
  for (pumps_iter=pump_list.begin(); pumps_iter != pump_list.end(); pumps_iter++)
    {
      fout << setw(13)<< (*pumps_iter).getConcentration()  ;
    }
  fout << setw(13)<< update;
  fout << endl;
  fout.close();
}

string Cell::RatePoint(){
  ostringstream formatter;
  formatter << setw(13) << pump_rate 
	    << setw(13) << anabolic_rate
	    << setw(13) << catabolic_rate
	    << setw(13) << leak_rate
	    << setw(13) << update;
  return formatter.str();
}

void Cell::printRates(string savedir, double time){
  ostringstream formatter;
  formatter  << "rates" << number << ".dat";
  string savefile = savedir + formatter.str();
  if(bf::exists(savefile) &&  bf::last_write_time(savefile)  < open_time )
    bf::remove(savefile);
  fout.open(savefile.c_str(),ios_base::out | ios_base::app);
  fout << setw(13) << time << setw(13) << pump_rate 
       << setw(13) << anabolic_rate << setw(13) << catabolic_rate 
       << setw(13) << leak_rate << std::endl;
  fout.close();
}

void Cell::reset_concentration(){
  update = true;
  A_in = startAin;
  X_in = startXin;
  Anabolite = 0;
  for (enzymes_iter=enzyme_list.begin(); enzymes_iter != enzyme_list.end(); enzymes_iter++)
    {
      (*enzymes_iter).resetConcentration();
    }
  for (tfs_iter=tf_list.begin(); tfs_iter != tf_list.end(); tfs_iter++)
    {
      (*tfs_iter).resetConcentration();
    }
  for (pumps_iter=pump_list.begin(); pumps_iter != pump_list.end(); pumps_iter++)
    {
      (*pumps_iter).resetConcentration();
    }
}

void Cell::store_fitness(int i){
  if(update) fitness[i] = 0.; // cell hasn't reached equilibrium at end of run
  else if(NO_TARGET_CONC) fitness[i]=1.;
  else{
    double deltaA = (fabs(A_in - TARGETA) + TARGETA )/TARGETA;
    double deltaX = (fabs(X_in - TARGETX) + TARGETX )/TARGETX;
    fitness[i] =  1./(deltaA*deltaX);
  }
}

double Cell::splitFitness(int i){
  double f = fitness[i];
  f = f*f*f;
  if(RESTRICT_SIZE && get_nmbr_genes() > SIZE_MAX){
    f = penalize(f) ;
  }
  return f;
}

double Cell::FitnessAssay(){   //doesn't store the fitness
  double f  = fitness[0];
    for(unsigned int i = 1;i< fitness.size(); i++){  
      f *=fitness[i];
    }
    if(RESTRICT_SIZE && get_nmbr_genes() > SIZE_MAX){
      f = penalize(f) ;
    }
    // }
  return f;
}

double Cell::penalize(double raw){
  std::cout << "size above" << SIZE_MAX << "penalized" << std::endl;
  return raw*pow((1.-SIZE_PENALTY),max(0,get_nmbr_genes() - SIZE_MAX));
}

void Cell::store_scaled_fitness(double f){
  _fitness = f;
}

void Cell::store_production(int i){
  if(genes > 0 && Anabolite > 0.){
    production[i] = exp(-15*(genes+350)/Anabolite);
    anabolite[i] = Anabolite;
  }
  else{
    production[i] = 0.;
    anabolite[i] = 0.;
  }
}

double Cell::ReproductionAssay(){
  double p = production[0];
  for(unsigned int i = 1; i< production.size(); i++){
    p *= production[i];
  }
  //  _production = p;
  return p;
}

void Cell::store_scaled_production(double p){
  _production = p;
}

// return a list of gene id of genes present in this cell, but not in the argument cell
list<Gene_iter_wrapper> Cell::compare_genomes(Cell * c){
  bool found = false;
  list<Gene_iter_wrapper> absentees = list<Gene_iter_wrapper>();
  list<Gene_iter_wrapper> c_genome = c -> genome;
  list<Gene_iter_wrapper>::iterator c_genome_it = c -> genome_iter;
  for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
    int gene_id = (*genome_iter).get_gene_number();

    for(c_genome_it = c_genome.begin(); c_genome_it != c_genome.end(); c_genome_it++){
      int c_gene_id = (*c_genome_it).get_gene_number();
      if(gene_id == c_gene_id){
	found = true;
	break;
      }
    }
    if(!found) absentees.push_front(*genome_iter);
    else found = false;
  }
  return absentees;
}

void Cell::reconstruct_network(){
  for(tfs_iter = tf_list.begin(); tfs_iter != tf_list.end(); tfs_iter++){
    pool.addGene(&*tfs_iter); 
  }
  for(enzymes_iter = enzyme_list.begin(); enzymes_iter != enzyme_list.end(); enzymes_iter++){
    pool.addGene(&*enzymes_iter);
  }
  for(pumps_iter = pump_list.begin(); pumps_iter != pump_list.end(); pumps_iter++){
    pool.addGene(&*pumps_iter); 
  }
  fish_pool();
}

void Cell::order_genes(){
// after serialization, iterators to genes in tf_list, pump_list & enzyme_list are lost
// but with the unique gene numbers we can restore 'genome'order
  for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
    int gene_id = (*genome_iter).get_gene_number();
    bool found = false;
    for(tfs_iter = tf_list.begin(); tfs_iter != tf_list.end(); tfs_iter++){
      if( (*tfs_iter).getNumber() == gene_id ){
	*genome_iter = Gene_iter_wrapper(tfs_iter);
	found = true;
	break;
      }
    }
    if(found) continue;
    for(enzymes_iter = enzyme_list.begin(); enzymes_iter != enzyme_list.end(); enzymes_iter++){
      if( (*enzymes_iter).getNumber() == gene_id){
	*genome_iter = Gene_iter_wrapper(enzymes_iter);
	found = true;
	break;
      }
    }
    if(found) continue;
    for(pumps_iter = pump_list.begin(); pumps_iter != pump_list.end(); pumps_iter++){
      if( (*pumps_iter).getNumber() == gene_id){
	*genome_iter = Gene_iter_wrapper(pumps_iter);
	found = true;
	break;
      }
    }
    if(found) continue;
    cout << "no corresponding gene found for this gene_iter" << endl;
  }
}

// list< list<Gene *> > Cell::buildGeneTrees(){
//   list< list<Gene *> > gene_tree_list;
//   for( genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
//     gene_tree_list.push_back(buildTreeList(genome_iter ->getGene()) ); 
//   }
//   return gene_tree_list;
// }

//checking consistency between the genome representation and the gene_list representation:
//every item in either of them should have a corresponding item in the other
bool Cell::consistent_genome(){

  bool missing_gene_list = false;
  bool found = true;
  bool double_genes = false;
  for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
    if(!found){
      missing_gene_list = true;
      cout << " found inconsistency " <<  endl;
    }
    found = false;
    switch((*genome_iter).getType()){  
    case TrF:{  
      for(tfs_iter = tf_list.begin(); tfs_iter != tf_list.end(); tfs_iter++){
	if( tfs_iter -> double_check_pointers() ) double_genes = true;
	if( tfs_iter == (*genome_iter).get_tf_it() ){
	  found = true;
	  break;
	}
      }
      break;
    }
    case ENZYME:{
      for(enzymes_iter = enzyme_list.begin(); enzymes_iter != enzyme_list.end(); enzymes_iter++){
	if( enzymes_iter == (*genome_iter).get_enz_it()){
	  found = true;
	  break;
	}
      }
      break;
    }
    case PUMP:{
      for(pumps_iter = pump_list.begin(); pumps_iter != pump_list.end(); pumps_iter++){
	if( pumps_iter == (*genome_iter).get_pump_it()){
	  found = true;
	  break;
	}
      }
      break;
    }
    default:{
      cout << "has no gene type:" << (*genome_iter).get_gene_number() << endl;
      break;
    }
    }
  }

  bool missing_genome = false;
  found = true;
  for(tfs_iter = tf_list.begin(); tfs_iter != tf_list.end(); tfs_iter++){
    if(!found){
      missing_genome = true;
      cout << "found inconsistency type2" << endl;
    }
    found = false;
    for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
      if( (*genome_iter).getType() == TrF && 
	  tfs_iter == (*genome_iter).get_tf_it() ){
	found = true;
	break;
      }
    }
  }
  for(enzymes_iter = enzyme_list.begin(); enzymes_iter != enzyme_list.end(); enzymes_iter++){
    if(!found){
      missing_genome = true;
      cout << "found inconsistency type2" << endl;
    }
    found = false;
    for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
      if( (*genome_iter).getType() == ENZYME &&
	  enzymes_iter == (*genome_iter).get_enz_it() ){
	found = true;
	break;
      }
    }
  }
  for(pumps_iter = pump_list.begin(); pumps_iter != pump_list.end(); pumps_iter++){
    if(!found){
      missing_genome = true;
      cout << "found inconsistency type2" << endl;
    }
    found = false;
    for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
      if( (*genome_iter).getType() == PUMP &&
	  pumps_iter == (*genome_iter).get_pump_it() ){
	found = true;
	break;
      }
    }
  }
  return !( missing_gene_list || missing_genome || double_genes );
}

string Cell::toString(){
  ostringstream formatter;
//   for(genome_iter = genome.begin(); genome_iter != genome.end(); genome_iter++){
//     formatter << *genome_iter << endl;
//   }
 
  for(enzymes_iter = enzyme_list.begin(); enzymes_iter != enzyme_list.end(); enzymes_iter++){
    formatter << *enzymes_iter << endl;
  }
  for(tfs_iter = tf_list.begin(); tfs_iter != tf_list.end(); tfs_iter++){
    formatter << *tfs_iter << endl;
  }
  for(pumps_iter = pump_list.begin(); pumps_iter != pump_list.end(); pumps_iter++){
    formatter << * pumps_iter << endl;
  }
  return formatter.str();
}

void Cell::toFile(string savedir){
  string id = name + ".str"; 
  string savefile = savedir + id;
  if(  bf::exists(savefile) )
    bf::remove(savefile);
  fout.open(savefile.c_str(),ios_base::out | ios_base::app );
  fout << *this;
  fout.close();
}

ostream & operator << (ostream & out,Cell & c){
  out<< c.toString();
  return out;
};

ostream & operator << (ostream & out, Gene_iter_wrapper & wrap){
  out << wrap.toString();
  return out;
}

double Cell::runge(double (*func)(double), double x)
{
  double K1 = (H * (*func)(x));
  double K2 = (H * (*func)(x + 1 / 2 * K1 ));
  double K3 = (H * (*func)(x + 1 / 2 * K2));
  double K4 = (H * (*func)(x + K3));
  x +=  ((K1 + 2 * K2 + 2 * K3 + K4)/6);
  return x;
}
 
