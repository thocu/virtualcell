#include "knockout.hpp"


/* miniTODO
list<Gene_iter_wrapper> Cell:getGenome()
Gene::setContribution(double)
int PetriDish::get_update_steps()

Analyze fitnesses of ancestors? not needed..
Analyze fitnesses of cells

*/
knockout::knockout(string storefile, string reconstructdir,string grace_par,int start_gen, 
		   int end_gen,int _step_size):print(false),resetting(true),
					       start_generation(start_gen),
					       end_generation(end_gen),
					       step_size(_step_size),
					       number_of_mutants(NR_MUTANTS),
					       number_of_closest(NR_CLOSEST),
					       analyzed(false),analyzed_pop(false),
					       analyzed_knockouts(false),
					       _path(reconstructdir){
  dish = new PetriDish(grace_par,0,6);
  dish -> RestoreDish(storefile,reconstructdir+"/",0,0);
  time(&open_time);
  if(PER_GENE_MUTATION == 7){
    double total_mut = INS+DUP+DEL+POINT;
    double all_mut_scales = GCR+SG+POLYP+NOTHING;
    gcr_scaling = 2*(1./MAX_GCR_FRAC);
    major_dup_rate = gcr_scaling* (MUTATION_RATE*DUP/total_mut - MUTATION_RATE*POLYP/all_mut_scales)/ ((SG+gcr_scaling*GCR)/all_mut_scales) ;
    major_del_rate = gcr_scaling* (MUTATION_RATE*DEL/total_mut)/((SG+gcr_scaling*GCR)/all_mut_scales) ;
    major_ins_rate = gcr_scaling* (MUTATION_RATE*INS/total_mut)/(gcr_scaling*GCR/all_mut_scales);
  }

}

knockout::knockout(vector<string> storefiles, string reconstructdir,string grace_par,int start_gen, 
		   int end_gen,int _step_size):print(false),resetting(true),
					       start_generation(start_gen),
					       end_generation(end_gen),
					       step_size(_step_size),
					       number_of_mutants(NR_MUTANTS),
					       number_of_closest(NR_CLOSEST),
					       analyzed(false),analyzed_pop(false),
					       analyzed_knockouts(false),
					       _path(reconstructdir){

  vector<string>::iterator str_it;
  std::cout << storefiles.size() << std::endl;
  for(str_it=storefiles.begin(); str_it!=storefiles.end(); str_it++){
    PetriDish * temp_dish = new PetriDish(grace_par,-1,0);
    std::cout << "dish save: " << *str_it << std::endl;
    temp_dish -> RestoreDish(*str_it,reconstructdir+"/",0,0);
    dishes.push_back(temp_dish);
  }
  dishes.sort(DishSortPredicate);
  std::cout << "nr of dishes: " << dishes.size() << std::endl;
  dish = dishes.back();
  std::cout << "last dish generation: "<< dish->get_generations() << std::endl;
  time(&open_time);

  if(PER_GENE_MUTATION == 7){
    double total_mut = INS+DUP+DEL+POINT;
    double all_mut_scales = GCR+SG+POLYP+NOTHING;
    gcr_scaling = 2*(1./MAX_GCR_FRAC);
    major_dup_rate = gcr_scaling* (MUTATION_RATE*DUP/total_mut - MUTATION_RATE*POLYP/all_mut_scales)/ ((SG+gcr_scaling*GCR)/all_mut_scales) ;
    major_del_rate = gcr_scaling* (MUTATION_RATE*DEL/total_mut)/((SG+gcr_scaling*GCR)/all_mut_scales) ;
    major_ins_rate = gcr_scaling* (MUTATION_RATE*INS/total_mut)/(gcr_scaling*GCR/all_mut_scales);
  }

}



//picks one of the lineages in the ancester trace; this function
//should either be randomized or made 'complete'
void knockout::pick_lineage(){
  lineage.clear();
  list<Cell *> roots = dish -> getRoots();   
  Cell * ancestor = roots.front();
  int time = 1;
  do {
    if(time >= start_generation && time <= end_generation 
       && (time - start_generation) % step_size == 0){
      ancestor -> CleanDupPairs();    // we assume that this hasn't been done yet
      // as is the case in older runs, that didn't implement it in the mutateGenes methods
      lineage.push_back(ancestor);
    }
    list<Cell *> children = ancestor -> getOffspring();
    if(children.begin() == children.end() ) break;
    ancestor = children.front();
    // cout << " new ancestor added" << endl;
    time ++;
    }
    while(ancestor != NULL);
}

//select the full population, at the last generation of a storage point
void knockout::last_generation(){
  population.clear();
  population = dish -> get_cells();
}

//select the full population, at the last generation of a storage point
void knockout::population_snapshots(){
  list<PetriDish *>::iterator dish_it;
  population_stores.clear();
  for(dish_it= dishes.begin(); dish_it!= dishes.end(); dish_it++){
    population_stores.push_back(make_pair((*dish_it) -> get_cells(),(*dish_it)->get_generations()));
  }
  std::cout << "nr of population stores: " << population_stores.size() << std::endl;
}

Cell* knockout::find_individual(unsigned id, Cell* someone,int depth){
  Cell * the_one = NULL;
  if(static_cast<unsigned>(someone -> getnmbr()) == id){
    std::cout << "found The One" << std::endl;
    return someone;
  }
  list<Cell *> children = someone -> getOffspring();
  list<Cell *>::iterator child_iter;
  for(child_iter = children.begin(); child_iter != children.end(); child_iter++){
    the_one = find_individual(id, *child_iter,depth);
    if (the_one!=NULL) break;
  }
  return the_one;
}

//find individual cells by cell id's
void knockout::pick_individuals(vector<unsigned> ids){
  lineage.clear();
  list<Cell *> roots = dish -> getRoots();
  vector<unsigned>::iterator id;
  for(id=ids.begin(); id !=ids.end(); id++){
    Cell * the_one = *roots.begin();
    for(cells_iter = roots.begin(); cells_iter != roots.end(); cells_iter++){
      the_one = find_individual(*id, *cells_iter,0);
      if(the_one != NULL){
	printf("found the one");
	break;
      }
    }
    if(the_one == NULL){
      std::cout << "noone found"; 
    }
    else lineage.push_back(the_one);
  }
}

double knockout ::analyse_cell_in_env(Cell *c,double env,int assay,double time,bool allow_print){
  //int steps = int(LIFETIME/H);
  bool update = true;
  //  bool lower_H;
  A_out = env;
  double _time=0.;
  //  double old_H = H;

  if(resetting || assay == 0)
    c -> reset_concentration();
  else c -> reset_update();
  if(print && allow_print)
    c -> printConc(dish->getSaveDirectory(),time);
  for(; _time < double(LIFETIME);){
    /*   do {
	 lower_H=false;
      c->NewVals();
      if(c->get_error()){
	lower_H = true;
	H=H/STEP_REDUCTION;
	std::cout << "Lowered step size to " << H << " . Recalculating this step." << std::endl;	
      }
      } while(lower_H); */
    update = c -> Update(1);
    _time += H;
    if(print && allow_print && update && int(_time/H) % int(MEASURESTEP/H) == 0.){
      c->printConc(dish->getSaveDirectory(),time+_time);
      c->printRates(dish->getSaveDirectory(),time+_time);
    }
  }
  c->store_fitness(assay);
  c->store_production(assay);
  //  H = old_H;
  return time+_time;
}

// is called by knockout_fitness
void knockout::analyse_cell(Cell * c,bool allow_print){
  double _time = 0.;
  _time = analyse_cell_in_env(c, 0.1, 0, _time,allow_print);
  _time = analyse_cell_in_env(c, 1.,  1, _time,allow_print);
  _time = analyse_cell_in_env(c, 10., 2, _time,allow_print);
  double raw_fitness = c->FitnessAssay();
  c->store_scaled_fitness(dish -> FitnessFunction(raw_fitness));


}

//is called by gene_contributions
double knockout::knockout_fitness(Cell * c,int g_number){
  list<Gene_iter_wrapper>::iterator giwi;
  for(giwi = c->genome.begin(); giwi != c->genome.end(); giwi++){
    int temp = giwi -> get_gene_number();
    if( temp == g_number ){
      c->knockout_gene(giwi);
      // cout << "knocking out gene " << g_number << endl;
      break;
    }
  }
  //run the cell on all environments and assess fitness
  //  if(! c->consistent_genome()) cout << "genome not consistent " << *c << endl;;
  bool allow_print = false;
  analyse_cell(c,allow_print);
  return c->get_fitness();
}

//is called by analyse_lineage
void knockout::gene_contributions(Cell * c){
  list<Gene_iter_wrapper>::iterator genome_iter;
  double fitness = c -> get_fitness();
  for(genome_iter = (c -> genome).begin(); genome_iter != (c ->genome).end(); genome_iter++){
    Gene * g = (genome_iter -> getGene());
    if(fitness > 0.){
      Cell knockout = Cell(*c);
      // knockout.reset_concentration();
      int gene_number = genome_iter -> get_gene_number();
      double k_fitness = knockout_fitness(&knockout,gene_number);
      double contribution = (fitness - k_fitness)/fitness;         //positive contribution means that fitness is negatively
      g ->setContribution(contribution);                            //influenced by knocking out this gene. 
    }
    else g -> setContribution(0.);
  }
}

void knockout::analyse_lineage(){
  int checkgeneration = start_generation;
  bool allow_print = true;
  cout << "analysing_lineage" << std::endl;
  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
    dish -> restoreParamRead(checkgeneration); ////////////////////////////////////////////// change petridish parameters to reflect environment at that time during run.
    analyse_cell(*lin_iter,allow_print);

    checkgeneration += step_size;
  }
  analyzed = true;
}

//analyse a population snapshot, using a "dish save"
void knockout::analyse_population(){
  bool allow_print = true;
  for(pop_iter = population.begin(); pop_iter != population.end(); pop_iter++){
    analyse_cell(*pop_iter,allow_print);
  }
  analyzed_pop = true;
}

//analyse a set of population snapshots from a collection of "dish save" objects
void knockout::analyse_population_snapshots(){
  bool allow_print = true;
  for(pop_stores_it=population_stores.begin(); pop_stores_it!=population_stores.end();pop_stores_it++){
    for(pop_iter=(*pop_stores_it).first.begin(); pop_iter!=(*pop_stores_it).first.end();pop_iter++){
      analyse_cell(*pop_iter,allow_print);
    }
  }
  analyzed_pop=true;
}
// save contribution of every gene in all cells in the lineage
void knockout::knockout_analyse_lineage(){
  int checkgeneration = start_generation;
  if(!analyzed)
    analyse_lineage();
  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
    dish -> restoreParamRead(checkgeneration); ////////////////////////////////////////////// change petridish parameters to reflect environment at that time during run.
    gene_contributions(*lin_iter);
    checkgeneration += step_size;
  }
  analyzed_knockouts = true;
}

double knockout::point_mutant_fitness(Cell * mutant, int gene_nr){
  double mutant_fitness = 0.;
  list<Gene_iter_wrapper>::iterator giwi;
  for(giwi = mutant->genome.begin(); giwi != mutant->genome.end(); giwi++){
    int temp = giwi -> get_gene_number();
    if( temp == gene_nr ){
      mutant -> pointMutate(giwi);
      bool allow_print = false;
      analyse_cell(mutant,allow_print);
      mutant_fitness = mutant -> get_fitness();
      break;
    }
  }
  return mutant_fitness;
}

// determine the average basal expression level of genes in this cell, given a contribution cutoff for genes
double knockout::expression_level(Cell * c, double gene_contribution_cutoff=NEUTRAL_CUT_OFF){
  double average = 0.;
  int gene_count = 0;
  for(list<Gene_iter_wrapper>::iterator giwi = c->genome.begin(); giwi != c->genome.end(); giwi++){
    Gene * g = (giwi -> getGene());
    if( fabs(g -> getFitnessContribution()) > gene_contribution_cutoff ) {
      average += g -> getPromStr();
      gene_count++;
    }
  }
  if(gene_count>0) average=average/double(gene_count);
  else average=0.;
  return average;
}

void knockout::expression_level_evolution(){
  string savefile = dish -> getSaveDirectory() + "expression_levels.dat";
  if(bf::exists(savefile))
    bf::remove(savefile);
  fout.open(savefile.c_str(),ios_base::out | ios_base::app);

  int generation = start_generation;
  if(!analyzed_knockouts) knockout_analyse_lineage();
  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){    
    fout << setw(13) << generation << setw(13) << expression_level(*lin_iter) << std::endl;
    generation+= step_size;
  }
  fout.close();
}


// list format :: { {nr_neutrals, nr_super_neutrals, mutant_average} , 
// {1,gene_id,mutant_fitness, mutant_fitness2 .. mutant_fitnessN} .. {n,gene_id, mutant_fitness} }
list<list<double> > knockout::point_mutant_neutral_genes(Cell * c, int nr_mutants,double cutoff){  
  double avrg_mutant_avrg_fitness = 0.;
  int super_neutral_gene_counter = 0;
  int neutral_gene_counter = 0;
  list<list<double> > avrg_all;
  //  double cutoff = 1./double(BINS); //  NEUTRAL_CUT_OFF;  

  for(list<Gene_iter_wrapper>::iterator giwi = c->genome.begin(); giwi != c->genome.end(); giwi++){
    Gene * g = (giwi -> getGene());
    if( fabs(g -> getFitnessContribution()) < cutoff ) {
      list<double> mutant_fitnesses = list<double>();
      double mutant_avrg_fitness = 0.;
      int gene_number = g -> getNumber();
   
      for(int i = 0 ; i < nr_mutants; i++){
	Cell mutant = Cell(*c);
	double mutant_fitness = point_mutant_fitness(&mutant,gene_number);
	mutant_fitnesses.push_front(mutant_fitness);
	mutant_avrg_fitness += mutant_fitness/double(nr_mutants);
      }

      super_neutral_gene_counter ++;
      neutral_gene_counter ++;
      mutant_fitnesses.push_front(gene_number);
      mutant_fitnesses.push_front(super_neutral_gene_counter);

      avrg_all.push_front(mutant_fitnesses);
      avrg_mutant_avrg_fitness += mutant_avrg_fitness;
    }
    else if( fabs(g -> getFitnessContribution()) < 1./double(BINS) )
      neutral_gene_counter ++;
  }
  if(super_neutral_gene_counter > 0)
    avrg_mutant_avrg_fitness /= double(super_neutral_gene_counter);
  list<double> average;
  average.push_front(avrg_mutant_avrg_fitness);            // pushing the average of all mutants of Cell c 
  average.push_front(super_neutral_gene_counter);
  average.push_front(neutral_gene_counter);
  avrg_all.push_front(average);
  return avrg_all;
}

// analyse mutational robustness of neutral genes, where neutral is having a fitness contribution
// lower than a certain cutoff value
void knockout::neutral_gene_mutant_effect(double cutoff){
  ostringstream formatter;
  formatter << dish -> getSaveDirectory() << "neutral_gene_point_mut_effect_avrg" << cutoff << ".dat" ;
  string savefile  =  formatter.str(); //+ "neutral_gene_effect_avrg.dat";
  formatter.str("");
  formatter << dish -> getSaveDirectory() << "neutral_gene_point_mut_effect_all" << cutoff << ".dat" ;
  string savefile2 = formatter.str(); // dish -> getSaveDirectory() + "neutral_gene_effect_all.dat";

  int generation = start_generation;
  double fitness = 0.;
  if(bf::exists(savefile))
    bf::remove(savefile);
  if(bf::exists(savefile2))
    bf::remove(savefile2);
  if(! analyzed_knockouts) knockout_analyse_lineage();
  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
    dish -> restoreParamRead(generation); ////////////////////////////////////////////// change petridish parameters to reflect environment at that time during run.
    //    if(! analyzed_knockouts) gene_contributions(*lin_iter);
    fitness = (*lin_iter) -> get_fitness();
    if(fitness > 0.){
      list<list<double> > list_list = point_mutant_neutral_genes(*lin_iter,number_of_mutants,cutoff);
      list<list<double> >::iterator list_it = list_list.begin();
      list<double> vals = *list_it;
      list<double> ::iterator val_it = vals.begin();
      fout.open(savefile.c_str(),ios_base::out | ios_base::app);
      fout << setw(13) << generation << setw(13) << *val_it ; // nr_neutrals
      val_it++;
      fout << setw(13) << *val_it;                            // nr_super_neutrals
      val_it++;
      fout << setw(13) << (*val_it)/fitness << endl;         // average of all mutants of this cell
      fout.close();

      fout.open(savefile2.c_str(),ios_base::out | ios_base::app);
      list_it++;
      for(; list_it != list_list.end(); list_it++){             // loop over fitness vals per gene mutated
        dish -> restoreParamRead(generation); ////////////////////////////////////////////// change petridish parameters to reflect environment at that time during
	vals = *list_it;
	val_it = vals.begin();
	fout << setw(13) << generation << setw(13) << *val_it;   // super_neutral_gene_number
	val_it++;
	fout << setw(13) << *val_it;                             // gene_id
	val_it++;
	for(;val_it != vals.end();val_it++){
	  fout << setw(13) << (*val_it)/fitness;                 // mutant_fitness_vals
	}
	fout << endl;
      }
      fout.close();
    }
    generation += step_size;
  }
  //  analyzed = true;
  //  analyzed_knockouts = true;
}

// list format :: { {nr_neutrals, nr_super_neutrals, mutant_average} , 
// {1,gene_id,mutant_fitness, mutant_fitness2 .. mutant_fitnessN} .. {n,gene_id, mutant_fitness} }
list<double> knockout::multiple_mutant(Cell * c, int nr_mutants,double mut_rate){
  
  list<double> avrg_all;
  double mutant_avrg_fitness = 0.;

  for(int i = 0 ; i < nr_mutants; i++){
    Cell mutant = Cell(*c);
    dish -> mutateCell3(&mutant,mut_rate);
    analyse_cell(&mutant,false);
    double mutant_fitness = mutant.get_fitness(); 
    avrg_all.push_front(mutant_fitness);
    mutant_avrg_fitness += mutant_fitness/double(nr_mutants);
  }

  avrg_all.push_front(mutant_avrg_fitness);
  return avrg_all;
}

void knockout::multiple_mutation_robustness(double mut_rate){
  ostringstream formatter;
  formatter << dish -> getSaveDirectory() << "multiple_mutation_robustness" << mut_rate << ".dat" ;
  string savefile  =  formatter.str(); 

  int generation = start_generation;
  double fitness = 0.;
  if(bf::exists(savefile))
    bf::remove(savefile);
  fout.open(savefile.c_str(),ios_base::out | ios_base::app);

  if(! analyzed) analyse_lineage();
  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
    dish -> restoreParamRead(generation); ////////////////////////////////////////////// change petridish parameters to reflect environment at that time during
    fitness = (*lin_iter) -> get_fitness();
    if(fitness > 0.){

      list<double> vals = multiple_mutant(*lin_iter,number_of_mutants,mut_rate);
      list<double> ::iterator val_it = vals.begin();

      fout << setw(13) << generation ;
      for(; val_it != vals.end(); val_it++){           
	fout << setw(13) << *val_it/fitness;  
      }
      fout << endl;
    }
    generation += step_size;
  }
  fout.close();
}

// called by major_mutation_robustness()
// create mutants of a cell by duplication in the genome; return their fitnesses 
vector<double> knockout::dup_mutants_fitness(Cell * c,int nr_mutants){
  double mutant_fitness = 0.;
  vector<double> avrg_all = vector<double>(nr_mutants+1,0.);

  for(int i = 0; i < nr_mutants; i++){
    Cell dup_mutant = Cell(*c);
    dup_mutant.mutateGenes3(0,1.,0,0);
    bool allow_print = false;
    analyse_cell(&dup_mutant,allow_print);
    mutant_fitness = dup_mutant.get_fitness();
    avrg_all[i+1] = mutant_fitness; 
    avrg_all[0] += mutant_fitness/double(nr_mutants);
  }
  return avrg_all;
}

// called by major_mutation_robustness()
// create mutants of a cell by deletion in the genome; return their fitnesses 
vector<double> knockout::del_mutants_fitness(Cell * c, int nr_mutants){
  double mutant_fitness = 0.;
  vector<double> avrg_all = vector<double>(nr_mutants+1,0.);

  for(int i = 0; i < nr_mutants; i++){
    Cell del_mutant = Cell(*c);
    del_mutant.mutateGenes3(0,0,1.,0);
    bool allow_print = false;
    analyse_cell(&del_mutant,allow_print);
    mutant_fitness = del_mutant.get_fitness();
    avrg_all[i+1] = mutant_fitness;
    avrg_all[0] += mutant_fitness/double(nr_mutants);
  }

  return avrg_all;
}

// analyse major mutation robustness of an ancestor relative to a
// subset of the population in existence at the same time and having
// comparable fitness to the ancestor
void knockout::major_mutation_robustness_pop_vs_ancestor(){
  string savefile = dish -> getSaveDirectory() + "major_mutation_robustness.dat";
  if(bf::exists(savefile))
    bf::remove(savefile);
  fout.open(savefile.c_str(),ios_base::out | ios_base::app);
  string savefile2 = dish -> getSaveDirectory() + "neutral_major_mut_count.dat";
  if(bf::exists(savefile2))
    bf::remove(savefile2);
  fout2.open(savefile2.c_str(),ios_base::out | ios_base::app);
  string savefile3 = dish -> getSaveDirectory() + "major_mutation_robustness_pop_snapshots.dat";
  if(bf::exists(savefile3))
    bf::remove(savefile3);
  fout3.open(savefile3.c_str(),ios_base::out | ios_base::app);
  string savefile4 = dish -> getSaveDirectory() + "neutral_major_mut_count_pop_snapshots.dat";
  if(bf::exists(savefile4))
    bf::remove(savefile4);
  fout4.open(savefile4.c_str(),ios_base::out | ios_base::app);
  string savefile5 = dish -> getSaveDirectory() + "raw_major_mutation_robustness.dat";
  if(bf::exists(savefile5))
    bf::remove(savefile5);
  fout5.open(savefile5.c_str(),ios_base::out | ios_base::app);
  string savefile6 = dish -> getSaveDirectory() + "raw_major_mutation_robustness_pop_snapshots.dat";
  if(bf::exists(savefile6))
    bf::remove(savefile6);
  fout6.open(savefile6.c_str(),ios_base::out | ios_base::app);
  string savefile7 = dish -> getSaveDirectory() + "fitnesses_ancestor_pop.dat";
  if(bf::exists(savefile7))
    bf::remove(savefile7);
  fout7.open(savefile7.c_str(),ios_base::out | ios_base::app);

  string savefile8 = dish -> getSaveDirectory() + "major_mutation_robustness_best.dat";
  if(bf::exists(savefile8))
    bf::remove(savefile8);
  fout8.open(savefile8.c_str(),ios_base::out | ios_base::app);
  string savefile9 = dish -> getSaveDirectory() + "neutral_major_mut_count_best.dat";
  if(bf::exists(savefile9))
    bf::remove(savefile9);
  fout9.open(savefile9.c_str(),ios_base::out | ios_base::app);
  string savefile10 = dish -> getSaveDirectory() + "raw_major_mutation_robustness_best.dat";
  if(bf::exists(savefile10))
    bf::remove(savefile10);
  fout10.open(savefile10.c_str(),ios_base::out | ios_base::app);


  lin_iter=lineage.begin();
  int  generation=start_generation;
  list<pair<Cell *,double> > temp_eval_list;
  if(!analyzed) analyse_lineage();
  for(pop_stores_it=population_stores.begin(); pop_stores_it!=population_stores.end();pop_stores_it++){
    temp_eval_list.clear();
    std::cout << "new pop_store" << std::endl;

    //test//
    while(generation < (*pop_stores_it).second){ //
      lin_iter++;
      generation+=step_size;
    }
    double ancestor_fitness=(*lin_iter)->get_fitness();
    double best_fitness=0.;
    Cell * generation_best = NULL;
    //    std::cout << "ancestor fitness: "<< ancestor_fitness << std::endl;
    for(pop_iter=(*pop_stores_it).first.begin();pop_iter!=(*pop_stores_it).first.end();pop_iter++){
      double _fitness=(*pop_iter)->get_fitness();
      if(_fitness > best_fitness){
	best_fitness=_fitness;
	generation_best=(*pop_iter);
      }
      double dist = fabs(_fitness-ancestor_fitness);
      temp_eval_list.push_back(make_pair(*pop_iter,dist));
    }
    temp_eval_list.sort(CellFitnessPairsSortPredicate);
    list<pair<Cell*,double> >::iterator test_iter;
    /*    for(test_iter=temp_eval_list.begin();test_iter!=temp_eval_list.end();test_iter++){
      std::cout << (*test_iter).second << std::endl;
      }*/

    double fitness = 0;

    int neutral_dup_count = 0;
    int neutral_del_count = 0;   
    vector<double> avrg_all_dup_mutant;
    vector<double> avrg_all_del_mutant;
    double avrg_dup_del_mutant_fitness = 0.;
    
    fitness = ancestor_fitness;
    fout7 << setw(13) << generation << setw(13) << fitness;
    if(ancestor_fitness > 0.){

      /*---------------------------- generate data for the ancestor----------------------*/
      // divide all returned mutant fitnesses by ancestor fitness ( to get a relative change in f)
      avrg_all_dup_mutant = dup_mutants_fitness(*lin_iter,number_of_mutants);
      avrg_all_del_mutant = del_mutants_fitness(*lin_iter,number_of_mutants);
      avrg_dup_del_mutant_fitness = (avrg_all_dup_mutant[0]/fitness + avrg_all_del_mutant[0]/fitness) / 2. ;

      fout << setw(13) << generation;

      // << setw(13) << avrg_all_dup_mutant[0]/fitness << setw(13) << avrg_all_del_mutant[0]/fitness 
      //   << setw(13) <<  avrg_dup_del_mutant_fitness ;
      fout5 << setw(13) << generation ;
      for(int i = 1; i <= number_of_mutants ; i++){
	fout << setw(13) << avrg_all_dup_mutant[i]/fitness ; 
	fout5 << setw(13) << avrg_all_dup_mutant[i]; 
	// count dup mutants with (near) neutral fitness effect
	if(avrg_all_dup_mutant[i]/fitness > 0.95 &&  avrg_all_dup_mutant[i]/fitness < 1.05)
	  neutral_dup_count ++;
      }
      for(int i = 1; i <= number_of_mutants ; i++){
	fout << setw(13) << avrg_all_del_mutant[i]/fitness ; 
	fout5 << setw(13) << avrg_all_del_mutant[i];
	// count del mutants with (near) neutral fitness effect
	if(avrg_all_del_mutant[i]/fitness > 0.95 &&  avrg_all_del_mutant[i]/fitness < 1.05)
	  neutral_del_count ++;
      }
      fout << endl;
      fout5 << endl;
    }
    fout2 << std::setw(13) <<  generation << std::setw(13) << neutral_dup_count << std::setw(13) << neutral_del_count << std::endl;

    neutral_dup_count = 0;
    neutral_del_count = 0;   

    if(ancestor_fitness > 0.){
      /*---------------------------- generate data for the best    ----------------------*/
      fitness=best_fitness;
      // divide all returned mutant fitnesses by ancestor fitness ( to get a relative change in f)
      avrg_all_dup_mutant = dup_mutants_fitness(generation_best,number_of_mutants);
      avrg_all_del_mutant = del_mutants_fitness(generation_best,number_of_mutants);
      avrg_dup_del_mutant_fitness = (avrg_all_dup_mutant[0]/fitness + avrg_all_del_mutant[0]/fitness) / 2. ;

      fout8 << setw(13) << generation;

      // << setw(13) << avrg_all_dup_mutant[0]/fitness << setw(13) << avrg_all_del_mutant[0]/fitness 
      //   << setw(13) <<  avrg_dup_del_mutant_fitness ;
      fout9 << setw(13) << generation ;
      for(int i = 1; i <= number_of_mutants ; i++){
	fout8 << setw(13) << avrg_all_dup_mutant[i]/fitness ; 
	fout9 << setw(13) << avrg_all_dup_mutant[i]; 
	// count dup mutants with (near) neutral fitness effect
	if(avrg_all_dup_mutant[i]/fitness > 0.95 &&  avrg_all_dup_mutant[i]/fitness < 1.05)
	  neutral_dup_count ++;
      }
      for(int i = 1; i <= number_of_mutants ; i++){
	fout8 << setw(13) << avrg_all_del_mutant[i]/fitness ; 
	fout9 << setw(13) << avrg_all_del_mutant[i];
	// count del mutants with (near) neutral fitness effect
	if(avrg_all_del_mutant[i]/fitness > 0.95 &&  avrg_all_del_mutant[i]/fitness < 1.05)
	  neutral_del_count ++;
      }
      fout8 << endl;
      fout9 << endl;
    }
    fout10 << std::setw(13) <<  generation << std::setw(13) << neutral_dup_count << std::setw(13) << neutral_del_count << std::endl;

    neutral_dup_count = 0;
    neutral_del_count = 0;   

    list<pair<Cell*,double> >::iterator temp_eval_it;
    int close_counter = 0;
    for(temp_eval_it=temp_eval_list.begin();temp_eval_it!=temp_eval_list.end();temp_eval_it++){
      close_counter++;
      if(close_counter > number_of_closest) break; 
      vector<double> avrg_all_dup_mutant;
      vector<double> avrg_all_del_mutant;
      double avrg_dup_del_mutant_fitness = 0.;
        
      neutral_dup_count = 0;
      neutral_del_count = 0;
      fitness = (*temp_eval_it).first -> get_fitness();
      fout7 << setw(13) << fitness ;
      if(ancestor_fitness > 0.){
	// divide all returned mutant fitnesses by ancestor fitness ( to get a relative change in f)
	avrg_all_dup_mutant = dup_mutants_fitness((*temp_eval_it).first,number_of_mutants/number_of_closest);
	avrg_all_del_mutant = del_mutants_fitness((*temp_eval_it).first,number_of_mutants/number_of_closest);
	avrg_dup_del_mutant_fitness = (avrg_all_dup_mutant[0]/fitness + avrg_all_del_mutant[0]/fitness) / 2. ;
	
	fout3 << setw(13) << generation;
	// << setw(13) << avrg_all_dup_mutant[0]/fitness << setw(13) << avrg_all_del_mutant[0]/fitness 
	//   << setw(13) <<  avrg_dup_del_mutant_fitness ;
	fout6 << setw(13) << generation ;
	for(unsigned int i = 1; i < avrg_all_dup_mutant.size() ; i++){
	  fout3 << setw(13) << avrg_all_dup_mutant[i]/fitness ; 
	  fout6 << setw(13) << avrg_all_dup_mutant[i];
	  // count dup mutants with (near) neutral fitness effect
	  if(avrg_all_dup_mutant[i]/fitness > 0.95 &&  avrg_all_dup_mutant[i]/fitness < 1.05)
	    neutral_dup_count ++;
	}
	for(unsigned int i = 1; i < avrg_all_del_mutant.size() ; i++){
	  fout3 << setw(13) << avrg_all_del_mutant[i]/fitness ; 
	  fout6 << setw(13) << avrg_all_del_mutant[i]; 
	  // count del mutants with (near) neutral fitness effect
	  if(avrg_all_del_mutant[i]/fitness > 0.95 &&  avrg_all_del_mutant[i]/fitness < 1.05)
	    neutral_del_count ++;
	}
	fout3 << endl;
	fout6 << endl;
      }
      //else std::cout << "fitness of this individual <= 0" << std::endl;
      fout4 << setw(13) << generation << std::setw(13) <<  neutral_dup_count << std::setw(13) << neutral_del_count << std::endl;
    }
    fout7 << std::endl;
  }
  fout.close();
  fout2.close();
  fout3.close();
  fout4.close();
  fout5.close();
  fout6.close();
  fout7.close();
  fout8.close();
  fout9.close();
  fout10.close();
  std::cout << "saves in :" << savefile <<", "<< savefile2 <<", "<< savefile3 <<", "<< savefile4 << ", " << savefile5 << ", " << savefile6 << std::endl; 

}

// analyse robustness towards duplication and deletion mutations of all cells in a lineage 
void knockout::major_mutation_robustness(){

  string savefile = dish -> getSaveDirectory() + "major_mutation_robustness.dat";
  if(bf::exists(savefile))
    bf::remove(savefile);
  fout.open(savefile.c_str(),ios_base::out | ios_base::app);
  string savefile2 = dish -> getSaveDirectory() + "neutral_major_mut_count.dat";
  if(bf::exists(savefile2))
    bf::remove(savefile2);
  fout2.open(savefile2.c_str(),ios_base::out | ios_base::app);


  int generation = start_generation;
  double fitness = 0;
  vector<double> avrg_all_dup_mutant;
  vector<double> avrg_all_del_mutant;
  double avrg_dup_del_mutant_fitness = 0.;
  if(!analyzed) analyse_lineage();
  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
    dish -> restoreParamRead(generation); ////////////////////////////////////////////// change petridish parameters to reflect environment at that time during
    int neutral_dup_count = 0;
    int neutral_del_count = 0;
    fitness = (*lin_iter) -> get_fitness();
    if(fitness > 0.){
      // divide all returned mutant fitnesses by ancestor fitness ( to get a relative change in f)
      avrg_all_dup_mutant = dup_mutants_fitness(*lin_iter,number_of_mutants);
      avrg_all_del_mutant = del_mutants_fitness(*lin_iter,number_of_mutants);
      avrg_dup_del_mutant_fitness = (avrg_all_dup_mutant[0]/fitness + avrg_all_del_mutant[0]/fitness) / 2. ;

      fout << setw(13) << generation << setw(13) << avrg_all_dup_mutant[0]/fitness << setw(13) << avrg_all_del_mutant[0]/fitness 
	   << setw(13) <<  avrg_dup_del_mutant_fitness ;

      for(int i = 1; i <= number_of_mutants ; i++){
	fout << setw(13) << avrg_all_dup_mutant[i]/fitness ; 
	// count dup mutants with (near) neutral fitness effect
	if(avrg_all_dup_mutant[i]/fitness > 0.95 &&  avrg_all_dup_mutant[i]/fitness < 1.05)
	  neutral_dup_count ++;
      }
      for(int i = 1; i <= number_of_mutants ; i++){
	fout << setw(13) << avrg_all_del_mutant[i]/fitness ; 
	// count del mutants with (near) neutral fitness effect
	if(avrg_all_del_mutant[i]/fitness > 0.95 &&  avrg_all_del_mutant[i]/fitness < 1.05)
	  neutral_del_count ++;
      }
      fout << endl;
    }
    fout2 << std::setw(13) <<  generation << std::setw(13) << neutral_dup_count << std::setw(13) << neutral_del_count << std::endl;
    generation += step_size;
  }
  fout.close();
  fout2.close();
}

// analyse robustness towards duplication and deletion mutations of all cells in a lineage 
void knockout::major_mutation_robustness_pop(){

  int g = dish -> get_generations();
  string savefile = dish -> getSaveDirectory() + "major_mutation_robustness_pop_g"+boost::lexical_cast<string>(g)+".dat";
  if(bf::exists(savefile))
    bf::remove(savefile);
  fout.open(savefile.c_str(),ios_base::out | ios_base::app);
  string savefile2 = dish -> getSaveDirectory() + "neutral_major_mut_count_pop_g"+boost::lexical_cast<string>(g)+".dat";
  if(bf::exists(savefile2))
    bf::remove(savefile2);
  fout2.open(savefile2.c_str(),ios_base::out | ios_base::app);


  double fitness = 0;
  vector<double> avrg_all_dup_mutant;
  vector<double> avrg_all_del_mutant;
  double avrg_dup_del_mutant_fitness = 0.;
  if(!analyzed_pop) analyse_population();
  for(pop_iter = population.begin(); pop_iter != population.end(); pop_iter++){
    int neutral_dup_count = 0;
    int neutral_del_count = 0;
    fitness = (*pop_iter) -> get_fitness();
    if(fitness > 0.){
      // divide all returned mutant fitnesses by ancestor fitness ( to get a relative change in f)
      avrg_all_dup_mutant = dup_mutants_fitness(*pop_iter,number_of_mutants);
      avrg_all_del_mutant = del_mutants_fitness(*pop_iter,number_of_mutants);
      avrg_dup_del_mutant_fitness = (avrg_all_dup_mutant[0]/fitness + avrg_all_del_mutant[0]/fitness) / 2. ;

      fout << setw(13) << avrg_all_dup_mutant[0]/fitness << setw(13) << avrg_all_del_mutant[0]/fitness 
	   << setw(13) <<  avrg_dup_del_mutant_fitness ;

      for(int i = 1; i <= number_of_mutants ; i++){
	fout << setw(13) << avrg_all_dup_mutant[i]/fitness ; 
	// count dup mutants with (near) neutral fitness effect
	if(avrg_all_dup_mutant[i]/fitness > 0.95 &&  avrg_all_dup_mutant[i]/fitness < 1.05)
	  neutral_dup_count ++;
      }
      for(int i = 1; i <= number_of_mutants ; i++){
	fout << setw(13) << avrg_all_del_mutant[i]/fitness ; 
	// count del mutants with (near) neutral fitness effect
	if(avrg_all_del_mutant[i]/fitness > 0.95 &&  avrg_all_del_mutant[i]/fitness < 1.05)
	  neutral_del_count ++;
      }
      fout << endl;
    }
    //else std::cout << "fitness of this individual <= 0" << std::endl;
    fout2 << std::setw(13) <<  neutral_dup_count << std::setw(13) << neutral_del_count << std::endl;
  }
  fout.close();
  fout2.close();
}


// given that contributions of individual genes ar known from knockout experiments,
// we can bin genes that have a similar contribution. Dividing bin size by the total number of 
// genes in the cells network, we have proportion of genes with certain fitness effect in the network
void knockout::binned_gene_contribution(){

  string savefile = dish -> getSaveDirectory() + "gene_contributions.dat";
  if(bf::exists(savefile))
    bf::remove(savefile);
  fout.open(savefile.c_str(),ios_base::out | ios_base::app);
  int generation = start_generation;
  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
    dish -> restoreParamRead(generation); ////////////////////////////////////////////// change petridish parameters to reflect environment at that time during
    int size = (*lin_iter) -> get_nmbr_genes();
    //    int bins = 5;
    double contribution_bins[BINS];

    double extreme_neutrals = 0.;
    for(int j = 0; j < BINS ; j ++ ){
      contribution_bins[j] = 0.;
    }
    double total_contribution = 0.;
    list<Gene_iter_wrapper>::iterator genome_iter;
    for(genome_iter = ((*lin_iter) -> genome).begin(); genome_iter != ((*lin_iter) ->genome).end(); genome_iter++){
      double contribution = (*(*genome_iter).getGene()).getFitnessContribution();
      total_contribution += contribution;
      if( fabs(contribution) < NEUTRAL_CUT_OFF) extreme_neutrals += 1./double(size);
      for(int i = 1; i <= BINS ; i ++ ){
	if( fabs(contribution) <= (double(i) / double(BINS)) ){ 
	  contribution_bins[i-1] += 1. / double(size) ;
	  break;
	}
      }
    }
    fout << setw(13) << generation;
    for(int i = 0; i < BINS; i ++){
      fout << setw(13) << contribution_bins[i]; 
    }
    fout << setw(13) << total_contribution <<  setw(13) << extreme_neutrals << endl;
    generation += step_size;
  }
  fout.close();
}


// detect deletion of genes, then determine robustness against point mutations and 
// knockout of the genes that were deleted subsequently 
// output format : { {nr_dels,mutant_average}, {1,gene_id, knockout_fitness, mutant_fitness
list<list<double> > knockout::dup_analysis(Cell * child, int nr_mutants){
 
  list<Gene_iter_wrapper> giw_list;
  list<Gene_iter_wrapper>::iterator giwi;
  list<list<double > > avrg_all = list<list<double> >();
  double avrg_mutant_avrg_fitness = 0.;
  int duplication_counter = 0;
  Cell *parent = child -> getAncestor();
  
  if(parent != NULL && parent -> get_dups() < child -> get_dups() ){

    double mutant_avrg_fitness = 0.;
    if(!analyzed){
      bool allow_print = false;
      analyse_cell(child,allow_print);
    }
    double child_fitness = child -> get_fitness();

    giw_list = child -> compare_genomes(parent);    // returns id's of genes present in child but not in parent (duplications)

    for(giwi = giw_list.begin(); giwi != giw_list.end(); giwi++){
      int gene_id = giwi -> get_gene_number();
      list<double> mutant_fitnesses = list<double>();      // to store individual mutant fitnesses
      double k_fitness = 0.;
      double k_contribution;
      if(!analyzed_knockouts){                            // else we already have it's knockout_fitness
	Cell knockout = Cell(*child);                     // create temporary cell for knockout analysis
	k_fitness = knockout_fitness(&knockout,gene_id); 
	k_contribution = (child_fitness - k_fitness)/child_fitness;
      }
      else k_contribution = (giwi -> getGene()) -> getFitnessContribution();
      for(int i = 0; i < nr_mutants; i++){
	Cell mutant = Cell(*child);
	double mutant_fitness = point_mutant_fitness(&mutant,gene_id);

	mutant_fitnesses.push_front(mutant_fitness/child_fitness);
	mutant_avrg_fitness += mutant_fitness/(double(nr_mutants)*child_fitness);	
      }

      duplication_counter ++;
      mutant_fitnesses.push_front(k_contribution);
      mutant_fitnesses.push_front(gene_id);                    // store deleted gene id
      mutant_fitnesses.push_front(duplication_counter);          // store count of deleted gene
 
      avrg_all.push_front(mutant_fitnesses);                   // 
      avrg_mutant_avrg_fitness += mutant_avrg_fitness;	
    }

    if(duplication_counter > 0)
      avrg_mutant_avrg_fitness /= double(duplication_counter);
    list<double> average;
    average.push_front(avrg_mutant_avrg_fitness);
    average.push_front(duplication_counter);
    avrg_all.push_front(average);

  }
  else{
    list<double> dummy = list<double>(0,0);
    avrg_all.push_front(dummy);
  }
  return avrg_all;
}

// detect deletion of genes, then determine robustness against point mutations and 
// knockout of the genes that were deleted subsequently 
// output format : { {nr_dels,mutant_average}, {1,gene_id, knockout_fitness, mutant_fitness
list<list<double> > knockout::del_analysis(Cell * child, int nr_mutants){
 
  list<Gene_iter_wrapper> giw_list;
  list<Gene_iter_wrapper>::iterator giwi;
  list<list<double > > avrg_all = list<list<double> >();
  double avrg_mutant_avrg_fitness = 0.;
  int deletion_counter = 0;
  Cell *parent = child -> getAncestor();
  
  if(parent != NULL && parent -> get_dels() < child -> get_dels() ){

    //    if(!analyzed) analyse_cell(child);                  
    double mutant_avrg_fitness = 0.;
    if(!analyzed){
      bool allow_print = false;
      analyse_cell(parent,allow_print);
    }
    double parent_fitness = parent -> get_fitness();

    giw_list = parent -> compare_genomes(child);    // returns id's of genes present in parent but not in child (deletions)

    for(giwi = giw_list.begin(); giwi != giw_list.end(); giwi++){
      int gene_id = giwi -> get_gene_number();
      list<double> mutant_fitnesses = list<double>();      // to store individual mutant fitnesses
      double k_fitness = 0.;
      double k_contribution;

      if(!analyzed_knockouts){                            // else we already have it's knockout_fitness
	Cell knockout = Cell(*parent);                     // create temporary cell for knockout analysis
	k_fitness = knockout_fitness(&knockout,gene_id); 
	k_contribution = (parent_fitness - k_fitness)/parent_fitness;
      }
      else k_contribution = (giwi -> getGene()) -> getFitnessContribution();
      for(int i = 0; i < nr_mutants; i++){
	Cell mutant = Cell(*parent);
	double mutant_fitness = point_mutant_fitness(&mutant,gene_id);

	mutant_fitnesses.push_front(mutant_fitness/parent_fitness);
	mutant_avrg_fitness += mutant_fitness/(double(nr_mutants)*parent_fitness);	
      }

      deletion_counter ++;
      mutant_fitnesses.push_front(k_contribution);
      mutant_fitnesses.push_front(gene_id);                    // store deleted gene id
      mutant_fitnesses.push_front(deletion_counter);          // store count of deleted gene
 
      avrg_all.push_front(mutant_fitnesses);                   // 
      avrg_mutant_avrg_fitness += mutant_avrg_fitness;	
    }

    if(deletion_counter > 0)
      avrg_mutant_avrg_fitness /= double(deletion_counter);
    list<double> average;
    average.push_front(avrg_mutant_avrg_fitness);
    average.push_front(deletion_counter);
    avrg_all.push_front(average);

  }
  else{
    list<double> dummy = list<double>(0,0);
    avrg_all.push_front(dummy);
  }
  return avrg_all;
}

void knockout::analyse_duplicated_deleted_genes(){
  string savefile  = dish -> getSaveDirectory() + "dups_robustness_to_point_analysis_avrg.dat";
  string savefile2 = dish -> getSaveDirectory() + "dups_robustness_to_point_analysis_all.dat";
  string savefile3 = dish -> getSaveDirectory() + "dels_robustness_to_point_analysis_avrg.dat";
  string savefile4 = dish -> getSaveDirectory() + "dels_robustness_to_point_analysis_all.dat";

  int generation = start_generation;
  double fitness = 0.;

  if(bf::exists(savefile))
    bf::remove(savefile);
  if(bf::exists(savefile2))
    bf::remove(savefile2);
  if(bf::exists(savefile3))
    bf::remove(savefile3);
  if(bf::exists(savefile4))
    bf::remove(savefile4);

  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
    dish -> restoreParamRead(generation); ////////////////////////////////////////////// change petridish parameters to reflect environment at that time during
    if(!analyzed){
      bool allow_print = false;
      analyse_cell(*lin_iter,allow_print);
    }
    fitness = (*lin_iter) -> get_fitness();
    if(fitness > 0.){

      list<list<double> > dup_list = dup_analysis(*lin_iter,number_of_mutants);
      list<list<double> >::iterator dup_list_it = dup_list.begin();
      list<double> dup_vals = *dup_list_it;
      list<double> ::iterator dup_val_it = dup_vals.begin();

      fout.open(savefile.c_str(),ios_base::out | ios_base::app);
      fout << setw(13) << generation << setw(13) << *dup_val_it ; // nr_dels
      dup_val_it++;
      fout << setw(13) << (*dup_val_it) << endl;         // average of all mutants of this cell
      fout.close();

      fout.open(savefile2.c_str(),ios_base::out | ios_base::app);
      dup_list_it++;
      for(; dup_list_it != dup_list.end(); dup_list_it++){             // loop over fitness vals per gene mutated
	dup_vals = *dup_list_it;
	dup_val_it = dup_vals.begin();
	fout << setw(13) << generation << setw(13) << *dup_val_it;   // super_neutral_gene_number
	dup_val_it++;
	fout << setw(13) << *dup_val_it;                             // gene_id
	dup_val_it++;
	for(;dup_val_it != dup_vals.end();dup_val_it++){
	  fout << setw(13) << (*dup_val_it);                 // mutant_fitness_vals
	}
	fout << endl;
      }
      fout.close();
    }
    Cell * parent = (*lin_iter) -> getAncestor();
    if(parent !=NULL && parent -> get_fitness() > 0.){
      list<list<double> > del_list = del_analysis(*lin_iter,number_of_mutants);
      list<list<double> >::iterator del_list_it = del_list.begin();
      list<double> del_vals = *del_list_it;
      list<double> ::iterator del_val_it = del_vals.begin();

      fout.open(savefile3.c_str(),ios_base::out | ios_base::app);
      fout << setw(13) << generation << setw(13) << *del_val_it ; // nr_dels
      del_val_it++;
      fout << setw(13) << (*del_val_it) << endl;         // average of all mutants of this cell
      fout.close();

      fout.open(savefile4.c_str(),ios_base::out | ios_base::app);
      del_list_it++;
      for(; del_list_it != del_list.end(); del_list_it++){             // loop over fitness vals per gene mutated
	del_vals = *del_list_it;
	del_val_it = del_vals.begin();
	fout << setw(13) << generation << setw(13) << *del_val_it;   // super_neutral_gene_number
	del_val_it++;
	fout << setw(13) << *del_val_it;                             // gene_id
	del_val_it++;
	for(;del_val_it != del_vals.end();del_val_it++){
	  fout << setw(13) << (*del_val_it);                 // mutant_fitness_vals
	}
	fout << endl;
      }
      fout.close();
    }
    generation += step_size;
  }
}

// calculating the effect of point mutations of the child, relative to 
// the fitness of the parent ( in standard environments ). To do so, the parent
// is genetically modified to have the same point mutations as its child, but 
// not it's duplications ( possible point mutations incurred on duplicates are ignored )
double knockout::point_mutation_contribution(Cell * child, int generation){
  string savefile = dish -> getSaveDirectory() + "point_effects.dat";
  fout.open(savefile .c_str(),ios_base::out | ios_base::app);
  double point_mutation_contribution = 0.;
  Cell * parent = child -> getAncestor();
  if(parent == NULL) return 0.;
  if(parent -> get_points() < child -> get_points()){
    if(parent -> get_dups() < child -> get_dups() || parent -> get_dels() < child -> get_dels()){
      bool has_duplication = ( parent -> get_dups() < child -> get_dups());
      Cell pre_point_mut = Cell(*child);
      pre_point_mut.reset_genes_to(parent,has_duplication);
      point_mutation_contribution = child -> get_fitness() - pre_point_mut.get_fitness();
    }
    else{
      point_mutation_contribution = (child -> get_fitness()) - (parent -> get_fitness());
    }
    fout << setw(13) << generation << setw(13) << point_mutation_contribution << endl;
  }
  fout.close();
  return point_mutation_contribution;
}

// calculating the effect of duplications and/or deletions of the child, relative to
// the fitness of the parent ( in standard environments ). To do so, the child is
// genetically modified to have the pre- point mutation parameter values of the parent for
// its genes ( except possible point mutations incurred on duplicates ) 
double knockout::dup_del_contribution(Cell * child,int generation){
  string savefile1 = dish -> getSaveDirectory() + "dup_effects.dat";
  string savefile2 = dish -> getSaveDirectory() + "del_effects.dat";

  fout.open(savefile1.c_str(),ios_base::out | ios_base::app);
  fout2.open(savefile2.c_str(),ios_base::out | ios_base::app);

  double dup_del_contribution = 0.;
  Cell * parent = child -> getAncestor();
  if (parent == NULL) return 0. ;

  bool has_duplication = (parent -> get_dups () < child -> get_dups());
  bool has_deletion = (parent -> get_dels() < child -> get_dels());
  if(has_duplication || has_deletion){    
    if( parent -> get_points() < child -> get_points() ){  
      Cell pre_point_mut = Cell(*child);
      pre_point_mut.reset_genes_to(parent,has_duplication);
      dup_del_contribution = pre_point_mut.get_fitness() - (parent -> get_fitness());
    }
    else{
      dup_del_contribution = (child -> get_fitness()) - (parent -> get_fitness() );
    }
  }
  if(has_duplication){
    if(has_deletion){
      fout << setw(13) << generation << setw(13) << dup_del_contribution/2. << endl;
      fout2 << setw(13) << generation << setw(13) << dup_del_contribution/2. << endl;
    }
    else{
      fout << setw(13) << generation << setw(13) << dup_del_contribution << endl;
    }
  }
  else if(has_deletion){
      fout2 << setw(13) << generation << setw(13) << dup_del_contribution << endl;
  }
  fout.close();
  fout2.close();
  return dup_del_contribution;
}

double knockout::WGD_mutation_contributions(Cell * child){
    double WGD_contribution = 0.;
  Cell * parent = child -> getAncestor();
  WGD_contribution = (child -> get_fitness()) - (parent -> get_fitness());
  return WGD_contribution;
  }
  
void knockout::WGD_ancestor_fitness(){
string savefile = dish -> getSaveDirectory() + "WGD_effects.dat";
  if(bf::exists(savefile))
    bf::remove(savefile);
	double WGDcontribute = 0.;
	fout.open(savefile.c_str(),ios_base::out | ios_base::app);
	for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++)
	  {
           WGDcontribute = WGD_mutation_contributions(*lin_iter);
	   fout << setw(13) << WGDcontribute  << std::endl;
	  }
	fout.close();
}

// WORK IN PROGRESS
//
//

void knockout::pre_env_change_fitness(){
  int generation_time = end_generation;
  int envcheckold = dish -> readoutEnvNumberUp(generation_time, 0);
  int envchecknew;
  double fitness_old_env;
  double fitness_new_env;
  double old_fitness_old_env;
  double old_fitness_new_env;
  double new_fitness_old_env;
  double new_fitness_new_env;

  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
  //  if (lin_iter>lineage.begin + 50){ //to check if we can do this analysis, as we need at least 50 future generations to exist, as well as 50 past generations.
      envchecknew = dish -> readoutEnvNumberDown(generation_time, envcheckold);
      if (envcheckold < envchecknew){
        (*lin_iter) -> toFile(dish -> getSaveDirectory());//print cell current
        fitness_old_env = (*lin_iter) -> get_fitness();
        dish ->restoreParamReadSP(envchecknew); //we want to check the fitness in both environments to see how well it's adapted.
        fitness_new_env = (*lin_iter) -> get_fitness();
        // gathering data of the lineage after the environmental change
	for (int t1=0; t1<50; t1++){
          lin_iter--;
        }
        (*lin_iter) -> toFile(dish -> getSaveDirectory());//print cell future lineage
        new_fitness_new_env = (*lin_iter) ->get_fitness();
        dish ->restoreParamReadSP(envcheckold);
        new_fitness_old_env = (*lin_iter) ->get_fitness();
        // gathering data of the lineage before the environmental change
	for (int t1=0; t1<100; t1++){
          lin_iter++;
        }
        (*lin_iter) -> toFile(dish -> getSaveDirectory());//print cell past lineage
        old_fitness_old_env = (*lin_iter) ->get_fitness();
        dish ->restoreParamReadSP(envchecknew);
        old_fitness_new_env = (*lin_iter) ->get_fitness();    
        //make sure we restore the environment to what it was supposed to be like at this time in the run.
	for (int t1=0; t1<50; t1++){
          lin_iter--;
        }
        dish ->restoreParamReadSP(envchecknew); 
        cout << "old_old " <<  old_fitness_old_env << "old_new " <<  old_fitness_new_env <<"now_old " <<  fitness_old_env << "now_new " <<  fitness_new_env << "new_old " <<  new_fitness_old_env << "new_new " <<  new_fitness_new_env << std::endl;
        }
      generation_time -= step_size;
      //blablabla
   // }
  }
}


void knockout::cumulative_mutation_contributions(){
  string savefile1 = dish -> getSaveDirectory() + "dup_effects.dat";
  if(bf::exists(savefile1))
    bf::remove(savefile1);
  string savefile2 = dish -> getSaveDirectory() + "del_effects.dat";
  if(bf::exists(savefile2))
    bf::remove(savefile2);
  string savefile3 = dish -> getSaveDirectory() + "point_effects.dat";
  if(bf::exists(savefile3))
    bf::remove(savefile3);

  string savefile = dish -> getSaveDirectory() + "mutation_contributions.dat";
  if(bf::exists(savefile))
    bf::remove(savefile);
  fout.open(savefile.c_str(),ios_base::out | ios_base::app);
  int generation = start_generation;
  double dup_del = 0.;
  double point = 0.;
  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
    dish -> restoreParamRead(generation); ////////////////////////////////////////////// change petridish parameters to reflect environment at that time during
    dup_del += dup_del_contribution(*lin_iter,generation);
    point += point_mutation_contribution(*lin_iter,generation);
    fout << setw(13) << generation <<  setw(13) << dup_del << setw(13) << point << endl;
    (*dish).grace[3] << generation << dup_del << point << gracesc::endl;
    generation += step_size;
  }
  fout.close();
}

void knockout::ohnologs(){
  string savefile = dish -> getSaveDirectory() + "ohnologs.dat";
  string saveIDfile = dish -> getSaveDirectory() + "IDfile.dat"; //nieuw om ID van duplicaties te bewaren
  if(bf::exists(savefile))
    bf::remove(savefile);
  if(bf::exists(saveIDfile))	//nieuw om ID file te vervangen
    bf::remove(saveIDfile);	//nieuw om ID file te vervangen
  fout.open(savefile.c_str(),ios_base::out | ios_base::app);
  fout2.open(saveIDfile.c_str(), ios_base::out | ios_base::app);//nieuw om ID te gaan schrijven
  //std::cout << "in ohnologs, writing to" << savefile << std::endl
  int current_wgds = (*lineage.begin()) -> get_wgds();
  int currentTemp_wgds = current_wgds;
  int generation = start_generation;
  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++)
  { 
    if( (*lin_iter) -> get_wgds() > current_wgds)
     {
      (*lin_iter) -> toFile(dish -> getSaveDirectory()) ;
      fout << generation; 
      fout2 << generation; // tells the generation of the duplications
      fout2 << setw(13) << ((*lin_iter) -> get_number()); // tells the duplication IDs
      list<pair<int,int> > ohnologs;
      list<list<pair<int,int> > > dup_pair_list = ((*lin_iter) -> get_duplication_pairs());
      ohnologs = dup_pair_list.front();
      list<pair<int,int> >::iterator pair_it;
      for(pair_it = ohnologs.begin(); pair_it != ohnologs.end(); pair_it++)
      {
	fout << setw(13) << ((*lin_iter) -> getGene(pair_it -> first)).getType() <<":" << pair_it -> first << "," << pair_it -> second ; 
      }
      currentTemp_wgds = (*lin_iter) -> get_wgds(); // changed to get all WGDs in one lineage, 
      fout << std::endl;      	
      fout2<< std::endl; //adds endline to new file
     }
   generation += step_size;
   current_wgds = currentTemp_wgds;
  }
  fout.close();
  fout2.close(); //extra to close new filewriter
}

//starts from the end of a population and traces back in those lineages in which a WGD occured how many occured and how long each remained in the population
void knockout::WGD_trace(){ 
  string savefile = dish -> getSaveDirectory() + "WGD-trace.dat";
  if(bf::exists(savefile))
    bf::remove(savefile); 
  fout.open(savefile.c_str(),ios_base::out | ios_base::app);
  int end_gen = dish -> get_generations();
  Cell * lineage_start = lineage.front(); //picks a lineage to see how many WGD the population has already undergone at the time the run starts.
  int historical_wgds = lineage_start -> get_wgds(); // this is then used to stop running the moment an ancestor has undergone that many Whole genome duplications.
  for(pop_iter = population.begin(); pop_iter != population.end(); pop_iter++){
    int wgd_countback = (*pop_iter) -> get_wgds();
    if (wgd_countback > historical_wgds)
    {
      Cell *child = (*pop_iter); //the cell at the end of the population becomes the first child in the ancestor trace
      Cell *parent;
      fout << (wgd_countback - historical_wgds) <<setw(13) << end_gen << setw(13) << ((child) -> get_number());
      int generation_backtrack = 0;
          while (wgd_countback > historical_wgds && (end_gen - start_generation > generation_backtrack))
          {
	   int old_wgdcount = wgd_countback;
	    parent = child -> getAncestor();
	    wgd_countback = (parent) -> get_wgds();
	    generation_backtrack ++;
	    if (old_wgdcount > wgd_countback)
	    {
	      fout <<  setw(13) << (end_gen - generation_backtrack) <<  setw(13) << ((child) -> get_number());
	    }
	    child = parent;
          }
	   fout << std::endl;
      }
  }
      fout.close();
}
  
void knockout::cumulative_mutations(){

  string savefile = dish -> getSaveDirectory() + "mutations.dat";
  if(bf::exists(savefile))
    bf::remove(savefile);
  fout.open(savefile.c_str(),ios_base::out | ios_base::app);

  int generation = start_generation;
  int start_dups = 0;
  int start_dels = 0;
  int start_points = 0;
  int start_major_dups = 0;
  int start_major_dels = 0;
  int start_major_ins = 0;
  int start_wgds = 0;

  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
    if(lin_iter == lineage.begin()){
      start_dups = (*lin_iter) -> get_dups();
      start_dels = (*lin_iter) -> get_dels();
      start_points = (*lin_iter) -> get_points();
      start_wgds = (*lin_iter) -> get_wgds();
      start_major_dups = (*lin_iter) -> get_major_dups();
      start_major_dels = (*lin_iter) -> get_major_dels();
      start_major_ins = (*lin_iter) -> get_major_ins();
    }

    fout << setw(13) << generation 
	 <<  setw(13) << (*lin_iter) -> get_dups() - start_dups 
	 << setw(13) << (*lin_iter) -> get_dels() - start_dels 
	 << setw(13) << (*lin_iter) -> get_points() - start_points
	 << setw(13) << (*lin_iter) -> get_major_dups() - start_major_dups
	 << setw(13) << (*lin_iter) -> get_major_dels() - start_major_dels 
	 << setw(13) << (*lin_iter) -> get_major_ins() - start_major_ins 
	 << setw(13) << (*lin_iter) -> get_wgds() - start_wgds << endl;
    (*dish).grace[1] << generation  
		     << (*lin_iter) -> get_dups() - start_dups 
		     << (*lin_iter) -> get_dels() - start_dels
		     << (*lin_iter) -> get_points()  - start_points 
		     << (*lin_iter) -> get_major_dups() - start_major_dups 
		     << (*lin_iter) -> get_major_dels() -start_major_dels 
		     << (*lin_iter) -> get_major_ins() -start_major_ins 
		     << (*lin_iter) -> get_wgds() - start_wgds << gracesc::endl;
    generation += step_size;
  }
  fout.close();
}

void knockout::contribution_per_gene_type(){

 string savefile = dish -> getSaveDirectory() + "gene_type_contribution.dat";
 if(bf::exists(savefile))
   bf::remove(savefile);
 fout.open(savefile.c_str(),ios_base::out | ios_base::app);

 int generation = start_generation;
 for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
   dish -> restoreParamRead(generation); ////////////////////////////////////////////// change petridish parameters to reflect environment
					 ///at that time during run. Not sure if it needs to be here
   double genes =  (*lin_iter)->get_nmbr_genes();
   double prop_tfs = (*lin_iter)->get_nmbr_tfs()/ genes;
   double prop_enzymes = (*lin_iter)->get_nmbr_enzymes() / genes;
   double prop_pumps = (*lin_iter)->get_nmbr_pumps() / genes;
   fout << setw(13) << generation  << setw(13) << (*lin_iter)->tfs_contribution() 
	<< setw(13) << (*lin_iter)->enzymes_contribution() << setw(13) << (*lin_iter)->pumps_contribution() 
	<< setw(13) << prop_tfs << setw(13) << prop_enzymes << setw(13) << prop_pumps << endl ;
   (*dish).grace[0] << generation  << (*lin_iter)->tfs_contribution() << (*lin_iter)->enzymes_contribution() 
		    << (*lin_iter)->pumps_contribution() 
		    << prop_tfs << prop_enzymes << prop_pumps << gracesc::endl ;
   generation += step_size;
 }
 fout.close();
}

void knockout::type_numbers(){

 string savefile = dish -> getSaveDirectory() + "type_numbers.dat";
 if(bf::exists(savefile))
   bf::remove(savefile);
 fout.open(savefile.c_str(),ios_base::out | ios_base::app);

 int generation = start_generation;
 for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
   double genes =  (*lin_iter)->get_nmbr_genes();
   double tfs = (*lin_iter)->get_nmbr_tfs();
   double enzymes = (*lin_iter)->get_nmbr_enzymes();
   double pumps = (*lin_iter)->get_nmbr_pumps();
   fout << setw(13) << generation  << setw(13) << genes
	<< setw(13) << tfs << setw(13) << enzymes << setw(13) << pumps <<endl; 

   generation += step_size;
 }
 fout.close();
}


void knockout::mutations_per_gene_type(){

 string savefile = dish -> getSaveDirectory() + "gene_type_mutations.dat";
 if(bf::exists(savefile))
   bf::remove(savefile);
 fout.open(savefile.c_str(),ios_base::out | ios_base::app);

 int generation = start_generation;
 int start_tf_dups = 0;
 int start_tf_dels = 0;
 int start_tf_points = 0;
 int start_enzyme_dups = 0;
 int start_enzyme_dels = 0;
 int start_enzyme_points = 0;
 int start_pump_dups = 0;
 int start_pump_dels = 0;
 int start_pump_points = 0;
 int start_tf_bind_muts = 0;
 int start_tf_op_muts = 0;
 int start_enzyme_op_muts = 0;
 int start_pump_op_muts = 0;

 for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
   dish -> restoreParamRead(generation); ////////////////////////////////////////////// change petridish parameters to reflect environment at that time during
   if(lin_iter == lineage.begin()){
     start_tf_dups = (*lin_iter) -> get_tf_dups();
     start_tf_dels = (*lin_iter) -> get_tf_dels();
     start_tf_points = (*lin_iter) -> get_tf_points();
     start_enzyme_dups = (*lin_iter) -> get_enzyme_dups();
     start_enzyme_dels = (*lin_iter) -> get_enzyme_dels();
     start_enzyme_points = (*lin_iter) -> get_enzyme_points();
     start_pump_dups = (*lin_iter) -> get_pump_dups();
     start_pump_dels = (*lin_iter) -> get_pump_dels();
     start_pump_points = (*lin_iter) -> get_pump_points();
     start_tf_bind_muts = (*lin_iter) -> get_tf_bind_muts();
     start_tf_op_muts = (*lin_iter) -> get_tf_op_muts();
     start_enzyme_op_muts = (*lin_iter) -> get_enz_op_muts();
     start_pump_op_muts = (*lin_iter) -> get_pump_op_muts();
   }
   fout << setw(13) << generation 
	<< setw(13) << (*lin_iter)->get_tf_dups() - start_tf_dups
	<< setw(13) << (*lin_iter)->get_tf_dels() - start_tf_dels
	<< setw(13) << (*lin_iter)->get_tf_points() - start_tf_points
	<< setw(13) << (*lin_iter)->get_enzyme_dups() - start_enzyme_dups
	<< setw(13) << (*lin_iter)->get_enzyme_dels() - start_enzyme_dels
	<< setw(13) << (*lin_iter)->get_enzyme_points() - start_enzyme_points
	<< setw(13) << (*lin_iter)->get_pump_dups() - start_pump_dups
	<< setw(13) << (*lin_iter)->get_pump_dels() - start_pump_dels
	<< setw(13) << (*lin_iter)->get_pump_points() - start_pump_points
	<< setw(13) << (*lin_iter)->get_tf_bind_muts() - start_tf_bind_muts 
	<< setw(13) << (*lin_iter)->get_tf_op_muts() - start_tf_op_muts 
	<< setw(13) << (*lin_iter)->get_enz_op_muts() -start_enzyme_op_muts 
	<< setw(13) << (*lin_iter)->get_pump_op_muts() - start_pump_op_muts << endl;  
   (*dish).grace[2] << generation  
		    << (*lin_iter)->get_tf_dups() - start_tf_dups
		    << (*lin_iter)->get_tf_dels()- start_tf_dels  
		    << (*lin_iter)->get_tf_points() - start_tf_points
		    << (*lin_iter)->get_enzyme_dups() - start_enzyme_dups
		    << (*lin_iter)->get_enzyme_dels()- start_enzyme_dels
		    << (*lin_iter)->get_enzyme_points() - start_enzyme_points
		    << (*lin_iter)->get_pump_dups() - start_pump_dups 
		    << (*lin_iter)->get_pump_dels() - start_pump_dels
		    << (*lin_iter)->get_pump_points() - start_pump_points
		    << (*lin_iter)->get_tf_bind_muts()- start_tf_bind_muts 
		    << (*lin_iter)->get_tf_op_muts() - start_tf_op_muts 
		    << (*lin_iter)->get_enz_op_muts() -start_enzyme_op_muts 
		    << (*lin_iter)->get_pump_op_muts() - start_pump_op_muts << gracesc::endl;  
   generation += step_size;
 }
 fout.close();
}



//expectation for different gene types can be calculated by multiplying total expectation with
//the proportion of this type in the genome;
void knockout::expectations_per_gene_type(){

  string savefile = dish -> getSaveDirectory() + "gene_type_expectation.dat";
  if(bf::exists(savefile))
    bf::remove(savefile);
  fout.open(savefile.c_str(),ios_base::out | ios_base::app);

  double tfdups = 0.;
  double tfdels = 0.;
  double tfpoints = 0.;
  double tf_op_muts = 0.;
  double tf_bind_muts = 0.;
  double enzymedups = 0.;
  double enzymedels = 0.;
  double enzymepoints = 0.;
  double enzyme_op_muts = 0.;
  double pumpdups = 0.;
  double pumpdels = 0.;
  double pumppoints = 0.;
  double pump_op_muts = 0.;
  int generation = start_generation;

  double tf_mut_type_chance = 1./8.;
  double enz_mut_type_chance = 1./5.;
  double pump_mut_type_chance = 1./5.;
  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
    dish -> restoreParamRead(generation); ////////////////////////////////////////////// change petridish parameters to reflect environment at that time during
    tfdups += dups_expectation(*lin_iter, (*lin_iter)->get_nmbr_tfs());
    tfdels += dels_expectation(*lin_iter,(*lin_iter)->get_nmbr_tfs());
    tfpoints += points_expectation(*lin_iter,(*lin_iter)->get_nmbr_tfs());
    tf_op_muts   += tf_mut_type_chance * points_expectation(*lin_iter,(*lin_iter)->get_nmbr_tfs());
    tf_bind_muts += tf_mut_type_chance * points_expectation(*lin_iter,(*lin_iter)->get_nmbr_tfs());
    enzymedups += dups_expectation(*lin_iter, (*lin_iter)->get_nmbr_enzymes());
    enzymedels += dels_expectation(*lin_iter, (*lin_iter)->get_nmbr_enzymes());
    enzymepoints += points_expectation(*lin_iter, (*lin_iter)->get_nmbr_enzymes());
    enzyme_op_muts += enz_mut_type_chance * points_expectation(*lin_iter, (*lin_iter)->get_nmbr_enzymes());
    pumpdups += dups_expectation(*lin_iter, (*lin_iter)->get_nmbr_pumps());
    pumpdels += dels_expectation(*lin_iter, (*lin_iter)->get_nmbr_pumps());
    pumppoints += points_expectation(*lin_iter, (*lin_iter)->get_nmbr_pumps());    
    pump_op_muts += pump_mut_type_chance * points_expectation(*lin_iter, (*lin_iter)->get_nmbr_pumps());    

    fout << setw(13) << generation 
	 << setw(13) << tfdups << setw(13) << tfdels << setw(13) << tfpoints 
	 << setw(13) << enzymedups << setw(13) <<  enzymedels << setw(13) << enzymepoints 
	 << setw(13) << pumpdups << setw(13) << pumpdels << setw(13) << pumppoints 
	 << setw(13) << tf_bind_muts << setw(13) << tf_op_muts << setw(13) << enzyme_op_muts << setw(13) << pump_op_muts << endl;
    (*dish).grace[4] << generation << tfdups << tfdels << tfpoints  << enzymedups 
    		     << enzymedels << enzymepoints  << pumpdups << pumpdels << pumppoints 
		     << tf_bind_muts << tf_op_muts << enzyme_op_muts << pump_op_muts << gracesc::endl;
    generation += step_size;
  }
  fout.close();
}



void knockout::cumulative_mutation_expectations(){

  string savefile = dish -> getSaveDirectory() + "expectected_mutations.dat";
  if(bf::exists(savefile))
    bf::remove(savefile);
  fout.open(savefile.c_str(),ios_base::out | ios_base::app);

  double dups = 0.;
  double dels = 0.;
  double points = 0.;
  double major_dups = 0.;
  double major_dels = 0.;
  double major_ins = 0.;
  int generation = start_generation;


  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
    dish -> restoreParamRead(generation); ////////////////////////////////////////////// change petridish parameters to reflect environment at that time during
    int genome_size = (*lin_iter) -> get_nmbr_genes();
    dups += dups_expectation(*lin_iter,genome_size);
    dels += dels_expectation(*lin_iter,genome_size);
    points += points_expectation(*lin_iter,genome_size);
    major_dups +=  major_dups_expectation(*lin_iter);
    major_dels += major_dels_expectation(*lin_iter);
    major_ins += major_ins_expectation(*lin_iter);

    fout << setw(13) << generation <<  setw(13) << dups << setw(13) << dels
	 << setw(13) << points << setw(13) << major_dups
	 << setw(13) << major_dels << setw(13) << major_ins << endl;
    (*dish).grace[5] << generation << dups << dels
		  << points << major_dups
		  << major_dels << major_ins << gracesc::endl;
    generation += step_size;
  }
  fout.close();
}

//is called by cumulative_mutation_expectations()
double knockout::per_gene_mutation_rate(){
  //  double mutationrate = dish -> get_mutation_rate();
  double total_mut = INS + DUP + DEL + POINT;
  double per_gene_rate = MUTATION_RATE/total_mut;

  return per_gene_rate;
}

//is called by cumulative_mutation_expectations()
 double knockout::dups_expectation(Cell *c, int nmbr_genes){
  double mut_rate = per_gene_mutation_rate();
  double expectation = mut_rate*DUP*nmbr_genes;
  return expectation;
}

//is called by cumulative_mutation_expectations()
double knockout:: dels_expectation(Cell *c, int nmbr_genes){
  double mut_rate = per_gene_mutation_rate();
  double expectation = mut_rate*DEL*nmbr_genes;
  return expectation;
}

//is called by cumulative_mutation_expectations()
double  knockout::points_expectation(Cell *c, int nmbr_genes){
  double mut_rate = per_gene_mutation_rate();
  double expectation = mut_rate*POINT*nmbr_genes;
  return expectation;
}

//is called by cumulative_mutation_expectations()
double  knockout::major_dups_expectation(Cell *c){  
  double expectation = 0.;
  if(PER_GENE_MUTATION == 2 || PER_GENE_MUTATION == 3){
    double mut_rate = per_gene_mutation_rate()*gcr_scaling;         // *gcr_scaling because on average 1/gcr_scaling of genome is 
                                                          // affected
    expectation = mut_rate*DUP;
  }
  else if(PER_GENE_MUTATION == 7){    
    expectation = major_dup_rate;
  }
  return expectation;
}

//is called by cumulative_mutation_expectations()
double  knockout::major_dels_expectation(Cell *c){
  double expectation = 0.;
  if(PER_GENE_MUTATION == 2 || PER_GENE_MUTATION == 3){
    double mut_rate = per_gene_mutation_rate()*gcr_scaling;         // *gcr_scaling because on average 1/gcr_scaling of genome is 
                                                          // affected
    expectation = mut_rate*DEL;
  }
  else if(PER_GENE_MUTATION == 7){    
    expectation = major_del_rate;
  }
  return expectation;
}

//is called by cumulative_mutation_expectations()
double  knockout::major_ins_expectation(Cell *c){
  double expectation = 0.;
  if(PER_GENE_MUTATION == 3){
    double mut_rate = per_gene_mutation_rate()*gcr_scaling;         // *4 because on average 1/4 of genome is 
                                                          // affected
    expectation = mut_rate*INS;
  }
  else if(PER_GENE_MUTATION == 7){    
    expectation = major_ins_rate;
  }
  return expectation;
}

void knockout::gene_contribution_graphs(){
  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
    // if(! (*lin_iter) -> consistent_genome()) cout <<  "genome not consistent " << **lin_iter << endl;
    (*lin_iter) -> geneContributionGraph( dish -> getSaveDirectory());
  }
}

void knockout::function_graphs(){
  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
    (*lin_iter) -> Graph( dish -> getSaveDirectory());
  }
}

void knockout::genome_order_graphs(){
  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
    (*lin_iter) -> genomeOrderGraph( dish -> getSaveDirectory());
  }
}

//the actual fitnesses of ancesters will have been overwritten with
//standardized fitness values after analyse_lineage() has been performed
//on 'lineage'
void knockout::fitness_ancestors(){
  string savefile;
  int generation = start_generation;
  // after 'analyzing', saved fitnesses are overwritten with 'standard' fitness
  if(analyzed) 
    savefile = dish -> getSaveDirectory() + "ancestor_standard_fitnesses.dat";
  else 
    savefile = dish -> getSaveDirectory() + "ancestor_real_fitnesses.dat";

  if(bf::exists(savefile))
    bf::remove(savefile);
  fout.open(savefile.c_str(),ios_base::out | ios_base::app);
  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){//--lineage.end(); lin_iter++){
    dish -> restoreParamRead(generation); ////////////////////////////////////////////// change petridish parameters to reflect environment at that time during
    double fitness = (*lin_iter) -> get_fitness();
    fout << setw(13) << generation <<  setw(13) << fitness;
    if(analyzed){
      double split_f01 = dish -> FitnessFunction( (*lin_iter) -> splitFitness(0));
      double split_f1 =  dish -> FitnessFunction( (*lin_iter) -> splitFitness(1));
      double split_f10 = dish -> FitnessFunction( (*lin_iter) -> splitFitness(2));
      fout << setw(13) << split_f01 << setw(13) << split_f1 << setw(13) << split_f10;
    }
    fout << endl;
    generation += step_size;
  }
  fout.close();
  if(analyzed){
    savefile = dish -> getSaveDirectory() + "ancestor_standard_production.dat";
    if(bf::exists(savefile))
      bf::remove(savefile);
    fout.open(savefile.c_str(),ios_base::out | ios_base::app);
    generation = start_generation;
    for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){// --
      dish -> restoreParamRead(generation); ////////////////////////////////////////////// change petridish parameters to reflect environment at that time during
      double split_a01 =  (*lin_iter) -> splitAnabolite(0);
      double split_a1 = (*lin_iter) -> splitAnabolite(1);
      double split_a10 = (*lin_iter) -> splitAnabolite(2);
      double total_a = split_a01 + split_a1 + split_a10;
      fout << setw(13) << generation 
	   << setw(13) << log10(total_a) 
	   << setw(13) << log10(split_a01) 
	   << setw(13) << log10(split_a1) 
	   << setw(13) << log10(split_a10) << endl;
      generation += step_size;
    }
    fout.close();
  }
}

void knockout::size_ancestors(){
 
  string savefile = dish -> getSaveDirectory() + "ancestor_sizes.dat";
  if(bf::exists(savefile))
    bf::remove(savefile);
  fout.open(savefile.c_str(),ios_base::out | ios_base::app);
  int generation = start_generation;
  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){ //--
    int genome_size = (*lin_iter) -> get_nmbr_genes();
    fout << setw(13) << generation <<  setw(13) << genome_size << endl;
    generation += step_size;
  }
  fout.close();
}

void knockout::print_cells(){
  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
    (*lin_iter) -> toFile(dish -> getSaveDirectory());
  }
}


void knockout::param_sweep(Type type, int par,int steps,double start_val,double end_val){
  for(lin_iter = lineage.begin(); lin_iter != lineage.end(); lin_iter++){
    int checkgeneration = start_generation;
    dish -> restoreParamRead(checkgeneration); ////////////////////////////////////////////// change petridish parameters to reflect environment at that time during run.
    sweep_cell(*lin_iter,type,par,steps,start_val,end_val);
    checkgeneration += step_size;
  }
}

void knockout::sweep_cell(Cell * c,Type type,int par,int steps,double s_val, double e_val){
  ostringstream formatter;
  string savefile;
  double start_val = s_val;
  double end_val = e_val;
  // start_val * stepbase^(steps-1) = endval -> stepbase^(steps-1) = endval/start_val ->
  double stepbase = pow(end_val/start_val , 1./double(steps-1)); // -1 because we want steps to include the start value step 

  double true_val;                // to store original parameter value
  double par_val = start_val;

  list<Pump> * p_list = &(c -> pump_list);
  list<Pump>::iterator p_it;
  list<Enzyme> * e_list = &(c -> enzyme_list);
  list<Enzyme>::iterator e_it;
  list<TF> * tf_list = &(c -> tf_list);
  list<TF>::iterator tf_it;

  switch (type){
  case PUMP:
    for(p_it= p_list->begin(); p_it !=p_list->end(); p_it++){
      formatter.str("");
      formatter << "sweep." << c -> getnmbr() << ".pump." << p_it -> getNumber() << "."  << par ;// << ".c.dat";
      string save_base_name = dish -> getSaveDirectory() + formatter.str();
     
      true_val = c -> set_par(*(p_it),par,par_val);

      int assays = ASSAYS;
      double env = 0.1;

      for(int j = 0; j< assays; j++){
	
	string c_save = save_base_name+".c." + "env" +  boost::lexical_cast<std::string>(env) + ".dat";
	string r_save = save_base_name+".r." + "env" +  boost::lexical_cast<std::string>(env) + ".dat";

	if(bf::exists(c_save))
	  bf::remove(c_save);

	if(bf::exists(r_save))
	  bf::remove(r_save);

	fout.open(c_save.c_str(),ios_base::out | ios_base::app);
	fout2.open(r_save.c_str(),ios_base::out | ios_base::app);
	for(int i = 1; i<= steps ; i++){
	  int Time = 0;
	  bool allow_print = false;
	  analyse_cell_in_env(c, env, j, Time,allow_print);
	  fout  << setw(13) << par_val << setw(13) << c -> ConcPoint() << std::endl; 
	  fout2 << setw(13) << par_val << setw(13) << c -> RatePoint() << std::endl;
	  par_val *= stepbase;
	  c->set_par(*(p_it),par,par_val);
	}
	fout.close();
	fout2.close();

	env *=10;
	par_val = start_val;
	c ->set_par(*(p_it),par,par_val);
      }

      c -> set_par(*(p_it),par,true_val);
    }

    break;
  case ENZYME:
      for(e_it= e_list->begin(); e_it !=e_list->end(); e_it++){
      formatter.str("");
      formatter << "sweep." << c -> getnmbr() << ".enz." << e_it -> getNumber() << "." <<  par ;
      string save_base_name = dish -> getSaveDirectory() + formatter.str();
      
      true_val = c -> set_par(*(e_it),par,par_val);

      int assays = ASSAYS;
      double env = 0.1;

      for(int j = 0; j< assays; j++){
	string c_save = save_base_name+".c." + "env" +  boost::lexical_cast<std::string>(env) + ".dat";
	string r_save = save_base_name+".r." + "env" +  boost::lexical_cast<std::string>(env) + ".dat";

	if(bf::exists(c_save))
	  bf::remove(c_save);
	if(bf::exists(r_save))
	  bf::remove(r_save);

	fout.open(c_save.c_str(),ios_base::out | ios_base::app);
	fout2.open(r_save.c_str(),ios_base::out | ios_base::app);

	for(int i = 1; i<= steps ; i++){
	  int Time = 0;
	  bool allow_print = false;
	  analyse_cell_in_env(c, env, j, Time,allow_print);
	  fout << setw(13) << par_val << setw(13) << c -> ConcPoint() << std::endl;
	  fout2 << setw(13) << par_val << setw(13) << c -> RatePoint() << std::endl;
	  par_val *= stepbase;
	  c->set_par(*(e_it),par,par_val);
	}
	fout.close();
	fout2.close();

	env *=10;
	par_val = start_val;
	c -> set_par(*(e_it),par,par_val);
      }

      c -> set_par(*(e_it),par,true_val);
    }

    break;
  case TrF:

    for(tf_it= tf_list->begin(); tf_it !=tf_list->end(); tf_it++){
      formatter.str("");
      formatter << "sweep." << c -> getnmbr() << ".tf." << tf_it -> getNumber() << "." << par ;
      string save_base_name = dish -> getSaveDirectory() + formatter.str();

      true_val = c -> set_par(*(tf_it),par,par_val);

      int assays = ASSAYS;
      double env = 0.1;

      for(int j = 0; j< assays; j++){

	string c_save = save_base_name+".c." + "env" +  boost::lexical_cast<std::string>(env) + ".dat";
	string r_save = save_base_name+".r." + "env" +  boost::lexical_cast<std::string>(env) + ".dat";

	if(bf::exists(c_save))
	  bf::remove(c_save);
	if(bf::exists(r_save))
	  bf::remove(r_save);

	fout.open(c_save.c_str(),ios_base::out | ios_base::app);
	fout2.open(r_save.c_str(),ios_base::out | ios_base::app);

	for(int i = 1; i<= steps ; i++){
	  int Time = 0;
	  bool allow_print = false;
	  analyse_cell_in_env(c, env, j, Time,allow_print);
	  fout << setw(13) << par_val << setw(13) << c -> ConcPoint() << std::endl;
	  fout2 << setw(13) << par_val << setw(13) << c -> RatePoint() << std::endl;
	  par_val *= stepbase;
	  c->set_par(*(tf_it),par,par_val);
	}
	fout.close();
	fout2.close();

	env *=10;
	par_val = start_val;
	c->set_par(*(tf_it),par,par_val);
      }

      c -> set_par(*(tf_it),par,true_val);
    }
    break;
  case NOTYPE:
    formatter.str("");
    formatter << "sweep." << c -> getnmbr() << ".env.c.dat";
    savefile = dish -> getSaveDirectory() + formatter.str();
    if(bf::exists(savefile))
      bf::remove(savefile);
    fout.open(savefile.c_str(),ios_base::out | ios_base::app);

    formatter.str("");
    formatter << "sweep." << c -> getnmbr() << ".env.r.dat";
    savefile = dish -> getSaveDirectory() + formatter.str();
    if(bf::exists(savefile))
      bf::remove(savefile);
    fout2.open(savefile.c_str(),ios_base::out | ios_base::app);
      int assays = steps;
    double env = start_val;

    for(int j = 0; j< assays; j++){

      int Time = 0;
      bool allow_print = false;
      analyse_cell_in_env(c, env, 0, Time,allow_print);
      fout << setw(13) << env << setw(13) << c -> ConcPoint() << std::endl;
      fout2 << setw(13) << env << setw(13) << c -> RatePoint() << std::endl;

      env *= stepbase;
    }

    fout.close();
    fout2.close();
    break;
  }
}

void knockout::plotGrace(){
  if(((*dish).grace).open()){
    (*dish).grace[0].draw();
    (*dish).grace[0] << "autoscale";
    (*dish).grace[1].draw();
    (*dish).grace[1] << "autoscale";
    (*dish).grace[2].draw();
    (*dish).grace[2] << "autoscale";
    (*dish).grace[3].draw();
    (*dish).grace[3] << "autoscale";
    (*dish).grace[4].draw();
    (*dish).grace[4] << "autoscale";
    (*dish).grace[5].draw();
    (*dish).grace[5] << "autoscale";
    (*dish).grace.redraw();
  }
}

void knockout::closeGrace(){
  if((*dish).grace.open()){
    (*dish).grace.close();
    cout << "grace closed" << endl;
  }
}

 bool CellFitnessPairsSortPredicate(const pair<Cell *,double> lhs,const pair<Cell *,double> rhs){
   return lhs.second < rhs.second;
 }
;

