#ifndef GENE_ITER_WRAPPER_HPP_
#define GENE_ITER_WRAPPER_HPP_
//#include "temp.hpp"
#include "gene.hpp"
#include <vector>
#include <list>
#include <boost/serialization/export.hpp>
//#include "cell.hpp"

using namespace std;

//class Cell;

class Gene_iter_wrapper{

 public:
  Gene_iter_wrapper():typ(NOTYPE){};
  Gene_iter_wrapper(list<TF>::iterator tf_iter):
    typ(TrF),gene_number((*tf_iter).getNumber())
    ,tf_it(tf_iter),duped(false){};
  Gene_iter_wrapper(list<Pump>::iterator pump_iter):
    typ(PUMP),gene_number((*pump_iter).getNumber())
    ,pump_it(pump_iter),duped(false){};
  Gene_iter_wrapper(list<Enzyme>::iterator enzyme_iter):
    typ(ENZYME),gene_number((*enzyme_iter).getNumber())
    ,enz_it(enzyme_iter),duped(false){};
  Type getType()const{return typ;};
  int get_gene_number()const{return gene_number;};
  bool is_duped(){return duped;};
  void reset_duped(){duped = false;};
  const list<TF>::iterator& get_tf_it()const{return tf_it;};
  const list<Pump>::iterator& get_pump_it()const{return pump_it;};
  const list<Enzyme>::iterator& get_enz_it()const{return enz_it;};
  Gene * getGene(){
    Gene * g;
    switch(typ){
    case TrF: g = &(*tf_it);
      break;
    case ENZYME: g = &(*enz_it);
      break;
    case PUMP: g = &(*pump_it);
      break;
    default: g = new Gene();
      cout << "noType gene_iter_wrapper";
      break;
    }
    return g;
  }
  string toString(){
    ostringstream formatter;
    switch(typ){
    case TrF: formatter << (*tf_it).toString(); break;
    case ENZYME: formatter << (*enz_it).toString();break;
    case PUMP: formatter << (*pump_it).toString();break;
    default: formatter << "noType gene_iter_wrapper";break;
    }
    return formatter.str();
  }
 private:
  Type typ;
  int gene_number;
  list<TF>::iterator tf_it;
  list<Pump>::iterator pump_it;
  list<Enzyme>::iterator enz_it;
  bool duped;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int){
    ar & BOOST_SERIALIZATION_NVP(typ)
      & BOOST_SERIALIZATION_NVP(gene_number)
      //      & BOOST_SERIALIZATION_NVP(tf_it)   //we can't serialize these iterators
      //      & BOOST_SERIALIZATION_NVP(pump_it)
      //      & BOOST_SERIALIZATION_NVP(enz_it)
      ;      
  }

};
//BOOST_CLASS_EXPORT(Gene_iter_wrapper);
#endif
