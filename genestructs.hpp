#include <vector>
#include "temp.hpp"

#ifndef GENESTRUCTS_HPP_
#define GENESTRUCTS_HPP_

using namespace std;

class TF;
class Gene;

class GeneList{
 public:
  GeneList();
  GeneList(Type);
		
  Type getType(){return type;};
  const vector<Gene *> &  getList()const{return glist;};
  int nmbrGenes(){return genes;};
  void addGene(Gene *);
  string toString();
  void removeGene(Gene *);
  void replaceGenePtr(const Gene*,Gene*);
  void removeAll();
  GeneList(const GeneList&);
 private:
  int genes;
  Type type;
  vector<Gene *> glist;  
  //  string name;
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int){
    cout << "start genelist" << endl;
    ar & BOOST_SERIALIZATION_NVP(genes);
    ar & BOOST_SERIALIZATION_NVP(type);
    ar & BOOST_SERIALIZATION_NVP(glist); 
    //    ar  & BOOST_SERIALIZATION_NVP(name);
    cout << "end genelist " << endl;
  }
};

ostream & operator << (ostream &,GeneList &);
class GeneCollection{
 public:
  GeneCollection():tfs(TrF),enzymes(ENZYME),pumps(PUMP),clist(3){		
		
    clist[0] = & tfs;
    clist[1] = & enzymes;
    clist[2] = & pumps;
  };
  GeneCollection(const GeneCollection&);
  GeneList & get_tfs(){return tfs;};
  GeneList & get_enzymes(){return enzymes;};
  GeneList & get_pumps(){return pumps;};
  const vector<GeneList *> & getList()const {return clist;};
  string toString();
  void addGene(Gene *);
  void removeGene(Gene *); 
  void addGeneList(GeneList *);
  GeneList & getGeneList(Type);
  void replaceGenePtr(const Gene*, Gene*);
  void empty();
 private:
  GeneList  tfs;
  GeneList  enzymes;
  GeneList  pumps;
  vector<GeneList *> clist;
  //  string name;		

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int){

    cout << "start collection" << endl;
    ar & BOOST_SERIALIZATION_NVP(tfs);
    ar & BOOST_SERIALIZATION_NVP(enzymes);
    ar & BOOST_SERIALIZATION_NVP(pumps); 
      // & BOOST_SERIALIZATION_NVP(clist)
      // & BOOST_SERIALIZATION_NVP(name);
    cout << "end collection " << endl;
  }
};
ostream & operator << (ostream &, GeneCollection &);
//#include "gene.hpp"


#endif
