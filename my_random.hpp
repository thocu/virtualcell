#include <boost/random/lagged_fibonacci.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/uniform_int.hpp>            //addition
#include <boost/random/normal_distribution.hpp>    //addition
#include <boost/random/geometric_distribution.hpp> //addition
#include <boost/random/exponential_distribution.hpp> //addition
#ifndef MYRANDOM
#define MYRANDOM

class MyRandom{
  boost::lagged_fibonacci19937 random_gen; //our random number generator

  //various distributions to draw random numbers from:
  boost::uniform_real<double> uniform_dist;
  boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<double> >* uniform_gen;

  boost::uniform_real<double> uniform_dist2; 
  boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<double> >* uniform_gen2; 

  boost::uniform_real<double> uniform_dist3; 
  boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<double> >* uniform_gen3; 

  boost::uniform_real<double> uniform_dist4; 
  boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<double> >* uniform_gen4; 

  boost::uniform_int<int> uniform_int_dist10; 
  boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_int<int> >* uniform_int_gen10; 

  boost::normal_distribution<double> normal_dist; 
  boost::variate_generator<boost::lagged_fibonacci19937&, boost::normal_distribution<double> >* normal_dist_gen; 

  boost::normal_distribution<double> normal_dist2; 
  boost::variate_generator<boost::lagged_fibonacci19937&, boost::normal_distribution<double> >* normal_dist_gen2; 

  boost::geometric_distribution<int> geometric_dist1;
  boost::variate_generator<boost::lagged_fibonacci19937&, boost::geometric_distribution<int> >* geometric_dist_gen1; 

  boost::exponential_distribution<double> exponential_dist1;
  boost::variate_generator<boost::lagged_fibonacci19937&, boost::exponential_distribution<double> >* exponential_dist_gen1; 
 public:
  MyRandom();
  ~MyRandom();
  void init(unsigned int seed);
  inline double uniform();
  inline double uniform2();    
  inline double uniform3();    
  inline double uniform4();    
  inline int uniform_int10(); 
  inline double normal(); 
  inline double normal2();
  inline int geometric1();
  inline double exponential1();
};

/*
  A number in [0,1)
*/

//functions to call from the MyRandom objects yielding random numbers from the appropriate distribution:
double MyRandom::uniform()     //uniform [0..1)
{
  return (*uniform_gen)();
}

double MyRandom::normal()      //normal with median 0 , standard dev 1
{
  return (*normal_dist_gen)();
}

double MyRandom::normal2()    //normal with mean -0.300, standard dev 1.3
{
  return (*normal_dist_gen2)();
}

double MyRandom::uniform2()   //uniform [-1..1)
{
	return (*uniform_gen2)();
}

double MyRandom::uniform3()  //uniform [-1.5..1.5)
{
        return (*uniform_gen3)();
}

double MyRandom::uniform4()  //uniform [-1.75..1.25)
{
        return (*uniform_gen4)();
}

int MyRandom::uniform_int10()   // uniform integers [1..10]
{
	return (*uniform_int_gen10)();
}

int MyRandom::geometric1() // geometrically distributed integers (p=0.9)
{
  return(*geometric_dist_gen1)();
}

double MyRandom::exponential1() // geometrically distributed integers (p=0.5)
{
  return(*exponential_dist_gen1)();
}

   #ifndef MYRANDOM_CPP
   #define MYRANDOM_CPP
extern MyRandom pop_rand;
extern MyRandom evo_rand;
extern MyRandom env_rand;
extern MyRandom switch_rand;
   #endif
#endif

