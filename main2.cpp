#include "knockout.hpp"
#include <signal.h>
#include <boost/tokenizer.hpp>

#include <boost/lexical_cast.hpp>
#include <unistd.h>
#include <sys/resource.h>


namespace po = boost::program_options;
namespace bf = boost::filesystem;


void leave(int sig);

// A helper function to simplify the main part.
template<class T>
ostream& operator<<(ostream& os, const vector<T>& v)
{
    copy(v.begin(), v.end(), ostream_iterator<T>(cout, " ")); 
    return os;
}

/* Auxiliary functions for checking input for validity. */

/* Function used to check that 'opt1' and 'opt2' are not specified
   at the same time. */

//------------------------------------------------------------------
int main(int argc, char* argv[]){

  try{
    /* increase stack size to avoid stack overflow when serializing */
    const rlim_t kStackSize = 1024L * 1024L * 1024L;   // min stack size = 64 Mb
    struct rlimit rl;
    int result;

    result = getrlimit(RLIMIT_STACK, &rl);
    if (result == 0)
    {
      if (rl.rlim_cur < kStackSize)
        {
	  std::cout << "rlimit: " << rl.rlim_cur  << "smaller than " << kStackSize << "; resetting" << std::endl; 
	  rl.rlim_cur = kStackSize;
	  result = setrlimit(RLIMIT_STACK, &rl);
	  if (result != 0)
            {
	      fprintf(stderr, "setrlimit returned result = %d\n", result);
            }
        }
    }

    string lf;   // load file
    string lp;   // full load path
    string sd;   // save directory
    int start_gen;
    int end_gen;
    int gen_steps;
    int env_change_time;
    unsigned individual_number;
    vector<unsigned> group_numbers;
    int nr_mutants = 50;
    int nr_closest = 10;
    double cut_off_robustness = 1./double(BINS);
    int test_number = 0;
    double mutation_rate = 0.5;
    Type typ = PUMP;
    int par = 5;
    int steps = 50;
    double start_val = 0.1;
    double end_val = 10.;
    string config_file = "local_k_config.cfg";   // look for this file when loading
    string local_config = "local_config.cfg";    // write to this file at start analysis
    string change_config = "change_config.cfg";
    bool printing;
    bool no_reset;
    bool env_changing;
    bool defaults = false;
    string grace_par = "/home/thocu/Projecten/VirtualCell/WorkBench/grace_param_files/knockout_graphs.par";


    po::options_description generic("Generic");
    generic.add_options()
      ("help", "help on standard options")
      ("all-help", "help on standard and hidden options")
      ("config-file,c", po::value(&config_file)->default_value(config_file), "load parameters from non-default config file")
      ;

    ostringstream formatter;
    formatter << "max essentiality for additional robustness testing; a default test with " << NEUTRAL_CUT_OFF << " is in addition performed by default" ;

    po::options_description config("Configuration");
    config.add_options()
      ("start-at-generation,g", po::value(&start_gen), "start processing at this generation \n" 
       "    when no --end-at-generation is provided, processing of just this generation will take place")
      ("end-at-generation,e", po::value(&end_gen), "end processing at this generation")
      ("generation-steps,m",po::value(&gen_steps), "analyse every n generations")
      ("test-individual,i",po::value(&individual_number),"choose an individual to analyse")
      ("test-group",po::value<std::vector<unsigned> >(&group_numbers)->multitoken(),"choose a group of individual to analyse")
      ("test-pop","analyse the whole population at a given time point")
      ("population-vs-ancestor","compare multiple population snapshots with the ancestor")
      ("history,h","mutational history of lineage")
      //      ("performance,p","fitness & production over time")
      ("analyse,a","analyse fitness and production performance")
      ("ohnologs","write lists of ohnologs after WGD")
      ("WGD-trace","traces back the WGD history of the last population")
      ("WGD-ancestor-fitness","compares the fitness of an individual having undergone a WGD with that of it's ancestor." )
      ("timeplots,t",po::bool_switch(&printing)->default_value(false),"print concentration and rate timeplots")
      ("knockout,k", "knockout analysis")
      ("robustness,r",po::value(&test_number) ,"mutational robustness\n"
       "Values: \n"
       "  0: \tall types of robustness checking \n"
       "  1: \tanalyse point mutation and major mutation robustness seperately \n"
       "  2: \tanalyse multiple mutations per mutant \n"
       "  3: \tanalyse major mutations only \n"
       "  4: \tanalyse point mutation robustness") 
      ("expression-evolution", "record the evolution of expression levels in an essentiality class (determined by cutoff)")
      ("k-mutation-rate",po::value(&mutation_rate)->default_value(mutation_rate),"set mutation rate for multiple mutation robustness testing")
      ("number-of-mutants",po::value(&nr_mutants)->default_value(nr_mutants),"set number of mutants created per in robustness tests")
      ("number-of-closest",po::value(&nr_closest)->default_value(nr_closest),"set number of individuals that are closest to the ancestor, for mutation testing")
      ("cutoff,u,",po::value(&cut_off_robustness)->default_value(cut_off_robustness),formatter.str().c_str() )
      ("graphs,n", "draw network graphs")
      ("functional,f", "functional representations of cells")   // function graph + string representation
      ("param-sweep-type,w",po::value(&typ),"type of gene to sweep (ENZYME,PUMP or TF) or NOTYPE for environment")
      ("param-sweep-par,v",po::value(&par),"parameter to sweep, where for (PUMP)ENZYME/TF \n"
       "  2: Prom. Str \n"
       "  3: K(p)A/Kd \n"
       "  4: K(p)X/Kb \n"
       "  5: Vmax/EffBound \n"
       "  6: n.a./EffApo")
      ("param-sweep-steps,x", po::value(&steps)->default_value(steps),"number of steps to increase parameter")
      ("param-sweep-start-val,y", po::value(&start_val)->default_value(start_val),"starting param value")
      ("param-sweep-end-val,z" ,  po::value(&end_val)->default_value(end_val),"ending param value")
      ("no-reset", po::bool_switch(&no_reset)->default_value(false),"don't reset concentrations")
      ("env-switching",po::value(&env_changing), "environment switching was used in analyzed run")      
      ("env-change-time",po::value(&env_change_time)->default_value(6000),"when did the environment ACTUALLY in this run")
      ("env-config", po::value(&change_config),"new environment config file")
      ;

    po::options_description hidden("Hidden options");
    hidden.add_options()
      //      ("load-dir", po::value(&ld), "location of load file, default location for further saving")
      ("load-file", po::value(&lf),"dish object file name")
      ("save-dir,s", po::value(&sd), "save output here")
      ("load-file-list,I", po::value< vector<string> >()->multitoken(),"a set of dish save objects to load simultaneously")
      ("grace-par" , po::value(&grace_par)->default_value(grace_par), "grace parameter file")
      ("mutation-ratios", po::value<string>()->default_value("1:1.5:1.5:5"),"ratio INS:DUP:DEL:POINT")
      ("mutation-scale-ratios,R", po::value<string>()->default_value("8:8:1:1"),"ratio GCR:SG:POLYP:NOTHING")
      ("gcr-maximum-fraction,F", po::value(&MAX_GCR_FRAC)->default_value(0.5),"maximum fraction of total genome length affected by GCR")
      ("conversion-rate", po::value(&N)->default_value(4.), "rate of conversion A to X")
      ("passive-diffusion", po::value(&Perm)->default_value(0.1),"passive diffusion of A over cell membrane")
      ("enzyme-start-concentration", po::value(&STARTCONC)->default_value(1.), "start concentration of gene products")
      ("start-A-in", po::value(&startAin)->default_value(0.5),"start concentration A")
      ("start-X-in", po::value(&startXin)->default_value(0.5),"start concentration X")
      ("target-A-in", po::value(&TARGETA)->default_value(1.), "target A concentration")
      ("target-X-in", po::value(&TARGETX)->default_value(1.), "target X concentration")
      ("assays", po::value(&ASSAYS)->default_value(3), "number of environments per generation")
      ("enzyme-degradation", po::value(&Degr)->default_value(1.), "degradation rate of gene products")
      ("mutation-rate,M", po::value(&MUTATION_RATE)->default_value(0.05), "per gene mutation rate")
      ("lifetime", po::value(&LIFETIME)->default_value(1000), "allowed time for the cells to equilibriate their internal state")
      ("skewed-environment", po::bool_switch(&ENV_SKEWED), "skew environment distribution towards lower lower concentrations")
      ("3-environments", po::bool_switch(&_3ENV), "when running sparse, use standard environments only")
      ("change-threshold", po::value(&CHANGE_THRESHOLD)->default_value(0.01), "fraction in concentration change considere no change")
      ("integration-step", po::value(&H)->default_value(0.1), "Euler integration step size")
      ("error-rate", po::value(&ERROR_RATE)->default_value(.95), "if errors in pop above this fraction, reduce step size")
      ("step-reduction", po::value(&STEP_REDUCTION)->default_value(10.), "fold reduction in step size upon integration error dection")
      ("mutation-type", po::value(&PER_GENE_MUTATION)->default_value(3),
       "Values: \n"
       "  1: \tno major mutations \n"
       "  2: \tmajor mutations, no ordered genome \n"
       "  3: \tmajor mutations on ordered genome \n"
       "  4: \tsmall scale geometrically distributed mutations with wgd \n"
       "  5: \tno major mutations; with wgd \n"
       "  6: \tmajor mutations with wgd \n"
       "  7: \tsmall scale + major mutations + wgd, following mutation-scale-ratios"
       ) 
      ("geometric-dist-length", po::value(&GEO_DIST_LENGTH)->default_value(0.1), "parameter to set up probability density function for stretch-lengths")
      ("production", po::bool_switch(&DEATH)->default_value(false), "production is an additional fitness criterium")
      ("low-profile", po::bool_switch(&LOWPROFILE), "reduce output")
      ("tree-cut-off", po::value(&TREE_CUTOFF)->default_value(50), "don't add nodes of last generations to tree")
      ("save-time", po::value(&SAVETIME)->default_value(500), "save population after this many generation")
      ("measure-step", po::value(&MEASURESTEP)->default_value(0.1), "Do measurements after this many timesteps")
      ("no-target-conc", po::bool_switch(&NO_TARGET_CONC)->default_value(false), "no target concentration as fitness criterium")
      ("restrict-size", po::bool_switch(&RESTRICT_SIZE)->default_value(false), "restrict genome size by setting a penalty on raw fitness")
      ("size-max", po::value(&SIZE_MAX)->default_value(500),"genome size above which a penalty will be applied")
      ("size-penalty", po::value(&SIZE_PENALTY)->default_value(0.1),"penalty per gene on raw fitness data")
      ("default-options" , po::bool_switch(&defaults), "print defaulted values in local config file as wel")
      ("pre-wgd-load-file", po::bool_switch(&BEFORE_WGD)->default_value(false),"refrain from serializing the non-existent wgds parameter in old saves (patch)")
      ;

    //parse command line first to have priority over config file options
    po::options_description cmdline_options;
    cmdline_options.add(generic).add(config).add(hidden);

    po::options_description config_file_options;
    config_file_options.add(config).add(hidden);

    po::options_description visible("Allowed options");
    visible.add(generic).add(config);
  
    po::positional_options_description p;
    //    p.add("load-dir",1);
    p.add("load-file", 1);
    p.add("save-dir", 1);

    po::variables_map vm;

    // parse command line first, specified options are protected from overwriting
    po::store(po::command_line_parser(argc,argv).
	      options(cmdline_options).positional(p).run(),vm);

    // notify first time to be able to specify configuration file location
    po::notify(vm);

    // look for a local configuration file
    bf::path _path;
    _path = bf::path(lf);
    _path /= "../";
    _path /= config_file;
    _path.normalize();
    ifstream ifs(_path.string().c_str());    
    if(!ifs.fail()){               // check if file exists
      po::store(parse_config_file(ifs,config_file_options), vm);
      std::cout << "loading local configurations" << std::endl;
    }
    else std::cerr << "cannot find '"+ _path.string() + "'" << std::endl; 

    po::notify(vm);
     
    // print out help before processing other options
    if(vm.count("help")){
      std::cout << visible << std::endl;
      return 0;
    }     

    if(vm.count("all-help")){
      std::cout << cmdline_options << std::endl;
      return 0;
    }     

    // print out chosen configuration options
    string configuration = map_to_string(vm,defaults);
    std::cout << configuration;

    // some consistency checking 
    option_dependency(vm,"end-at-generation","start-at-generation");
    option_dependency(vm,"generation-steps","end-at-generation");
    option_dependency(vm,"graphs","knockout");
    option_dependency(vm,"knockout","analyse");
    option_dependency(vm,"robustness","analyse");
    option_dependency(vm,"population-vs-ancestor","save-dir");
    option_dependency(vm,"expression-evolution","knockout");
	option_dependency(vm,"WGD-ancestor-fitness", "test-group");
    //option_dependency(vm,"population-vs-ancestor","generation-steps");

    //parsing special MUTATIONS RATIO option value
    if(vm.count("mutation-ratios")){
      std::string str = vm["mutation-ratios"].as<string>();
      typedef boost::tokenizer<boost::char_separator<char> > 
	tokenizer;
      boost::char_separator<char> sep(":");
      tokenizer tokens(str, sep);
      list<double> rats(0);
      for (tokenizer::iterator tok_iter = tokens.begin();tok_iter != tokens.end(); ++tok_iter){
	stringstream ss(*tok_iter);
	double n; 
	ss >> n; 
	rats.push_back(n);
      }
      if(rats.size() != 4) {
	std::cerr << "mutation-ratios not well defined" << std::endl;
	return -1;
      }
      INS = rats.front();
      rats.pop_front();
      DUP = rats.front();
      rats.pop_front();
      DEL = rats.front();
      rats.pop_front();
      POINT = rats.front();
    }

    // starting directory creation
    if(!vm.count("save-dir")){
      _path = bf::path(lf);
      _path /= "..";
      _path.normalize();
    }
    else{
      _path = bf::path(sd);
      if(! bf::exists(_path))
	bf::create_directory(_path);
      else if( ! bf::is_directory(_path)){
	std::cerr << "given savedirectory exists as (regular) file," << std::endl 
		  << "cannot save" << std::endl;
	return 1;
      }
    }

    if(vm.count("mutation-scale-ratios")){
      std::string str = vm["mutation-scale-ratios"].as<string>();
      typedef boost::tokenizer<boost::char_separator<char> > 
	tokenizer;
      boost::char_separator<char> sep(":");
      tokenizer tokens(str, sep);
      list<double> rats(0);
      for (tokenizer::iterator tok_iter = tokens.begin();tok_iter != tokens.end(); ++tok_iter){
	stringstream ss(*tok_iter);
	double n; 
	ss >> n; 
	rats.push_back(n);
      }
      if(rats.size() != 4) {
	std::cerr << "mutation-scale-ratios not well formed" << std::endl;
	return 1;
      }
      GCR = rats.front();
      rats.pop_front();
      SG = rats.front();
      rats.pop_front();
      POLYP = rats.front();
      rats.pop_front();
      NOTHING = rats.front();
    }

    if(vm.count("start-at-generation")){
      ostringstream formatter;
      if(vm.count("end-at-generation")){
	if(vm.count("generation-steps")){
	  formatter << "step_section_" << start_gen << "_" 
		    << end_gen << "_" << gen_steps ;
	  _path /= formatter.str();
	}
	else{
	  formatter << "section_" << vm["start-at-generation"].as<int>() << "_"
		    << vm["end-at-generation"].as<int>() ;
	  _path /= formatter.str();
	}
      }
      else {
	formatter << "slice_" << vm["start-at-generation"].as<int>() ; 
	_path /= formatter.str();
      }
    }
    else{
      if(vm.count("test-individual")){
	_path /= "individual"+boost::lexical_cast<string>(individual_number);
      }
      else if(vm.count("test-group")){
	_path /= "group"+boost::lexical_cast<string>(group_numbers[0]);
      }
      else if(vm.count("test-pop")){
	_path /= "population";
      }
      else if(vm.count("population-vs-ancestor")){
	_path /= "pop_vs_ancestor";
      }
      else _path /= "global";
    }

    if(vm["env-switching"].as<bool>()==true){
      if(!bf::exists(_path.string()+ "_before"))
	bf::create_directory(_path.string()+ "_before");	
      if(!bf::exists(_path.string()+ "_after"))	
	bf::create_directory(_path.string()+"_after");	
    }
    else if(!bf::exists(_path)){
      bf::create_directory(_path);	
    }

    ofstream run_config_file; 
    string savefile = _path.string() + '/' + local_config; 
    run_config_file.open(savefile.c_str());
    run_config_file << configuration;
    run_config_file.close();


    bool skip = false;
    bool do_after = false;
    (void) signal(SIGSEGV,leave);
    knockout * k;
    if(vm.count("population-vs-ancestor")){
      k = new knockout(vm["load-file-list"].as<vector<string> >(),_path.string(),grace_par);
    }
    else{
      if(vm.count("start-at-generation")==0)
	if(vm["env-switching"].as<bool>()==true){
	  k = new knockout(lf,_path.string()+"_before",grace_par,1,env_change_time);
	  do_after = true;
	}
	else
	  k = new knockout(lf,_path.string(),grace_par);
      else if(vm.count("end-at-generation")==0)
	if(vm["env-switching"].as<bool>()==true && start_gen > env_change_time){
	  skip = true;
	  do_after = true;
	}
	else if(vm["env-switching"].as<bool>()==true)
	  k = new knockout(lf,_path.string()+"_before",grace_par,start_gen,start_gen);
	else
	  k = new knockout(lf,_path.string(),grace_par,start_gen,start_gen);
      else{
	if(vm.count("generation-steps")){
	  if(vm["env-switching"].as<bool>()==true && start_gen > env_change_time){
	    skip = true;
	    do_after = true;
	  }
	  else if(vm["env-switching"].as<bool>()==true && end_gen > env_change_time){
	    k = new knockout(lf,_path.string()+"_before",grace_par,start_gen,env_change_time,gen_steps);
	    do_after = true;
	  }
	  else if(vm["env-switching"].as<bool>()==true)
	    k = new knockout(lf,_path.string()+"_before",grace_par,start_gen,end_gen,gen_steps);
	  else
	    k = new knockout(lf,_path.string(),grace_par,start_gen,end_gen,gen_steps);	 
	}
	else{
	  if(vm["env-switching"].as<bool>()==true && start_gen > env_change_time){
	    skip = true;
	    do_after = true;
	  }
	  else if(vm["env-switching"].as<bool>()==true && end_gen > env_change_time){
	    k = new knockout(lf,_path.string()+"_before",grace_par,start_gen,env_change_time);
	    do_after = true;
	  }
	  else if(vm["env-switching"].as<bool>()==true)
	    k = new knockout(lf,_path.string()+"_before",grace_par,start_gen,end_gen);
	  else
	    k = new knockout(lf,_path.string(),grace_par,start_gen,end_gen);
	}
      }
    }
    bool analyse = true;
    while(analyse){
      analyse = false;
    // after loading the old style save file we toggle the Boolean
    // such that in the new dish save, we do save the wgds parameter
      if(!skip){
	if(vm.count("pre-wgd-load-file")) BEFORE_WGD = false; 

	if(vm.count("number-of-mutants"))
	  k -> set_nr_mutants(nr_mutants);
	if(vm.count("number-of-closest"))
	  k -> set_nr_closest(nr_closest);

	k -> print = printing;
	k -> resetting = !no_reset;
	if(vm.count("test-individual")){
	  vector<unsigned> one(1);
	  one[0]=individual_number;
	  k -> pick_individuals(one);
	}
	else if(vm.count("test-group")){
	  k -> pick_individuals(group_numbers);
	}
	else if(vm.count("test-pop")){
	  k -> last_generation();
	}
	else if(vm.count("population-vs-ancestor")){
	  k->pick_lineage();
	  k-> population_snapshots();
	}
	else  k -> pick_lineage();

	if(vm.count("test-pop")){
	  k -> analyse_population();
	  k -> major_mutation_robustness_pop();
	}    
	if(vm.count("population-vs-ancestor")){
	  k -> analyse_population_snapshots();
	  k -> analyse_lineage();
	  k -> major_mutation_robustness_pop_vs_ancestor();
	}
	if(vm.count("analyse")){
	  k -> fitness_ancestors();                 // ancestor_real_fitnesses    
	  k -> analyse_lineage();
	  k -> fitness_ancestors();                 // ancestor_standard_fitness
	}                
	if(vm.count("knockout")){
	  k -> knockout_analyse_lineage();     // calculate gene contributions in every ancestor
	  k -> binned_gene_contribution();
	}
	if(vm.count("expression-evolution")){
	  k -> expression_level_evolution();
	}
	if(vm.count("ohnologs")){
	  k -> ohnologs();
	}
	if(vm.count("WGD-trace")){
	  k -> last_generation();
	  k -> pick_lineage();
	  k -> WGD_trace();
	}
	 if(vm.count("WGD-ancestor-fitness")){
	  k -> WGD_ancestor_fitness();
	}
	if(vm.count("history")){
	  k -> size_ancestors();
	  k -> type_numbers();
	  k -> cumulative_mutation_contributions(); // 
	  k -> cumulative_mutations();
	  k -> mutations_per_gene_type();
	  k -> cumulative_mutation_expectations();
	  k -> expectations_per_gene_type();
	  if(vm.count("knockout"))
	    k -> contribution_per_gene_type();
	}
	if(vm.count("robustness")){
	  switch (test_number){
	  case 1:
	    k -> neutral_gene_mutant_effect(NEUTRAL_CUT_OFF); //robustness towards point mutations of non-essential genes
	    k -> neutral_gene_mutant_effect(cut_off_robustness); //robustness towards point mutations of non-essential genes
	    k -> analyse_duplicated_deleted_genes();          //robustness towards point mutations of deleted/duplicated genes
	    k -> major_mutation_robustness();                       //robustness towards random dups/dels
	  case 2:
	    k -> multiple_mutation_robustness(mutation_rate);
	    break;
	  case 3: 
	    k -> major_mutation_robustness();
	    break;
	  case 4: 
	    //k -> neutral_gene_mutant_effect(NEUTRAL_CUT_OFF); //robustness towards point mutations of non-essential genes
	    k -> neutral_gene_mutant_effect(cut_off_robustness); //robustness towards point mutations of non-essential genes
	    k -> analyse_duplicated_deleted_genes();          //robustness towards point mutations of deleted/duplicated genes
	    break;
	  default:
	    k -> neutral_gene_mutant_effect(NEUTRAL_CUT_OFF); //robustness towards point mutations of non-essential genes
	    k -> neutral_gene_mutant_effect(cut_off_robustness); //robustness towards point mutations of non-essential genes
	    k -> analyse_duplicated_deleted_genes();          //robustness towards point mutations of deleted/duplicated genes
	    k -> major_mutation_robustness();                       //robustness towards random dups/dels
	    k -> multiple_mutation_robustness(mutation_rate);
	  }
	}
	if(vm.count("graphs")){
	  k -> gene_contribution_graphs();
	  k -> function_graphs();
	  k -> genome_order_graphs();
	}
	if(vm.count("functional")){
	  k -> print_cells();
	  if(!vm.count("graphs"))
	    k -> function_graphs();
	}
	if(vm.count("param-sweep-type") || vm.count("param-sweep-par")){
	  k -> param_sweep(typ,par,steps,start_val,end_val);
	}
	if(vm.count("history")){
	  k -> plotGrace();
	  k -> exitGrace();
	}
	else
	  k -> closeGrace();
	sleep(5);
	delete k;
      }    

    /* ---go on if there was an environmental switch that we need to analyse out ---*/
    

      if(do_after){
	/* this part loads new configuration settings in the special
	 case of switching environment during the simulation. It is
	 necessary to make a new variables mapping by parsing the new
	 variables first and then augmenting with the old command line
	 and configuration file options. */

	po::variables_map env_change_map;
	ifstream ifs(change_config.c_str());    
	if(!ifs.fail()){               // check if file exists
	  po::store(parse_config_file(ifs,config_file_options), env_change_map);
	  po::notify(env_change_map);

	  /*From here on, parsing proceeds in the same way as the
	    initial option parsing */
	  po::store(po::command_line_parser(argc,argv).
		    options(cmdline_options).positional(p).run(),env_change_map);
	  po::notify(env_change_map);

	  bf::path conf_path;
	  conf_path = bf::path(lf);
	  conf_path /= "../";
	  conf_path /= config_file;
	  conf_path.normalize();
	  ifstream ifs(conf_path.string().c_str());    
	  if(!ifs.fail()){               // check if file exists
	    po::store(parse_config_file(ifs,config_file_options), env_change_map);
	    std::cout << "loading local configurations" << std::endl;
	  }
	  else std::cerr << "cannot find '"+ conf_path.string() + "'" << std::endl; 
	  po::notify(env_change_map);

	}
	else std::cerr << "cannot find '" <<  change_config << "'" << std::endl;

	std::cout << "CHANGING ENVIRONMENT" << std::endl;
	std::cout << map_to_string(env_change_map,defaults);

	
	if(vm.count("start-at-generation")==0)
	  {
	  k = new knockout(lf,_path.string()+"_after",grace_par,env_change_time+1);
	  }
	else if(vm.count("end-at-generation")==0)
	  	{
	 	k = new knockout(lf,_path.string()+"_after",grace_par,start_gen,start_gen);
	  	}
	     else
	     {
	     	if (start_gen>env_change_time+1)
		{
	     	     if(vm.count("generation-steps"))
		     {
	        	k = new knockout(lf,_path.string()+"_after",grace_par,start_gen,end_gen,gen_steps);
		     }
	    	     else
	     	     k = new knockout(lf,_path.string()+"_after",grace_par,start_gen,end_gen);	
		}
		else
		{
		if(vm.count("generation-steps"))
	 		{    
			 k = new knockout(lf,_path.string()+"_after",grace_par,env_change_time+1,end_gen,gen_steps);
			}
	  		else
			{
	    		 k = new knockout(lf,_path.string()+"_after",grace_par,env_change_time+1,end_gen);
			}
		}
	}	
	analyse = true;
	do_after = false;
	skip = false;
      }
    
    }
    }
  catch(exception& e) {
    cerr << e.what() << "\n";
  }
}
 

void leave(int sig) {
  printf("SEGFAULT OCCURED, ATTEMPTING TO CLOSE GRACEBAT... \n");
  gracesc::Xmgrace::close();  
  exit(sig);
}
