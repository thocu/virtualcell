CPP = g++ 
CC = gcc
OBJDIR := ../build/

SHAREDOBJ := $(addprefix $(OBJDIR),petridish.o cell.o param_option.o genestructs.o gene.o my_random.o gracesc.o)
KNOCKOBJ := $(OBJDIR)main2.o $(OBJDIR)knockout.o 
VCOBJ := $(OBJDIR)spatial_dish.o
OBJS := $(SHAREDOBJ) $(KNOCKOBJ) $(VCOBJ)

CASHSUBDIR := cash/
CASHSOURCE := $(SOURCEDIR)$(CASHSUBDIR)
OBJCASHDIR =  $(OBJDIR)$(CASHSUBDIR)
OBJCASH = $(addprefix $(OBJCASHDIR),arithmetic.o basic.o color.o filter.o io.o logical.o \
margolus.o movie.o neighbors.o noise.o png.o ps.o random.o shift.o\
x11.o histogram.o)
OBJCASH2 = $(OBJCASHDIR)mersenne.o 
OBJMAIN = $(OBJCASHDIR)cash2-s.o  $(OBJCASHDIR)cash2.o 
OBJCASHALL = $(OBJCASH) $(OBJCASH2) $(OBJMAIN)

REBUILDABLES = $(OBJCASHALL) $(OBJS) $(OBJDIR)$(KNOCKOUT) $(OBJDIR)$(VC)

IPATH := -I$(CASHSUBDIR)

LIBBOOST =  -lboost_serialization -lboost_program_options -lboost_filesystem -lboost_system 
LIBGRACE = -lgrace_np
LARGE = -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64


KNOCKOUT = knockout
VC = virtual_cell_spatial

#SOURCEDIR = ./
#SOURCES = $(SOURCDEDIR)*.cpp



LDFLAGS = -O3 -Wall
CFLAGS = -c -O3 -Wall $(IPATH)

CCOPT = -c -O3 -Wall  
#CCOPT = -fPIC -c -ggdb -Wall

LDIR = -L/usr/X11R6/lib #do we still need this
LIBSCASH = -lc -lpng -lz -lX11 -lm $(LIBGRACE)

$(OBJS): | $(OBJDIR)
$(OBJCASHALL): | $(OBJCASHDIR)

$(OBJDIR):
	mkdir -p $@
 
$(OBJCASHDIR):
	mkdir -p $@

#generic rule to build from cpp 
$(OBJDIR)%.o : %.cpp
	$(CPP) $(CFLAGS) -o $@ $<

$(OBJDIR)%.o : %.cc
	$(CPP) $(CFLAGS) -o $@ $<
	
$(OBJCASHDIR)%.o: $(CASHSOURCE)%.c
	$(CC) $(CCOPT) -o $@ $<

$(OBJCASHDIR)%.o: $(CASHSOURCE)%.cpp 
	$(CPP) $(CFLAGS) -o $@ $<

#header dependencies	
$(OBJDIR)knockout.o : knockout.hpp petridish.hpp param_option.hpp cell.hpp genestructs.hpp gene.hpp $(HPPHELP) Makefile
$(OBJDIR)petridish.o: petridish.hpp cell.hpp param_option.hpp genestructs.hpp gracesc.h gene.hpp $(HPPHELP) Makefile
$(OBJDIR)cell.o: cell.hpp genestructs.hpp gene_iter_wrapper.hpp gene.hpp $(HPPHELP) Makefile
$(OBJDIR)param_option.o :param_option.hpp Makefile
$(OBJDIR)genestructs.o: genestructs.hpp gene.hpp temp.hpp Makefile
$(OBJDIR)gene.o: gene.hpp genestructs.hpp temp.hpp Makefile
$(OBJDIR)my_random.o: my_random.hpp Makefile
$(OBJDIR)gracesc.o: gracesc.h grace_np.h Makefile
$(OBJDIR)main2.o: knockout.hpp $(HPPHELP) Makefile
$(OBJDIR)spatial_dish.o: spatial_dish.hpp $(CASHSOURCE)cash2-s.hpp Makefile

$(OBJCASH): $(CASHSOURCE)cash2003.h $(CASHSOURCE)histogram.h Makefile
$(OBJCASH2): $(CASHSOURCE)cash2.hpp $(CASHSOURCE)mersenne.h  Makefile
$(OBJMAIN): $(CASHSOURCE)cash2003.h $(CASHSOURCE)cash2.hpp $(CASHSOURCE)mersenne.h $(CASHSOURCE)cash2-s.hpp \
$(CASHSOURCE)cash2-s.cpp $(CASHSOURCE)histogram.h petridish.hpp Makefile


# inference rules http://www.opussoftware.com/tutorial/TutMakefile.htm

# '$@' : the name of the file to be made ( name of the rule )
# '$<' : the name of the (first) related file ( dependent ) that caused the action

all: $(VC) $(KNOCKOUT)
 
$(KNOCKOUT): $(SHAREDOBJ) $(KNOCKOBJ) Makefile
	$(CPP) $(LDFLAGS) $(INCLUDE) -o $(OBJDIR)$@ $(LARGE) $(SHAREDOBJ) $(KNOCKOBJ) $(LIBGRACE) $(LIBBOOST) $(LDIR)
 
$(VC): $(SHAREDOBJ) $(VCOBJ) $(OBJCASH) $(OBJCASH2) $(OBJMAIN) Makefile
	 $(CPP) $(LDFLAGS) $(INCLUDE) -o $(OBJDIR)$@ $(OBJCASH) $(OBJCASH2) $(OBJMAIN) $(SHAREDOBJ) $(VCOBJ) $(LDIR) $(LIBBOOST) $(LIBSCASH)  	

clean:
	rm -f -v $(REBUILDABLES)
	echo Clean done
	


