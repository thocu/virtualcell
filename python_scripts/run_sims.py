#autowinds
import sys
import os
import subprocess
import locale
import _thread
import time

class autowind:

	def runset (mutant, windslocation, datalocation, duplicates, numbers, randomstart, environmentFile, generationstarts, generationstartFilenames, endgeneration, oldlist, writelocation):
		startedrun = oldlist
		totalrunstart = 0
		#random = int(randomstart) +1
		#check if mutant has freecores
		encoding = locale.getdefaultlocale()[1]
		allcores = (float((str(subprocess.check_output(["rsh","mutant" + str(mutant), "grep -c processor /proc/cpuinfo"])))[2:-3]))
		top_out =subprocess.check_output(["rsh","mutant" + str(mutant), "top","-b -n 1"])
		usedcores = (float((top_out.decode(encoding)).split('\n')[0].split()[-3][0:-2])+0.2)
		print("usedcores " +str(usedcores))
		freecores = allcores - usedcores	
		for i, t1 in zip(duplicates, list(range(0,len(mutantnumbers)))):
			if (len(startedrun)<=t1):
				startedrun.append(list())
			for j,genstart,generationstartFilename, t2 in zip(numbers,generationstarts, generationstartFilenames, list(range(0,len(mutantnumbers)))):
				if (len(startedrun[t1])<=t2):
					startedrun[t1].append(False) #adds to the list the first time this duplicate is seen in the run.

				if ((freecores >1) and (startedrun[t1][t2]==False)):			
					random= (randomstart + t1*3 +t2)
					programstart = windslocation +"winds_of_change"
					'''
					##################
					fill in the winds of change order here.
					
					#environmental change:
					print("rsh mutant" + mutant + " " + windslocation +"winds_of_change "+"-l " + datalocation+ "generation"+ generationstartFilename + " --generations " +str(endgeneration) + " --evo-seed " + str(random) + " -h 0.004 --save-dir " + datalocation + "noenvduplo"+ str(j)+ "-" +str(i) +" >& " + datalocation + "noenvlogdup"+str(j)+"-"+str(i) + " &")
					os.system("rsh mutant" + mutant + " " + windslocation +"winds_of_change "+"-l " + datalocation+ "generation"+ generationstartFilename + " --generations " +str(endgeneration) + " --evo-seed " + str(random) +" -e " +"1" + " --env-change-time " + str(genstart) + " --env-config " + environmentFile + " --save-dir " + datalocation + "duplo"+ str(j)+ "-" +str(i) +" >& " + datalocation + "logdup"+str(j)+"-"+str(i) + " &")
					#no environmental change, change 
					os.system("rsh mutant" + mutant + " " + windslocation +"winds_of_change "+"-l " + datalocation+ "generation"+ generationstartFilename + " --generations " +str(endgeneration) + " --evo-seed " + str(random) + " -h 0.004 -e 0 --save-dir " + datalocation + "noenvduplo"+ str(j)+ "-" +str(i) +" >& " + datalocation + "noenvlogdup"+str(j)+"-"+str(i) + " &")
						
					
					###################
					'''
					#evironmental switch between neutral and environment every 1000 generations
					os.system("ssh mutant" + mutant + " " + windslocation +"winds_of_change "+"-l " + datalocation+ "generation"+ generationstartFilename + " --generations " +str(endgeneration) + " --evo-seed " + str(random) + " -h 0.4 -e 0  --env-switch-time 1000" + " --env-config " + environmentFile + " --save-dir " + datalocation + "duplo"+ str(j)+ "-" +str(i) +" > " + datalocation + "logdup"+str(j)+"-"+str(i) + " 2>&1 &")
					#print("rsh mutant" + mutant + " " + windslocation +"winds_of_change "+"-l " + datalocation+ "generation"+ generationstartFilename + " --generations " +str(endgeneration) + " --evo-seed " + str(random) + " -h 0.4 -e 0 --env-switch-change 1000" + " --env-config " + environmentFile + " --save-dir " + datalocation + "duplo"+ str(j)+ "-" +str(i) +" > " + datalocation + "logdup"+str(j)+"-"+str(i) + " 2>&1 &")
					startedrun[t1][t2]=True
					totalrunstart += 1
					freecores -=1
					print("mutant" + str(mutant) + " freecores " + str(freecores))
					f = open(writelocation, "a")
					f.write (str(mutant) + ' ' + str(j) + ' ' + str(i) + ' ' + str(t1) + ' ' +str(t2) + "\n")
					f.close()
					time.sleep(5)
		amfinished = True
		for duplicate in startedrun:
			for starttime in duplicate:
				if (starttime == False):
					amfinished = False
		return ([startedrun, amfinished])
		
							
	def locatiebepaler (mutantnummer, stageproject):
		locatie = "/hosts/linuxhome/mutant" +str(mutantnummer)+ "/tmp/jaap/" + stageproject+'/'
		return locatie
	def stageproject (project, environmentnumber):
		stageproject = project + 'system_env' + str(environmentnumber)
		return stageproject
	
	
	def optionparser(aw, mutantnumbers, seednumbers, duplicates, numbersetlength, startnumberlength, randomstart, environmentnumbers, generationstartset, generationstartFilenamesset, minimumRunlength, project):
		batchrunset = list()
		finishedruns = list()
		answer = list()
		allrun = False
		firstrun = True
		runcounter = 0
		totalcycles = (len(seednumbers)*len(environmentnumbers))/len(mutantnumbers)
		print (str(totalcycles))
		if os.path.isfile(project[1]):
			readfile = open(project[1], 'r') #opens a previous file with the same project name
			for line in readfile:
				linedata = line.split(' ')
				if (len(linedata) >4):
					runnumber = int(linedata[1])
					runinstant = int(linedata[3])
					instanttime = int(linedata[4])
					runset = ((runnumber-(startnumberlength-1))//numbersetlength)
					while (len(batchrunset) <= runset):
						batchrunset.append(list())
					while (len(batchrunset[runset]) <= runinstant):
						batchrunset[runset].append(list())
					while (len(batchrunset[runset][runinstant])<=len(duplicates)):
						batchrunset[runset][runinstant].append(list())
					while (len(batchrunset[runset][runinstant])<=numbersetlength):
						batchrunset[runset][runinstant].append(False)
					batchrunset[runset][runinstant][instanttime]= True
					while (len(batchrunset) > len(finishedruns)):
						finishedruns.append(False)
		else:
			print("newfile")	
			
			
		
		while (allrun == False):
			mutantcycling = 0
			while (mutantcycling < totalcycles):
				for mutant, t1 in zip(   mutantnumbers, list(  range( mutantcycling*len(mutantnumbers) ,(len(mutantnumbers))*(1+mutantcycling) )  )   ):
					if (len(batchrunset)<=t1):
						batchrunset.append(list())
					if (len(finishedruns)<=t1):
						finishedruns.append(False)
					seednumberdeduction = (t1//len(environmentnumbers))
					seed = seednumbers[seednumberdeduction]
					environmentnumber = environmentnumbers[t1%(len(environmentnumbers))]
					print (str(environmentnumber))
					stageproject = aw.stageproject(project[0], environmentnumber)
					windslocation = aw.locatiebepaler (mutant, stageproject) 
					environmentFile = windslocation + 'systematic_env' + environmentnumber + '.cfg'
					datalocation = windslocation + 'seed' + seed +'/'
					generationstarts = generationstartset[seednumberdeduction].split('-')
					endgeneration = int(generationstarts[-1])+minimumRunlength
					if (type(generationstartFilenamesset) ==type('')):
						if (generationstartFilenamesset == ('generic')):
							generationstartFilenames = generationstarts
						elif (generationstartFilenamesset.startswith('set')):
							generationstartFilenames = generationstartFilenameset.split(',')[t1+1]
					else:
						generationstartFilenames = generationstartFilenamesset.split('-')[t1]
					numberset = list()
					t2=0
					while (t2 < numbersetlength):
						numberset.append(startnumberlength + numbersetlength*t1 + t2)
						t2 +=1
					randomrunstart = randomstart + (t1*len(duplicates)*len(numberset))
					
					if (firstrun):
						try:
							os.makedirs(datalocation)
						except:
							fail = True
						#subprocess.call(["cp", "-f", "/home/jaap/miniproject_1/seeds/" + seed +"/local_config.cfg", datalocation])
						subprocess.call(["cp", "-f", "/home/jaap/git-repository/virtual-cell/winds_of_change", windslocation])	
						#subprocess.call(["cp", "-f", "/home/jaap/miniproject_1/config_files/systematic/systematic_env" + environmentnumber + ".cfg", windslocation])
						#for start in generationstarts:
						#	subprocess.call(["cp", "-f", "/home/jaap/miniproject_1/seeds/" + seed +"/generation" +start, datalocation])
					
					if (finishedruns[t1] ==False):
						answer = aw.runset(mutant, windslocation, datalocation, duplicates, numberset, randomrunstart, environmentFile, generationstarts, generationstartFilenames, endgeneration, batchrunset[t1],project[1])
						batchrunset[t1]=answer[0]
						if(answer[1]):
							finishedruns[t1] = True
				mutantcycling +=1
			allrun = True
			finishcounter = 0
			for run in finishedruns:
				if (run == False):
					allrun = False
				else:
					finishcounter +=1
			runcounter +=1
			print("runcounter " + str(runcounter))
			print("finished " + str(finishcounter) + " out of " + str(len(seednumbers)*len(environmentnumbers)))
			if (firstrun):
				time.sleep(20000)	
			firstrun = False
			time.sleep(12800)
			
if (__name__ == '__main__'):
	aw = autowind
mutantnumbers = sys.argv[1].split(',')
seednumbers = sys.argv[2].split(',')
duplicates = sys.argv[3].split(',')
numbersetlength = int(sys.argv[4])
startnumberlength = int(sys.argv[5])
randomstart = int(sys.argv[6])
environmentnumbers = sys.argv[7].split(',')
generationstartset = sys.argv[8].split(',')
minimumRunlength = int(sys.argv[9])
project = sys.argv[10].split(',')
if (len(sys.argv) > 11):
	generationstartFilenamesset = sys.argv[11].split(',')	
else:
	generationstartFilenamesset = 'generic'


#encoding = locale.getdefaultlocale()[1]
#print(subprocess.check_output(["ssh","mutant3","grep -c processor /proc/cpuinfo"]))
#subprocess.call(["ls","/hosts/linuxhome/mutant13/tmp/jaap"])
#top_out =subprocess.check_output(["ssh","mutant3","top","-b -n 1"])
#print(str(float((top_out.decode(encoding)).split('\n')[0].split()[-1])-0.2)
aw.optionparser(aw, mutantnumbers, seednumbers, duplicates, numbersetlength, startnumberlength, randomstart, environmentnumbers, generationstartset, generationstartFilenamesset, minimumRunlength, project)


'''
            retcode = subprocess.call(["ssh",host,program,options,
                                       load_file,redirect])

#voorbeeldcommando:python3 autowind.py 7,9,13,8,18,26,31,32,36,37,38,39 2,3,4 1,2,3,4 3 13 49 1,10,14,44 4000-8000-15500,11500-15500-23000,4500-8500-16000 15000 min_pro1_
#commando project 2_2: python3 autowindpro_2.py 17,19,23,24,25,27,34,37 1,2,3,4 1,2,3,4 3 1 100 1,10,14,44 3500-7500-15000,4000-8000-15500,11500-15500-23000,4500-8500-16000 15000 pro_2_2_,/home/jaap/project_2/firstrun
#commando project 2_3: python3 autowindpro_2.py 3,4,5,6,8,10,11,14,15,40 1,2,3,4 1,2,3,4 3 1 200 1,10,14,44,73 3500-7500-15000,4000-8000-15500,11500-15500-23000,4500-8500-16000 15000 pro_2_3_,/home/jaap/project_2/thirdrun
#commando project 2_4: python3 autowindpro_2.py 3,4,5,6,8,10,11,14,15,40 1,2,3,4 1,2,3,4 3 1 200 1,10,14,44,73 3500-7500-15000,4000-8000-15500,11500-15500-23000,4500-8500-16000 15000 pro_2_4_,/home/jaap/project_2/fouthrun

#in project2_2 we restarted without mutant37, so it has to be manually added later.
'''
