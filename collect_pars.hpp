//petridish.hpp
#define LIFETIME 1000 
#define MEASURESTEP 1       //every n steps we make a concentration measurement if LOWPROFILE = false

#define PER_GENE_MUTATION 2 // 1 = no major mut,2 = major mut,3 = ordered genome 
#define SPARSE true
#define DEATH false    // IF TRUE, reproduce2() IS USED; make production rate an evolutionary target
#define ENV_SKEWED 0 // skew the environment prob func towards lower concentrations
                     // using a normal distribution with mean < 0.

#define REPRODUCE 1  //1 or 2 works with DEATH true or false
                     //but when 1 , DEATH doesn't have any effect
#define _3ENV false
#define LOWPROFILE false

#define TREE_CUTOFF 50   // when making a tree, stop adding nodes n generations
                         // before last generation
#define SAVETIME 500     // after how many times will a dish_save + drawing of 
                         // ancestor tree occur
#define STOPSAVE 10000    // 5000 //  stop saving dishes after this time to prevent segfault

//cell.hpp
#define Perm 0.1
#define startAin 0.5
#define startXin 0.5
#define TARGETA 1.
#define TARGETX 1.
#define N 4. // # of ATP molecules produced in catabolism from 1 glucose
#define INS 1 //1
#define DUP 1.5 //0.5  // 1;  1.5
#define DEL 1.5 // 0.5  // 2;  1.5
#define POINT 5 // 7  //     5
#define ASSAYS 3

#define TF_RAT 2.    //1.
#define PUMP_RAT 1.  //1.
#define ENZ_RAT 1.   //1.

//gene.hpp
#define CHANGE_TRHESHOLD 0.001
#define Degr 1.
#define H 0.1
#define STARTCONC 1.

//spatial_dish.cpp
#define MUTATION_RATE 0.05
#define CHANGE_RATE 0.4
